# PFAD - FEB2/FEB6 - Github Repository

The XYTER test suite is written entirely in Python, and depends on ROOT.

![ENC Test board](overview.jpg)

## Quick Start

1. open a new session on the PC: ag-ob-strasse (this will automatically source _/home/ststest/cbmsoft/ipbuslogin.sh_)
2. Navigate to _/home/ststest/PFAD/emu_feb26/python_. This is important, as many of the scripts rely on relative pathes
3. Initialize the connection to the GBTxEMU board and the FEB cards _python start_up.py_

### Calibration of the Channels
For each ASIC on each FEB, all of the 128 Channels have 31 ADC and 1 TDC comparator, which can be tuned individually, using the TRIM values. To get a new calibration, run _python trim_calibration.py_.
The calibration files will be stored in _/home/ststest/PFAD/emu_feb26/calibration_files/trim_calibration_files_.

### S-Curves 
To get the S-Curve of a specific channel run _python scurves.py [channel number]_. This will generate S-Curves for this channel on all connected ASICs.

### Acquiring Data
To acquire data with the current settings, run _python MTsaveRawData.py 0 [length in seconds]_. It can read all connected chips in round-robin manner.

### Getting ENC values
To get the ENC values of all channels, invoke from PFAD_lib the functions _check_trim(smx)_ or _fast_ENC(smx)_.

### GET ENC vs CSA values in an infinite loop
To see nice 2D plots of our ENC vs. the applied CSA, run _python ENCMonitor.py_.

### References

Many Pictures, as well the XYTER manual and the CBM Progress Reports can be found at the [STRASSE Wiki](https://www.strasse.tu-darmstadt.de/Electronics) 
