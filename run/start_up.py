import sys, os, time, struct, code, threading, argparse, numpy as np
sys.path.append("../lib")
sys.path.append("../")
import pprint, time, logging as log

log.basicConfig(
    level=log.WARN,
    format="%(asctime)s,%(msecs)03d:%(module)s:%(levelname)s:%(message)s",
    datefmt="%H:%M:%S",
)

import Environment as Env
import os, uhal, agwb

from PFAD_lib import init_cmd, initialise, set_trim,set_clk_phase_data_phase,\
                     circuit_monitoring_logging, check_trim, fast_ENC,\
                     read_registers, read_trim, channel_test,ENC_scurves_scan, trim_calibration, PFAD_update_configuration
                     
#set_clk_phase_data_phase()

smxes, smx_tester = init_cmd()
list_ASICs, XYTER_registers_config, list_trim_calibration, ASIC_ul, ASIC_name, ASIC_position, registers_130, ana_chan_63, ana_chan_65, ana_chan_67 	= PFAD_update_configuration()

for smx in smxes:
    time_start = time.time()
    print("\nNow at Address {}  Downlink {}  ASIC-UL {}  Uplinks {}   Name {}"
            .format(smx.address, smx.downlink, smx.asic_uplinks, smx.uplinks, ASIC_name[smx.uplinks[0]]))
    if (smx.uplinks[0]==26): initialise(smx)
    #if (smx.uplinks[0]==14): set_trim(smx)
    #if (smx.uplinks[0]==14): read_registers(smx)
    #if (smx.uplinks[0]==14): read_trim(smx)
    #if (smx.uplinks[0]==23):  check_trim(smx)
    #fast_ENC(smx)
    #ENC_scurves_scan(smx)
    #if (smx.uplinks[0]==23):trim_calibration(smx)
    #channel_test(smx)
    time_end = time.time()
    print( "Duration: {:.2f}\n".format(time_end - time_start))

            
