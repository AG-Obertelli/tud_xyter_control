import sys, os, numpy as np

base_path = os.getcwd()+'/../'

config_and_calibration_path = base_path + 'config_and_calibration_files/'
log_path = base_path + 'log_files/'
phases_path = config_and_calibration_path + 'clock_and_data_phases/'
DAQ_path = base_path + 'data_files/root_files/'
"""
config_filename = config_and_calibration_path + 'Chosen_lists.txt'
list_ASICs = 'default'
XYTER_registers_config = 'default'
list_trim_calibration = 'default'
assert os.path.exists(config_filename)
with open(config_filename) as f:
    for line in f:
        x, y = line.split()
        if (x == 'list_ASICs'): list_ASICs = y
        if (x == 'XYTER_registers_config'): XYTER_registers_config = y
        if (x == 'list_trim_calibration'): list_trim_calibration = y



list_of_ASICs_path = config_and_calibration_path + 'list_of_ASICs/{}/'.format(list_ASICs)
XYTER_registers_config_path = config_and_calibration_path + 'register_config/{}/'.format(XYTER_registers_config)
trim_calibration_path = config_and_calibration_path + 'trim_calibration_files/{}/'.format(list_trim_calibration)

nul = 40
#List of ASICs (UL    name    position)
ASIC_name = ["" for i in range(nul)]
ASIC_position = ["" for i in range(nul)]
outfilename = list_of_ASICs_path + 'list_of_asics.txt'
assert os.path.exists(outfilename)
with open(outfilename) as f:
    for line in f:
        x, y, z= line.split()
        ASIC_name[int(x)] = y
        ASIC_position[int(x)] = z
        

#List of registers (UL    name    position)
registers_130 = np.full((19,nul),0)
ana_chan_63 = np.full((nul),0)	     
ana_chan_65 = np.full((nul),0) 	     
ana_chan_67 = np.full((nul),0 ) 	    
outfilename = XYTER_registers_config_path + 'list_register_config.txt'
assert os.path.exists(outfilename)
with open(outfilename) as f:
    for line in f:
        row = line.split()
        ul = int(row[0])
        print(ul,end="   ")
        for reg in range(0,19):
            registers_130[reg][ul]=row[reg+1]
            print(registers_130[reg][ul],end="   ")
        ana_chan_63[ul]=row[20]
        print(ana_chan_63[ul],end="   ")
        ana_chan_65[ul]=row[21]
        print(ana_chan_65[ul],end="   ")
        ana_chan_67[ul]=row[22]
        print(ana_chan_67[ul])
        


# XYTER registers config
#-----------------------------------------------------------------------------------------------------------------------------------
registers_130 = np.full((19,nul),0)
I_csa_value = 31
registers_130[0] = np.full((nul),I_csa_value) #0  csa_value; 	kill_afe:0
registers_130[1] = np.full((nul),63)	      #1  feedback_resistance typ:63; Much mode +64
registers_130[2] = np.full((nul),163)	      #2  .     kill_afe: 163-128
registers_130[3] = np.full((nul),31)	      #3  shaper current;  kill_afe:0
registers_130[4] = np.full((nul),0)	          #4  calibration pulse
registers_130[5] = np.full((nul),12)	      #5  shaping time constant, channel selection
registers_130[6] = np.full((nul),32)	      #6  Reference current for the high speed discriminator
registers_130[7] = np.full((nul),40)	      #7  High speed discriminator threshold, thr_glb2 typ:40; Much mode: 30
registers_130[8] = np.full((nul),22)	      #8  VREF_N   typ:22; Much mode 0
registers_130[9] = np.full((nul),58)	      #9  VREF_P   typ:58; Much mode:25
#------------------------------------------
I_rangeT = [0,1,0]			      # typ: 010
I_vref_T = 57			              # typ:57;   Much mode: 10
registers_130[10] = np.full((nul),128 + I_rangeT[0] *64)	#10  Enable ADC_VREF_T
registers_130[18] = np.full((nul),128 * I_rangeT[1] + 64 * I_rangeT[2] + I_vref_T)	#18  range control + VREF_T
#------------------------------------------
registers_130[11] = np.full((nul),64)	      #11  calSTROBE_DLM  & global_gate_backend (enable)   typ:64;   kill:0;
registers_130[12] = np.full((nul),30)	      #12  NEW IN_CSAP register for PSC reference voltage control    typ:30
registers_130[13] = np.full((nul),I_csa_value)#13  csa_value;		kill_afe:0
registers_130[14] = np.full((nul),27)	      #14  CSA buffer & cascode current control register   8*3  +   3
registers_130[15] = np.full((nul),27)	      #15  SHAPERs buffer & cascode current control register   8*3  +  3
registers_130[16] = np.full((nul),88)	      #16  CSA_BIAS 1V generator
registers_130[17] = np.full((nul),0)	      #17  -
#------------------------------------------
ana_chan_63 = np.full((nul),144)	      # 8-bit ADC control register Typical value 144
ana_chan_65 = np.full((nul),244) 	      # typ:244; Much mode:228
ana_chan_67 = np.full((nul),36 ) 	      # Fast discriminator threshold, Typical value 36
#-----------------------------------------------------------------------------------------------------------------------------------
ASICs_MUCH = [32]
for i in ASICs_MUCH:  # For the back-to-back readout
    #print("Setting UL {} to MUCH mode".format(i))
    registers_130[1][i] = 63+64
    registers_130[9][i] = 58
    registers_130[8][i] = 42
    registers_130[7][i] = 8
    I_vref_t_range = 0b100
    I_vref_t = registers_130[9][i] - 1
    registers_130[10][i] = (1 << 7) + ((I_vref_t_range >> 2 ) << 6)
    registers_130[18][i] = ((I_vref_t_range & 0b11) << 6) + (I_vref_t & 0x3F)
    ana_chan_65[i] = 228
#-----------------------------------------------------------------------------------------------------------------------------------
"""
