
writefunction="Si5344_wr"
infile = "gbtxemu_si5344_regs.txt"
outfile = "gbtxemu_si5344_regs.fs"

lnr=1
ofp=open(outfile,'w')

ofp.write(": si5344_config\n")


with open(infile) as fp:
	for inline in fp:
		inline = inline.rstrip()
		if inline[0]=="#":
			outline = inline.replace("#", "\\ ")
			if "Delay 300 msec" in inline:
				outline=outline+"\n  300 ms"
		else:
			splitline = inline.split(",")
			if len(splitline) != 2:
				print "Couldn't split line nr %d into addr and value:" %lnr
				print inline
			try:
				addr = int(splitline[0],16)
			except:
				print "Couldn't convert expression \"%s\" into address" % splitline[0]
				continue

			try:
				val = int(splitline[1],16)
			except:
				print "Couldn't convert expression \"%s\" into register value" %splitline[1]
				continue

			outline = "${addr:04X} ${value:02X} {command}".format(addr=addr, value=val, command=writefunction)

		print outline
		ofp.write("  "+outline+'\n')

ofp.write(";")
ofp.close()


