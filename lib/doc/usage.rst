Usage
-----
This chapter presents an examples on how to use the :code:`smx_tester` package.

Instantiation
=============

Before instantiating the :code:`SmxTester` class object, one needs to first instantiate proper objects related with IPbus connection and AGWB registers.
These are :code:`ConnectionManager`, :code:`IPbus_interface` and :code:`agwb.top`.

Following listing shows how to instantiate all required objects:

.. code-block:: python
   :caption: Initial objects instantiation.

   manager = uhal.ConnectionManager("file://devices.xml")
   ipbus_interface = IPbusInterface(manager, "EMU")
   agwb_top = agwb.top(ipbus_interface, 0)

The path to the IPbus devices file, and the name of a device are user specific.

Once the top object describing the AGWB registers structure is available, it is possible to instantiate the :code:`SmxTester` object.

.. code-block:: python
   :caption: SmxTester object instantiation.

   smx_tester = SmxTester(agwb_top)

The :code:`SmxTester` class has following parameters :code:`def __init__(self, handle, elink_clk_mode=CLK_160)`.
The :code:`handle` is a handle to the AGWB top object.
The :code:`elink_clk_mode` is mode of the elink clock.
The default value is :code:`CLK_160`, what implies 160 MHz elink clock.
Although change of the :code:`elink_clk_mode` is available on the fly, it is not recommended, as it requires resynchronization of the link.
What is more the phase characteristics for different clock frequencies might be different.

Setup structure discovery
=========================

The Smx Tester is capable of automatically discovering the setup strcuture.
This means discovering which uplinks are connected to which downlink.
One needs to simply call the :code:`scan_setup()` method.
As a result, a list of :code:`setup_element` is returned.

.. code-block:: python
   :caption: Automatic setup structure discovery.

   setup_elements = smx_tester.scan_setup()

Following listings shows result of automatic setup structure discovery.

.. code-block:: python
   :caption: Example :code:`setup_element` print after setup structure discovery.

   [
   Setup element:
     group: 0
     downlink: 0
     uplinks: [0]
     clock_phase_iterator: <smx_tester.smx_tester.SmxTester.ClockPhaseIter object at 0x7fb09e35c3a0>
     data_phase_iterator: <smx_tester.smx_tester.SmxTester.DataPhaseIter object at 0x7fb09790b910>
     hctsp_master: <hctsp.master.Master object at 0x7fb09790b400>
     hctsp_uplink: <hctsp.uplink.Uplink object at 0x7fb09790b280>
     asics_map: {}
     Clock phase characteristic: None
     Data phase characteristics: None
   ]

The :code:`group` attribute in terms of the Smx Tester makes no sense, and always equals :code:`0`.
At this step it is already possible to check if expected uplinks are active on given downlink.

Clock phase characterization
============================

The clock phase characterization is done per :code:`setup_element`.

.. code-block:: python
   :caption: Clock phase characterization.

   for se in setup_elements:
       se.characterize_clock_phase()

.. code-block:: python
   :caption: Example :code:`setup_element` print after clock phase characterization.

   Setup element:
     group: 0
     downlink: 0
     uplinks: [0]
     clock_phase_iterator: <smx_tester.smx_tester.SmxTester.ClockPhaseIter object at 0x7fb4fe8a93a0>
     data_phase_iterator: <smx_tester.smx_tester.SmxTester.DataPhaseIter object at 0x7fb4f7e58910>
     hctsp_master: <hctsp.master.Master object at 0x7fb4f7e58400>
     hctsp_uplink: <hctsp.uplink.Uplink object at 0x7fb4f7e58280>
     asics_map: {}
     Clock phase characteristic:
       Optimal phase: 72
       Window length: 74
       Eye windows:
         Uplink  0: ______________________________XXXXXX____________________________________________
     Data phase characteristics: None

:code:`characterize_clock_phase()` does not set the clock phase, it must be done explicitly.
This is done to enable easy support for predefined setups.
To initialize the clock phase simply call the :code:`initialize_clock_phase()` method.

.. code-block:: python
   :caption: Clock phase initialization.

   se.initialize_clock_phase()

Data phases characterization
=============================

The data phases characterization is also done per :code:`setup_element`.

.. code-block:: python
   :caption: Data phases characterization.

   for se in setup_elements:
       se.characterize_data_phases()

.. code-block:: python
   :caption: Example :code:`setup_element` print after data phases characterization.

   Setup element:
     group: 0
     downlink: 0
     uplinks: [0]
     clock_phase_iterator: <smx_tester.smx_tester.SmxTester.ClockPhaseIter object at 0x7fc167a933a0>
     data_phase_iterator: <smx_tester.smx_tester.SmxTester.DataPhaseIter object at 0x7fc161042910>
     hctsp_master: <hctsp.master.Master object at 0x7fc161042400>
     hctsp_uplink: <hctsp.uplink.Uplink object at 0x7fc161042280>
     asics_map: {}
     Clock phase characteristic:
       Optimal phase: 73
       Window length: 75
       Eye windows:
         Uplink  0: _______________________________XXXXX____________________________________________
     Data phase characteristics:
       Uplink 0:
         Optimal phase: 37
         Window length: 47
         Eye window: _________XXXXX_______________________________________________XXX

:code:`characterize_data_phases()` does not set the data phases, it must be done explicitly.
This is done to enable easy support for predefined setups.
To initialize the clock phase simply call the :code:`initialize_data_phases()` method.

.. code-block:: python
   :caption: Data phases initialization.

   se.initialize_data_phases()

Uplinks map discovery
=====================

The Smx Tester is capable of automatically discovering the ASICs uplink map.
This is also done per :code:`setup_element`.

.. code-block:: python
   :caption: Uplinks map discovery.

   for se in setup_elements:
       se.scan_smx_asics_map()

.. code-block:: python
   :caption: Example :code:`setup_element` print after uplinks map discovery.

   Setup element:
     group: 0
     downlink: 0
     uplinks: [0]
     clock_phase_iterator: <smx_tester.smx_tester.SmxTester.ClockPhaseIter object at 0x7fefabbcc3a0>
     data_phase_iterator: <smx_tester.smx_tester.SmxTester.DataPhaseIter object at 0x7fefa517b910>
     hctsp_master: <hctsp.master.Master object at 0x7fefa517b400>
     hctsp_uplink: <hctsp.uplink.Uplink object at 0x7fefa517b280>
     asics_map: {1: {'ASIC uplinks': [0], 'uplinks': [0], 'uplinks map': [(0, 0)]}}
     Clock phase characteristic:
       Optimal phase: 72
       Window length: 74
       Eye windows:
         Uplink  0: ______________________________XXXXXX____________________________________________
     Data phase characteristics:
       Uplink 0:
         Optimal phase: 38
         Window length: 47
         Eye window: __________XXXXX_______________________________________________XX

The format of :code:`asics_map` dictionary is described in the :code:`hctsp` package, file :code:`setup_element.py`.

Elink synchronization
=====================

Elink synchronization (the SOS -> K28.1 -> EOS -> FRAME sequence) is also done per :code:`setup_element`.

.. code-block:: python
   :caption: Elink synchronization.

   for se in setup_elements:
       se.synchronize_elink()

SMX elink masks write
=====================

Writing proper SMX elink masks is not part of the synchronization procedure, and has to be called explicitly.
This step is also done per :code:`setup_element`.

.. code-block:: python
   :caption: SMX elink mask write.

   for se in setup_elements:
       se.write_smx_elink_masks()

This procedure requires the :code:`asics_map` attribute to be set correctly.
This implies that the :code:`scan_smx_asics_map()` method must be called before writing the elink mask registers or the setup has to be correctly predefined.

Smx class
=========

Once all procedures related with the :code:`setup_element` are done, one can create objects representing the SMXes.
There is a special method :code:`smxes_from_setup_element()`, that creates a list of Smx objects based on the :code:`setup_element`.
Smx class supports 2 user friendly methods for acessing the registers.

.. code-block:: python
   :caption: Smx class methods.

   def read(self, row, col):
       ...

   def write(self, row, col, data):
       ...

Both read and write are always checked for potential errors.
In case of any error proper Exception is raised.
The exceptions are described in the :code:`hctsp` package, file :code:`ack_monitor.py`.

If one wants to perform broadcast write, or read/write without checking the acknowledgment, then it has to been done explicitly by creating proper :code:`Command` and issuing it with desired command slot.

Predefining setup
=================

Setup can be predefined by manually creating proper :code:`setup_element` objects.

In case of working with predefined setups, one has to explicitly set uplinks mask.
By default all uplinks are disabled.
Following listing shows how to enable all uplinks for given :code:`setup_element`.

.. code-block:: python
   :caption: Explicit uplinks enabling.

   se.hctsp_uplink.set_uplinks_mask(se.uplinks)

Hit frame data
==============

Each uplink in the Smx Tester has its own FIFO, storing the hit frame data.
The :code:`SmxTester` has following methods for handling these FIFOs.

.. code-block:: python
   :caption: SmxTester methods for handling hit frame FIFOs.

   def fifos_data_count(self, uplinks=None):
       """Get count of data in hit frame FIFOs.
       Parameters
       ----------
       uplinks : None | int | [int] (The default is None)
           Uplink number or list of uplinks numbers.
           None is equivalent to all uplinks.
       Returns
       -------
       count : int
       """
       ...

   def read_fifos_data(self, count, uplinks=None):
       """Read data from hit frame FIFOs.
       Parameters
       ----------
       count : int
           Count of data to be read.
       uplinks : None | int | [int] (The default is None)
           Uplink number or list of uplinks numbers.
           None is equivalent to all uplinks.
       Returns
       -------
       List of data.
       """
       ...

   def reset_hit_fifos(self, uplinks=None):
       """Reset hit frame FIFOs.
       Parameters
       ----------
       uplinks : None | int | [int] (The default is None)
           Uplink number or list of uplinks numbers.
           None is equivalent to all uplinks.
       """
       ...

To operate on the FIFOs of given Smx, one can get the uplinks numbers from the :code:`uplinks` attribute of the :code:`Smx` object.
