.. SMX Tester documentation master file, created by
   sphinx-quickstart on Fri Nov 27 14:57:11 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SMX Tester's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   overview
   usage
   example
