#!/usr/bin/python
from setproctitle import setproctitle
import numpy as np, uhal, sys, time, collections, os, signal
sys.path.append("../lib")
sys.path.append("../")
from multiprocessing import Process, Queue, Pipe
from ROOT import TTree, TFile
from PFAD_lib import init_cmd, initialise, set_trim
np.set_printoptions(threshold=sys.maxsize)

def tsintegrity(frame):
    """This function checks the integrity of a TS_MSB frame """
    ts0 = (frame >> 4) & 0x3F  # Extract three TS from frame
    ts1 = (frame >> 10) & 0x3F # they all must be the same
    ts2 = (frame >> 16) & 0x3F
    if not ts0 ^ ts1 and not ts1 ^ ts2:
        return True
    else:
        return False

def save_chunk(tree, frames, nxyter, PCTimestamp, time_taken, time1, lastTsMsb,
        ADC_count, PacketSize, Channel, Amplitude, Timestamp,
        XyterNumber, TimeElapsed, timestamp_gbt, GBTTimestamp):

    """Whole-chunk save of frames to root"""
    # Get Indices of ADC hits. If there are none, cancel this block
    # TESTING
    # for i in frames:
    #     print("{0:b}".format(i))

    ADC_list      = np.where((frames & 0x800000) == 0)[0]
    TS_MSB_list   = np.where((frames & 0xC00000) == 0xC00000)[0] # Indices of TS_MSBs
    TS_MSB_values = (frames[TS_MSB_list] & 0x00FC00) >> 2        # List of 13:8 TS_MSB
    n_ADC_hits = ADC_list.size
    n_TS_MSB_hits = TS_MSB_list.size
    if n_ADC_hits:
        TS_reset_list = np.where((frames & 0xC0FC00) == 0xC00000)[0] # Indices of resets
        TS_MSB_frame  = np.repeat(np.concatenate(([lastTsMsb[nxyter]], TS_MSB_values)),
                np.diff(np.concatenate(([0], TS_MSB_list, [len(frames)]))))
        TS_offset_list = np.arange(0, 16384 * (TS_reset_list.size + 1), 16384)
        TS_offset_frame = np.repeat(TS_offset_list, np.diff(np.concatenate(([0],
            TS_reset_list, [len(frames)]))))

        PacketSize[0]  = n_ADC_hits #n_ADC_hits
        XyterNumber[0] = nxyter
        TimeElapsed[0] = time_taken
        PCTimestamp[0] = time1
        GBTTimestamp[0] = timestamp_gbt 
        adcframes      = frames[ADC_list] # get list of frames containing ADC hits
        tsmsbframes    = frames[TS_MSB_list] # list of tsmsb frames 
        TS_ADC_values  = (TS_offset_frame + TS_MSB_frame)[ADC_list]
        Channel[0:n_ADC_hits]   = ((adcframes & 0x7F0000) >> 16)
        Amplitude[0:n_ADC_hits] = ((adcframes & 0x00F800) >> 11)
        Timestamp[0:n_ADC_hits] = np.where(
                #Check TS<9:8> for MSB and LSB
                (TS_MSB_frame[ADC_list] & 0x300) == ((adcframes >> 1) & 0x300),
                (TS_ADC_values + ((adcframes & 0x0001FE ) >> 1)),
                (TS_ADC_values + ((adcframes & 0x0001FE ) >> 1) - (1 << 8)) & 0x3FFF)

        tree.Fill()

    if TS_MSB_values.size: #np.any returns false for i.e. [0]
        lastTsMsb[nxyter] = TS_MSB_values[-1]
        ADC_count[nxyter] += n_ADC_hits

def queuetoroot(durationS, conn4, frame_buffer,info):
    setproctitle("python MTsaveRawData.py writer process")
    """Processes the Queue from takedata() threads, and writes root file. """
    runid = time.strftime( "%Y%m%d_%Hh%Mm%Ss", time.localtime())
    print("Starting data taking @ {}".format(runid))

    #################################################
    file_index = 0
    rootfilename = "../data_files/root_files/PFAD{}_{}_for{}s_{}.root"\
            .format(info,runid, durationS / 1E9, file_index)
    ofile = TFile(rootfilename, 'RECREATE')
    assert ofile.IsOpen()

    tree = TTree('otree', 'fifo tree')
    # print("1 tree type is: ", type(tree))

    FIFO_depth = 4098

    PacketSize  = np.array([0], dtype = np.int32)
    Channel     = np.zeros((FIFO_depth), dtype = np.int32)
    Amplitude   = np.zeros((FIFO_depth), dtype = np.int32)
    Timestamp   = np.zeros((FIFO_depth), dtype = np.int32)
    XyterNumber = np.array([0], dtype = np.int32)
    TimeElapsed = np.array([0], dtype = np.int64)
    PCTimestamp = np.array([0], dtype = np.int64)
    GBTTimestamp = np.array([0], dtype = np.int64)
    tree.Branch('PacketSize',  PacketSize,  'PacketSize/I')
    tree.Branch('Channel',     Channel,     'Channel[PacketSize]/I')
    tree.Branch('Amplitude',   Amplitude,   'Amplitude[PacketSize]/I')
    tree.Branch('Timestamp',   Timestamp,   'Timestamp[PacketSize]/I')
    tree.Branch('XyterNumber', XyterNumber, 'XyterNumber/I')
    tree.Branch('TimeElapsed', TimeElapsed, 'TimeElapsed/l')
    tree.Branch('PCTimestamp', PCTimestamp, 'PCTimestamp/l')
    tree.Branch('GBTTimestamp', GBTTimestamp, 'GBTimestamp/l')
    ################################################
    timestart, num_xyter = conn4.recv()
    ADC_count = np.zeros((num_xyter),  dtype = np.int32)
    lastTsMsb = np.zeros((num_xyter),  dtype = np.int32)

    print("Writer thread ready to start")
    processed_events, potential_misses = 0, 0

    while True:
        if os.path.getsize(rootfilename) > 3.9e9:
            print(os.path.getsize(rootfilename)," ", rootfilename, " ")
            # print("2 tree type is: ", type(tree))
            # print("INSIDE")
            tree.Write()
            ofile.Close(
                    )
            timestop = time.perf_counter_ns()
            rootsavename = "../data_files/root_files/PFAD{}_{}_for{:.2f}s_{}.root"\
                    .format(info,runid, (timestop - timestart) / 1E9, file_index)
            os.system("mv {} {}".format(rootfilename, rootsavename))
            print("Wrote TTree! to: \nroot {}".format(rootsavename))
            file_index += 1
            tree = TTree("otree", 'fifo tree')
            PacketSize  = np.array([0], dtype = np.int32)
            Channel     = np.zeros((FIFO_depth), dtype = np.int32)
            Amplitude   = np.zeros((FIFO_depth), dtype = np.int32)
            Timestamp   = np.zeros((FIFO_depth), dtype = np.int32)
            XyterNumber = np.array([0], dtype = np.int32)
            TimeElapsed = np.array([0], dtype = np.int64)
            PCTimestamp = np.array([0], dtype = np.int64)
            GBTTimestamp = np.array([0], dtype = np.int64)
            tree.Branch('PacketSize',  PacketSize,  'PacketSize/I')
            tree.Branch('Channel',     Channel,     'Channel[PacketSize]/I')
            tree.Branch('Amplitude',   Amplitude,   'Amplitude[PacketSize]/I')
            tree.Branch('Timestamp',   Timestamp,   'Timestamp[PacketSize]/I')
            tree.Branch('XyterNumber', XyterNumber, 'XyterNumber/I')
            tree.Branch('TimeElapsed', TimeElapsed, 'TimeElapsed/l')
            tree.Branch('PCTimestamp', PCTimestamp, 'PCTimestamp/l')
            tree.Branch('GBTTimestamp', GBTTimestamp, 'GBTimestamp/l')
            rootfilename = "../data_files/root_files/PFAD{}_{}_for{}s_{}.root"\
                    .format(info,runid, durationS / 1E9, file_index)
            ofile = TFile(rootfilename, 'RECREATE')
            assert ofile.IsOpen()

        # print("3 tree type is: ", type(tree))
        buffer = frame_buffer.get()
        # print("????\n {}\n".format(buffer))
        for nxyter, item, time1, time0, ts_gbt in buffer:
            if nxyter == -1:
                break
            time_taken = time1 - time0

            # print("4 tree type is: ", type(tree))
            save_chunk(tree, np.array(item), nxyter, PCTimestamp, time_taken,
                    time1, lastTsMsb, ADC_count, PacketSize, Channel,
                    Amplitude, Timestamp, XyterNumber, TimeElapsed, ts_gbt, GBTTimestamp)
            processed_events += len(item)
            if len(item) == FIFO_depth:
                # print("Full frame after {:.2f}s.".format(
                #     (time.perf_counter_ns() - timestart) / 1E9))
                potential_misses += 1
        else:
            continue
        break
    timestop = time.perf_counter_ns()
    print("Data of {:.2f}s, {} frames. {} Overfull events".format(
        (timestop - timestart) / 1E9, processed_events,
        potential_misses))
    print("ADC cnt   {}".format(ADC_count))
    tree.Write()
    ofile.Close()

    # File written before the end:
    rootsavename = "../data_files/root_files/PFAD{}_{}_for{:.2f}s_{}.root"\
            .format(info,runid, (timestop - timestart) / 1E9, file_index)
    os.system("mv {} {}".format(rootfilename, rootsavename))
    print("Wrote TTree! to: \nroot {}".format(rootsavename))

def takedata(durationS, conn1, conn3):
    setproctitle("python MTsaveRawData.py getter process")
    print("Reading thread for XYTER starting...")
    smxes, smx_tester = init_cmd()
    num_xyter = 33   # Maximum 33 xyter chips

    uplinks = []
    for smx in smxes:
        initialise(smx)
        set_trim(smx)
        uplinks.append(smx.uplinks[0])
    print("We use uplinks {}".format(uplinks))

    smx_tester.reset_hit_fifos() # First, emptying the buffers

    timestart = time.perf_counter_ns()
    # The times for each uplink / XYTER
    time_buffer = [collections.deque([timestart], maxlen = 2)] * num_xyter
    conn3.send((timestart, num_xyter))
    time_reset = 0
    number_of_reset = 0
    rate_time_i = time.perf_counter_ns()
    rate_time_f = 10*1e9
    rate_time = 0
    rate = [0]*len(uplinks)
    while (time.perf_counter_ns() - timestart) < durationS:
        for u in uplinks:
            timeperf0 = time.perf_counter_ns()
            #time_buffer[u].append(time.perf_counter_ns())
            try:
                frameNb    = smx_tester.rawfifos[u].fdata.reg.used.read()
                time_buffer[u].append(time.perf_counter_ns())
                data_count = smx_tester.rawfifos[u].fdata.reg.data.read_fifo(
                        frameNb)
                timestamp_0 = smx_tester.handle.system_timestamp_0.read()
                timestamp_1 = smx_tester.handle.system_timestamp_1.read()
                timestamp_gbt = (timestamp_1 << 32) | timestamp_0

            except Exception as e:
                print("{} \n  Caught exception, moving on... \n".format(e))
                time_pause = time.perf_counter_ns()
                smxes, smx_tester = init_cmd()
                for smx in smxes:
                    initialise(smx)
                    set_trim(smx)
                smx_tester.reset_hit_fifos()
                local_time_reset = time.perf_counter_ns() - time_pause
                time_reset += local_time_reset 
                number_of_reset += 1
                durationS = durationS + local_time_reset
                print("Reinitialisation time: {} s, Number of reset: , Total time lost reinitialising \
                        : {} s\n".format(local_time_reset, number_of_reset, time_reset/1.e9))

            if data_count:
                conn1.send((u, data_count, time_buffer[u][1], time_buffer[u][0], timestamp_gbt))

                rate_time = time.perf_counter_ns() - rate_time_i 
                ADC_list      = np.where((np.array(data_count) & 0x800000) == 0)[0]
                # print(ADC_list)
                n_ADC_hits = ADC_list.size
                TS_MSB_list   = np.where((np.array(data_count) & 0xC00000) == 0xC00000)[0] # Indices of TS_MSBs
                n_TS_hits = TS_MSB_list.size
                if(n_ADC_hits):
                   if(rate_time < rate_time_f):
                       rate[u] = rate[u] + 1 

            #if len(data_count) > 4000:
            #     timeperf3 = time.perf_counter_ns()
            #     print("{} ev, cycle {:.1f}us, after {:.2f}s".format(
            #           len(data_count), (timeperf3 - timeperf0) / 1E3,
            #           (time.perf_counter_ns() - timestart - time_reset)/1E9))

            if rate_time > rate_time_f:
                rate_ul = -1
                print("\n")
                print("DAQ has been running for: {}".format((time.perf_counter_ns() - timestart)/1.e9))
                for i in rate:
                    rate_ul+=1
                    print("UL: {}, count: {}, rate: {} /s".format(rate_ul, i, i/(rate_time)*1e9))
                print("\n")
                rate_time_i = time.perf_counter_ns()
                rate_time = 0
                rate = [0]*len(uplinks)

    conn1.send((-1, [], 0, 0, 0))
    print("Getter Thread done. Time lost: {} s".format(time_reset/1.e9))

def io_balancer(conn2, frame_buffer):
    """This function takes the pipe (conn2) from the data taking thread, and
    fills it gently into the data_writing queue (frame_buffer). """
    print("IO balancer starts.")
    setproctitle("python MTsaveRawData.py IO balancer")
    while True:
        buffer = []
        for i in range(20):
            buffer.append(conn2.recv())
            if buffer[-1][0] == -1:
                break
        frame_buffer.put(buffer)
        if buffer[-1][0] == -1:
            break
    print("Io_balancer exits.")

# if __name__ == '__main__':
def DAQ_start(Duration = -1, info = ""):
    if (info != ""): info = "_" + info
    durationS = 0
    # By default will stop after a day (24h)
    if (Duration == -1):
        durationS = 86400 
    else:
        durationS = Duration

    setproctitle("python MTsaveRawData.py main process")
    durationS = 1E9 * durationS
    print(durationS)
    assert durationS > 0
    conn1, conn2 = Pipe() # Pipe for transferring Data, Getter -> IO balancer
    conn3, conn4 = Pipe() # Init Pipe to transfer timestart, Getter -> Writer
    frame_buffer = Queue()
    # Create new signal handle to make child processes ignore SIGINTs
    original_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)

    get_process = Process(target = takedata, args = (durationS, conn1, conn3))
    io_process = Process(target = io_balancer, args = (conn2, frame_buffer))
    writer_process = Process(target = queuetoroot, args = (durationS, conn4, frame_buffer,info))

    get_process.start()
    io_process.start()
    writer_process.start()
    # Get regular handle for SIGINTs back
    signal.signal(signal.SIGINT, original_sigint_handler)
    try:
        get_process.join()
        io_process.join()
        writer_process.join()
    except KeyboardInterrupt:
        get_process.terminate()
        print("\nCTRL+C\'ed. Terminated getter process.")
        conn1.send((-1, [], 0, 0,0))
