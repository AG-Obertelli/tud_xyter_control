#!/usr/bin/python
import sys, os, time, numpy as np, matplotlib.pyplot as plt
import progressbar
from os import path
sys.path.append("../lib")
sys.path.append("../")
import Environment as Env

import uhal, agwb
from ROOT import TFile, TTree, TH1F, TCanvas, TGraph, TF1, TAxis
from smx_tester import *

LINK_BREAK = 0b00001
LINK_ASIC = 0b00001

def PFAD_update_configuration():
    config_filename = Env.config_and_calibration_path + 'Chosen_lists.txt'
    list_ASICs = 'default'
    XYTER_registers_config = 'default'
    list_trim_calibration = 'default'
    assert os.path.exists(config_filename)
    with open(config_filename) as f:
        for line in f:
            x, y = line.split()
            if (x == 'list_ASICs'): list_ASICs = y
            if (x == 'XYTER_registers_config'): XYTER_registers_config = y
            if (x == 'list_trim_calibration'): list_trim_calibration = y
        
    list_of_ASICs_path = Env.config_and_calibration_path + 'list_of_ASICs/{}/'.format(list_ASICs)
    XYTER_registers_config_path = Env.config_and_calibration_path + 'register_config/{}/'.format(XYTER_registers_config)
    trim_calibration_path = Env.config_and_calibration_path + 'trim_calibration_files/{}/'.format(list_trim_calibration)

    nul = 50
    #List of ASICs (UL    name    position)
    ASIC_ul = []
    ASIC_name = ["" for i in range(nul)]
    ASIC_position = ["" for i in range(nul)]
    outfilename = list_of_ASICs_path + 'list_of_asics.txt'
    assert os.path.exists(outfilename)
    with open(outfilename) as f:
        for line in f:
            x, y, z= line.split()
            ASIC_ul.append(int(x))
            ASIC_name[int(x)] = y
            ASIC_position[int(x)] = z
        
    #List of registers (UL    name    position)
    registers_130 = np.full((19,nul),0)
    ana_chan_63 = np.full((nul),0)	     
    ana_chan_65 = np.full((nul),0) 	     
    ana_chan_67 = np.full((nul),0) 	    
    outfilename = XYTER_registers_config_path + 'list_register_config.txt'
    assert os.path.exists(outfilename)
    with open(outfilename) as f:
        for line in f:
            row = line.split()
            ul = int(row[0])
            for reg in range(0,19):
                registers_130[reg][ul]=row[reg+1]
            ana_chan_63[ul]=row[20]
            ana_chan_65[ul]=row[21]
            ana_chan_67[ul]=row[22]
            
    return list_ASICs, XYTER_registers_config, list_trim_calibration, ASIC_ul, ASIC_name, ASIC_position, registers_130, ana_chan_63, ana_chan_65, ana_chan_67 	    
    # list_ASICs, XYTER_registers_config, list_trim_calibration, ASIC_ul, ASIC_name, ASIC_position, registers_130, ana_chan_63, ana_chan_65, ana_chan_67 	= PFAD_update_configuration()
        
def set_clk_phase_data_phase():
    """ this function estabished the connection with the boards"""
    uhal.setLogLevelTo(uhal.LogLevel.WARNING)
    manager = uhal.ConnectionManager("file://"+Env.base_path+"lib/devices.xml")
    ipbus_interface = IPbusInterface(manager, "EMU")
    agwb_top = agwb.top(ipbus_interface, 0)
    smx_tester = SmxTester(agwb_top, CLK_80)    # CLK_160, CLK_80, CLK_40
    setup_elements = smx_tester.scan_setup()
    print (setup_elements)
    time_start = time.time()
    outfilename="{}clock_phases.txt".format(Env.phases_path)
    outfile = open(outfilename,"w")
    outfile.close()
    outfilename_data="{}data_phases.txt".format(Env.phases_path)
    outfile_data = open(outfilename_data,"w")
    outfile_data.close()
    for se in setup_elements:
        se.characterize_clock_phase(outfilename)
    for se in setup_elements:
        se.characterize_data_phases(outfilename_data)
    time_end = time.time()
    print( "Duration phases: {:.2f}\n".format(time_end - time_start))
        
def init_cmd():
    """ this function estabished the connection with the boards"""
    uhal.setLogLevelTo(uhal.LogLevel.WARNING)
    manager = uhal.ConnectionManager("file://"+Env.base_path+"lib/devices.xml")
    ipbus_interface = IPbusInterface(manager, "EMU")
    agwb_top = agwb.top(ipbus_interface, 0)
    smx_tester = SmxTester(agwb_top, CLK_80)    # CLK_160, CLK_80, CLK_40
    setup_elements = smx_tester.scan_setup()
    #time_start = time.time()
    outfilename="{}clock_phases.txt".format(Env.phases_path)
    outfilename_data="{}data_phases.txt".format(Env.phases_path)
    if ( not path.isfile(outfilename)): print ("please provide the clock_phases.txt !")
    if ( not path.isfile(outfilename_data)): print ("please provide the data_phases.txt !")
    for se in setup_elements:
        se.initialize_clock_phase(outfilename)
    #time_end = time.time()
    #print( "Duration clk phases: {:.2f}\n".format(time_end - time_start))
    for se in setup_elements:
        se.initialize_data_phases(outfilename_data)
    #time_end = time.time()
    #print( "Duration data phases: {:.2f}\n".format(time_end - time_start))
    for se in setup_elements:
        se.scan_smx_asics_map()

    for se in setup_elements:
        se.synchronize_elink()

    for se in setup_elements:
        se.write_smx_elink_masks()

    smxes = []

    for se in setup_elements:
        smxes.extend(smxes_from_setup_element(se))
    print("{} ASICs: ".format(len(smxes)))
    return smxes, smx_tester

def circuit_monitoring(smx, quantity = "VDDM"):
    dac_thr = 0b10000000 #half range: starting point
    if quantity == "VDDM":
        smx.write(130, 20, 7)
    elif quantity == "CSA_BIAS":
        smx.write(130, 20 ,5)
    elif quantity == "TEMP":
        smx.write(130, 20, 3)
    elif quantity == "AUX":
        smx.write(130, 20, 0)
    else:
        print("unknown potential")
    #---set dac_thr
    for i in range(0, 8):
        smx.write(130, 22, dac_thr)
        time.sleep(0.01)
        result = smx.read(192, 34)
        if (result <= 0):
            dac_thr = dac_thr & ~(2 ** (7 - i)) #clear current
        if i != 7:
            dac_thr = dac_thr | (2 ** (6 - i)) #set next bit
    #outvolt = (dac_thr**2)*(-0.000002)+0.0065*dac_thr+0.0192
    outvolt = (dac_thr ** 2) * (-0.0026) + 6.8971 * dac_thr + 3.2474
    if quantity == "TEMP":
        temperature = -0.5412 * outvolt + 379.91
    else:
        temperature = 0
    return [outvolt, dac_thr, temperature]

def circuit_monitoring_logging(smx, path_logging):

    list_ASICs, XYTER_registers_config, list_trim_calibration, ASIC_ul, ASIC_name, ASIC_position, registers_130, ana_chan_63, ana_chan_65, ana_chan_67 	= PFAD_update_configuration()
        
    vddm = circuit_monitoring(smx, "VDDM")
    csabias = circuit_monitoring(smx, "CSA_BIAS")
    temperature = circuit_monitoring(smx, "TEMP")
    log.info(" monitoring VDDM:{:10.4f} mV".format(vddm[0]))
    log.info(" monitoring CSA_BIAS:{:10.4f} mV".format(csabias[0]))
    print(" monitoring TEMP:{:10.4f} degC ({:10.4f} mV, dac_thr: {:6d})".\
           format(temperature[2], temperature[0], temperature[1]))
    f= open(path_logging, "a")
    runid = time.strftime( "%y%m%d_%H%M%S", time.localtime())
    line = "{} ASIC: {:3d}  ul: {:3d}  {:20s}  VDDM:{:10.4f}  mV		"\
           "CSA_BIAS:{:10.4f}  mV		TEMP: {:10.4f} degC".format(runid,
           smx.uplinks[0],smx.uplinks[0],ASIC_name[smx.uplinks[0]],
           vddm[0], csabias[0], temperature[2])
    f.write(str(line))
    f.write("\n")
    f.close()

def initialise(smx):

    list_ASICs, XYTER_registers_config, list_trim_calibration, ASIC_ul, ASIC_name, ASIC_position, registers_130, ana_chan_63, ana_chan_65, ana_chan_67 	= PFAD_update_configuration()
    print('Initialising at index {}'.format(smx.uplinks[0]))
    for ch in range(0, 128):
        smx.write(ch, 63, ana_chan_63[smx.uplinks[0]])
        smx.write(ch, 65, ana_chan_65[smx.uplinks[0]])
        smx.write(ch, 67, ana_chan_67[smx.uplinks[0]])

    # Global DAC settings
    for i, val in enumerate(registers_130):
        smx.write(130, i, val[smx.uplinks[0]])

    enable_TS_in_dummy = False
    enable_TS_MSB_change = False

    smx.write(192, 3, 0x1 + (enable_TS_in_dummy << 2) +
                            (enable_TS_MSB_change << 3)) # Enable the channel mask
    for reg in range(4, 13):
      smx.write( 192, reg, 0X0) # Channel Mask each column does 10 channels
    smx.write(192, 13, 0b1100) # Channel Mask <3:0> : 129-126

    # Reset of counters, fifos, AFE
    smx.write(192, 2, 0b101010) # <1>: channel fifos, <3>: front-end channels
                                 # <5>: front-end ADC counter
    smx.write(192, 2, 0)       # Releasing this reset

    # Global gate to 0
    smx.write(130, 11, 0x0)  # Enabling the Channels-readout from backend
    smx.write(192, 27, 0)    # reset status
    smx.write(192, 24, 0)    # reset FIFO almost full counter
    smx.write(192, 30, 0)    # reset event-missed counter

    # Disable channels #
    XYTER_registers_config_path = Env.config_and_calibration_path + 'register_config/{}/'.format(XYTER_registers_config)
    outfilename = XYTER_registers_config_path + 'disable_channels/{}.txt'.format(ASIC_name[smx.uplinks[0]])
    list_disabled = []
    if (os.path.exists(outfilename)):
        with open(outfilename) as f:
            for line in f:
                row = line.split()
                if (row[0] != "#"): 
                    list_disabled.append(int(row[0]))
    group = [[] for iq in range(0,10)]
    reg_value = [0 for ic in range(0,10)]

    #print(group)
    for i, ch in enumerate(list_disabled):
        Q = ch // 14
        R = ch % 14
        column = 4 + Q
        group[Q].append(R)
    for iq in range(0,10):
        value = 0
        for ir in range(len(group[iq])):
            value = value + (1 << group[iq][ir])
            #print(iq,"   ",ir,"   ",value)
        reg_value[iq] = value
        #print(iq,"   ",value)
    #print(group)
    for column in range (0,10):
        smx.write(192,column+4,reg_value[column])
    print("Disabled channels: ",list_disabled)
           
def read_registers(smx):
    list_ASICs, XYTER_registers_config, list_trim_calibration, ASIC_ul, ASIC_name, ASIC_position, registers_130, ana_chan_63, ana_chan_65, ana_chan_67 	= PFAD_update_configuration()
    # Global DAC settings
    for i, val in enumerate(registers_130):
        print("UL: {} (130,i): {} Read: {}, Written: {}".format( smx.uplinks[0], i, smx.read(130, i) & 0xff, val[smx.uplinks[0]]))
        
    print("chip_addr:", smx.read(192,22) & 0x3fff)
    print("unique address: ",smx.read(192,33) & 0x3fff)
    
def read_trim(smx):
    list_ASICs, XYTER_registers_config, list_trim_calibration, ASIC_ul, ASIC_name, ASIC_position, registers_130, ana_chan_63, ana_chan_65, ana_chan_67 	= PFAD_update_configuration()
    trim_calibration_path = Env.config_and_calibration_path + 'trim_calibration_files/{}/'.format(list_trim_calibration)
    #Filenames such as "FEBC_249"
    filename_trim = ("{}{}.txt".format(trim_calibration_path,ASIC_name[smx.uplinks[0]]))
    #Default values
    trim_values = [[128 for i2 in range(32)] for i3 in range(128)]
    for channel in range(128):
        trim_values[channel][31] = 0
        
    if (os.path.exists(filename_trim)):
        print('Setting trim values at UL {} ... \nfilename_trim: {}'.format(smx.uplinks[0],filename_trim))
        #---Reading the trim values
        data = np.genfromtxt(filename_trim, comments='#')
        #channels = data[:,1]
        trim_values = data[:,][:,2:34]
    else:
        print("NO TRIM CALIBRATION FILE EXISTING at UL {} - use DEFAULT!".format(smx.uplinks[0]))
    #---Set the trim values
    for channel in range(10):
        #print("\nch: ",channel,end="   ")
        for discriminator in range(32):
            #print(int(trim_values[channel][discriminator]),end="   ")
            if (discriminator < 31):
                ADC_comparator = 61 - 2 * discriminator
                print("UL: {} ch: {} disc: {} Read: {}, Written: {}".format( smx.uplinks[0], channel, discriminator, smx.read(channel, ADC_comparator)-3840, int(trim_values[channel][discriminator])))
            else:
                #FAST comparator
                print("UL: {} ch: {} disc: {} Read: {}, Written: {}".format( smx.uplinks[0], channel, discriminator, smx.read(channel, 67)-3840, int(trim_values[channel][discriminator])))


def set_trim(smx):
    list_ASICs, XYTER_registers_config, list_trim_calibration, ASIC_ul, ASIC_name, ASIC_position, registers_130, ana_chan_63, ana_chan_65, ana_chan_67 	= PFAD_update_configuration()
    trim_calibration_path = Env.config_and_calibration_path + 'trim_calibration_files/{}/'.format(list_trim_calibration)
    #Filenames such as "FEBC_249"
    filename_trim = ("{}{}.txt".format(trim_calibration_path,ASIC_name[smx.uplinks[0]]))
    #Default values
    trim_values = [[128 for i2 in range(32)] for i3 in range(128)]
    for channel in range(128):
        trim_values[channel][31] = 0
        
    if (os.path.exists(filename_trim)):
        print('Setting trim values at UL {} ... \nfilename_trim: {}'.format(smx.uplinks[0],filename_trim))
        #---Reading the trim values
        data = np.genfromtxt(filename_trim, comments='#')
        #channels = data[:,1]
        trim_values = data[:,][:,2:34]
    else:
        print("NO TRIM CALIBRATION FILE EXISTING at UL {} - use DEFAULT!".format(smx.uplinks[0]))
    #---Set the trim values
    for channel in range(128):
        #print("\nch: ",channel,end="   ")
        for discriminator in range(32):
            #print(int(trim_values[channel][discriminator]),end="   ")
            if (discriminator < 31):
                ADC_comparator = 61 - 2 * discriminator
                smx.write(channel, ADC_comparator,
                          int(trim_values[channel][discriminator]))	#ADC comp
            else:
                #FAST comparator
                smx.write(channel, 67, int(trim_values[channel][discriminator]))
    return 0

def get_scurves_scan_map(smx, npulses, amplitude_set, ADC_min = 0, ADC_max = 31, ch_min = 0, ch_max = 128, SHslowFS = 0):
    count_map = [[[0 for i1 in range (len(amplitude_set))] for i2 in range(32)] for i3 in range(128)]
    bar = progressbar.ProgressBar(maxval = 100, widgets = [progressbar.Bar('=', 'SCURVES SCAN MAP [', ']'), ' ', progressbar.Percentage()])
    bar.start()

    for iamp, amplitude in enumerate(amplitude_set):
        #print("Pulse amplitude:",amplitude)
        ibar = iamp / len(amplitude_set) * 100
        bar.update(ibar)
        
        amplitude_value = int(amplitude)
        if (amplitude_value<0): amplitude_value=0
        if (amplitude_value>255): amplitude_value=255
        smx.write(130, 4, amplitude_value)
        ngroups = 4
        for group in range(ngroups):
            #---apply the shaping time and group of channels
            group_SHslowFS = ((SHslowFS & 0x3) << 2 | (group & 0x3))
            smx.write(130, 5, group_SHslowFS)
            ch_start = ch_min + (ngroups - ch_min % ngroups)\
                       if (ch_min > group) else 0
            #---Reset ADC counters
            smx.write(192, 2, 32)
            #time.sleep(0.0001)
            #smx.write(192, 2,  0)
            #---trigger pulses (npulses)
            for itmp in range(npulses):
                smx.write(130, 11, 128)
                #time.sleep(0.0001)
                smx.write(130, 11, 0)
            #---reading the counters in each channel / ADC + fast comparator counter
            for channel in range(ch_start + group, ch_max, ngroups):
                for discriminator in range(ADC_min, ADC_max):
                    ADC_counter = 60 - 2 * discriminator
                    count_map[channel][discriminator][iamp] = smx.read(channel, ADC_counter)# & 0xfff
                #---FAST comparator
                count_map[channel][31][iamp] = smx.read(channel, 62) #& 0xfff
    bar.finish()
    return count_map

def get_trim_values_scan_map(smx,npulses,amplitude_set,amplitude_fast,ADC_trim_set,fast_trim_set,ntrim_values,ADC_min = 0,ADC_max = 31,ch_min = 0,ch_max = 128,SHslowFS = 0):
    count_map = [[[0 for i1 in range (ntrim_values)] for i2 in range(32)] for i3 in range(128)]

    bar = progressbar.ProgressBar(maxval = 100, widgets = [progressbar.Bar('=', 'TRIM VALUES SCAN MAP [', ']'), ' ', progressbar.Percentage()])
    bar.start()

    for discriminator in range(ADC_min,ADC_max+1):
        if (discriminator < ADC_max):
            counter = 60 - 2 * discriminator		#row=channel & col=0,2,4...60 comparators counter   0-the highest threshold comparator; 60-the lowest threshold comparator
            comparator = 61 - 2 * discriminator		#row=channel & col=1,3,5...61 comparators trimming  1-the highest threshold comparator; 61-the lowest threshold comparator
            #---set the pulse amplitude
            #print("Pulse amplitude:",int(V_ADC_set[discriminator]))
            ibar = (discriminator - ADC_min) / (ADC_max - ADC_min) * 100
            bar.update(ibar)
            smx.write(130, 4, int(amplitude_set[discriminator - ADC_min]))
        else:
            discriminator = 31
            counter = 62
            comparator = 67
            smx.write(130, 4, int(amplitude_fast))

        #---loop over trim values
        for i_trim_value in range (ntrim_values):
            #---loop over the 4 groups of channels
            ngroups = 4
            for group in range(ngroups):
                #---apply the shaping time and group of channels
                group_SHslowFS = ((SHslowFS & 0x3) << 2 | (group & 0x3))
                smx.write(130, 5, group_SHslowFS)
                ch_start = ch_min + (ngroups - ch_min % ngroups) if (ch_min>group) else 0
                #---set the trim_value for each comparator in each channel
                for channel in range(ch_start + group, ch_max, ngroups):
                    if (discriminator < ADC_max): 
                        trim_value = int(ADC_trim_set[channel][discriminator][i_trim_value])
                        if (trim_value<0): trim_value=0
                        if (trim_value>255): trim_value=255
                    else: 
                        trim_value = int(fast_trim_set[channel][i_trim_value])
                        if (trim_value<0): trim_value=0
                        if (trim_value>63): trim_value=63
                        
                    smx.write(channel,comparator,trim_value)

                #---Reset ADC counters
                smx.write(192, 2, 32)
                #time.sleep(0.05)
                #smx.write(192, 2,  0)
                #time.sleep(0.05)
                #---trigger pulses (npulses)
                for itmp in range(npulses):
                    smx.write(130, 11, 128)
                    #time.sleep(0.05)
                    smx.write(130, 11, 0)
                    #time.sleep(0.05)
                #---reading the counters in each channel / ADC comparator counter
                for channel in range(ch_start + group, ch_max, ngroups):
                    count_map[channel][discriminator][i_trim_value] = smx.read(channel,counter) & 0xfff
    bar.finish()
    return (count_map)

def get_fine_trim_set(count_map, npulses, ADC_trim_set, fast_trim_set, ntrim_values, ntrim_fine_values,ADC_min = 0,ADC_max = 31,ch_min = 0,ch_max = 128):
    fine_ADC_trim_set = [[[128 for i1 in range (ntrim_fine_values)] for i2 in range(ADC_max)] for i3 in range(ch_max)]
    fine_fast_trim_set = [[0 for i1 in range (ntrim_fine_values)] for i3 in range(ch_max)]
    #print("fine_trim_set:  ")
    for channel in range(ch_min,ch_max):
        #print("Ch:",channel)
        #---ADC comparators
        for discriminator in range(ADC_min,ADC_max+1):
            if (discriminator < ADC_max):
                fine_trim_min = min(ADC_trim_set[channel][discriminator])
                fine_trim_max = max(ADC_trim_set[channel][discriminator])
            else:
                #--fast discriminator
                discriminator=31
                fine_trim_min = min(fast_trim_set[channel])
                fine_trim_max = max(fast_trim_set[channel])
                
            if (discriminator < ADC_max): mean_fit, width_fit = fit_dataset_errfc_gaus(ntrim_values, ADC_trim_set[channel][discriminator] , count_map[channel][discriminator],npulses)
            else: mean_fit, width_fit = fit_dataset_errfc_gaus(ntrim_values, fast_trim_set[channel] , count_map[channel][discriminator],npulses)
            
            #for i_trim_value in range(2,ntrim_values-3):
            #    if((count_map[channel][discriminator][i_trim_value-2]+count_map[channel][discriminator][i_trim_value-1]+count_map[channel][discriminator][i_trim_value])/3 < 0.1*npulses \
            #    and (count_map[channel][discriminator][i_trim_value+1]+count_map[channel][discriminator][i_trim_value+2]+count_map[channel][discriminator][i_trim_value+3])/3>=0.1*npulses):
            #        if (discriminator < ADC_max): fine_trim_min = ADC_trim_set[channel][discriminator][i_trim_value]
            #        else: fine_trim_min = fast_trim_set[channel][i_trim_value]
            #    if((count_map[channel][discriminator][i_trim_value-2]+count_map[channel][discriminator][i_trim_value-1]+count_map[channel][discriminator][i_trim_value])/3 < 0.9*npulses \
            #    and (count_map[channel][discriminator][i_trim_value+1]+count_map[channel][discriminator][i_trim_value+2]+count_map[channel][discriminator][i_trim_value+3])/3>=0.9*npulses):
            #        if (discriminator < ADC_max): fine_trim_max = ADC_trim_set[channel][discriminator][i_trim_value+1]
            #        else: fine_trim_max = fast_trim_set[channel][i_trim_value+1]
            
            #if (discriminator < ADC_max): print("Channel: {} Discriminator: {} Mean: {} Width: {} FitMean: {} FitWidth: {} FitAmplitude: {}".format(channel, discriminator,(fine_trim_min+fine_trim_max)/2,fine_trim_max-fine_trim_min, mean_ADC, width_ADC, amplitude_ADC))
            if (mean_fit != 0 and width_fit != 0):
                fine_trim_min = int(mean_fit - 20)
                fine_trim_max = int(mean_fit + 21)
            if (fine_trim_min<0):
                fine_trim_min=0
                fine_trim_max=40
            if (fine_trim_max>255 and discriminator < ADC_max):
                fine_trim_max=255
                fine_trim_min=215
            if (fine_trim_max>63 and discriminator > ADC_max):
                fine_trim_max=63
                fine_trim_min=23
            fine_trim_step = int((fine_trim_max-fine_trim_min)/ntrim_fine_values)
            if (fine_trim_step < 1):
                fine_trim_step = 1
                fine_trim_max = fine_trim_min + ntrim_fine_values * fine_trim_step
            if (discriminator < ADC_max): fine_ADC_trim_set[channel][discriminator] = [i1 for i1 in range (fine_trim_min,fine_trim_max,fine_trim_step)]
            else: fine_fast_trim_set[channel] = [i1 for i1 in range (fine_trim_min,fine_trim_max,fine_trim_step)]
    return (fine_ADC_trim_set, fine_fast_trim_set)

def get_trim_values(smx,count_map,npulses,ADC_trim_set,fast_trim_set,ntrim_values,ADC_min,ADC_max,ch_min,ch_max):

    list_ASICs, XYTER_registers_config, list_trim_calibration, ASIC_ul, ASIC_name, ASIC_position, registers_130, ana_chan_63, ana_chan_65, ana_chan_67 	= PFAD_update_configuration()
    trim_calibration_path = Env.config_and_calibration_path + 'trim_calibration_files/{}/'.format(list_trim_calibration)
    date  = time.strftime("%y%m%d")
    phys_ul = smx.uplinks[0]
    filename_trim = ("{}Logging_trim_scan_{}.txt".format(trim_calibration_path,
                                       ASIC_name[phys_ul]))
    print(filename_trim)

    trimfile = open(filename_trim, "w+")
    trimfile.write("# TRIM SCAN  \n#\n")
    
    trim_values = [[128 for i2 in range(32)] for i3 in range(128)]
    for channel in range(128):
        trim_values[channel][31] = 0
        
    for channel in range(ch_min,ch_max):
        trimfile.write("ch: {:3d}   \n".format(channel))
        for discriminator in range(ADC_min,ADC_max+1):
            if (discriminator < ADC_max):
                fine_trim = 128
                mean_fit, width_fit = fit_dataset_errfc_gaus(ntrim_values, ADC_trim_set[channel][discriminator] , count_map[channel][discriminator],npulses)
                if (mean_fit != 0 and width_fit != 0): fine_trim = int(mean_fit)
                if (fine_trim<0 or fine_trim>255): fine_trim=128
                trimfile.write("disc: {:3d}   ".format(discriminator))
                for itrim in range(ntrim_values):
                    trimfile.write("({} {})    ".format(ADC_trim_set[channel][discriminator][itrim],count_map[channel][discriminator][itrim]))
                trimfile.write("trim value: {}\n".format(fine_trim))
            else:
                discriminator=31
                fine_trim = 0
                mean_fit, width_fit = fit_dataset_errfc_gaus(ntrim_values, fast_trim_set[channel] , count_map[channel][discriminator],npulses)
                if (mean_fit != 0 and width_fit != 0): fine_trim = int(mean_fit)
                if (fine_trim<0 or fine_trim>63): fine_trim=0
                trimfile.write("disc: {:3d}   ".format(discriminator))
                for itrim in range(ntrim_values):
                    trimfile.write("({} {})    ".format(fast_trim_set[channel][itrim],count_map[channel][discriminator][itrim]))
                trimfile.write("trim value: {}\n".format(fine_trim))

            #for i_trim_value in range(ntrim_values-1):
            #    if(count_map[channel][discriminator][i_trim_value]<0.5*npulses and count_map[channel][discriminator][i_trim_value+1]>=0.5*npulses):
            #        if (discriminator < ADC_max): fine_trim = int((ADC_trim_set[channel][discriminator][i_trim_value] + ADC_trim_set[channel][discriminator][i_trim_value+1])/2)
            #        else: fine_trim = int((fast_trim_set[channel][i_trim_value] + fast_trim_set[channel][i_trim_value+1])/2)
            trim_values[channel][discriminator] = fine_trim
            #if (discriminator < ADC_max and fine_trim == 128):
            #    print(channel, discriminator, count_map[channel][discriminator])
            #    print(ADC_trim_set[channel][discriminator])
            #if (discriminator == 31 and fine_trim == 0):
            #    print(channel, discriminator, count_map[channel][discriminator])
            #    print(fast_trim_set[channel])
    return trim_values

def write_trim_files(smx,trim_values, calibration_pulse_amplitude_min, calibration_pulse_amplitude_max, calibration_pulse_amplitude_fast):

    list_ASICs, XYTER_registers_config, list_trim_calibration, ASIC_ul, ASIC_name, ASIC_position, registers_130, ana_chan_63, ana_chan_65, ana_chan_67 	= PFAD_update_configuration()
    trim_calibration_path = Env.config_and_calibration_path + 'trim_calibration_files/{}/'.format(list_trim_calibration)
    date  = time.strftime("%y%m%d")
    phys_ul = smx.uplinks[0]
    filename_trim = ("{}{}.txt".format(trim_calibration_path,
                                       ASIC_name[phys_ul]))
    print(filename_trim)

    trimfile = open(filename_trim, "w+")
    assert os.path.exists(filename_trim)
    trimfile.write("# TRIM CALIBRATION    {}    pulse_ampl_min={}    pulse_"\
                   "ampl_max={}    pulse_ampl_fast={}\n#\n".format(
                   ASIC_name[phys_ul], calibration_pulse_amplitude_min,
                   calibration_pulse_amplitude_max, calibration_pulse_amplitude_fast))
    # writing trim values on file
    for ch in range (128):
        trimfile.write("ch: {:3d}   ".format(ch))
        print("ch: {:4d}".format(ch), end = "    ")
        for d in range(0,32):
            trimfile.write("{:5d}".format(trim_values[ch][d]))
            print("{:4d}".format(trim_values[ch][d]), end = " ")
        trimfile.write("\n")
        print("")
    trimfile.close()

def trim_calibration(smx):
    #---calibration pulse amplitude applied in register: row=130;col=4
    calibration_pulse_amplitude_min = 40
    calibration_pulse_amplitude_max = 200
    #--- threshold for fast discriminator
    calibration_pulse_amplitude_fast = calibration_pulse_amplitude_min
    #---ADC discriminators (31 ADC discriminators/channel)
    ADC_min = 0  			#corresponding to calibration_pulse_amplitude_min
    ADC_max = 31			    #corresponding to calibration_pulse_amplitude_max
    #---channels:
    ch_min = 0
    ch_max = 128				# 128 channels from 0 to 127
    #---number of pulses
    npulses = 10
    npulses_fine = 20
    #---slow shaper
    SHslowFS = 0
    #-
    ntrim_values = 8
    ntrim_fine_values = 20
    #---trim values
    ADC_trim_min = 40
    ADC_trim_max = 200
    #---trim values fast
    fast_trim_min = 0
    fast_trim_max = 64
    
    normalization = 0
    
    config_filename = Env.config_and_calibration_path + 'trim_calibration_files/trim_calibration_settings.txt'
    assert os.path.exists(config_filename)
    with open(config_filename) as f:
        for line in f:
            x, y = line.split()
            if (x == 'calibration_pulse_amplitude_min'): calibration_pulse_amplitude_min = int(y)
            if (x == 'calibration_pulse_amplitude_max'): calibration_pulse_amplitude_max = int(y)
            if (x == 'calibration_pulse_amplitude_fast'): calibration_pulse_amplitude_fast = int(y)
            if (x == 'ADC_min'): ADC_min = int(y)
            if (x == 'ADC_max'): ADC_max = int(y)
            if (x == 'ch_min'): ch_min = int(y)
            if (x == 'ch_max'): ch_max = int(y)
            if (x == 'npulses'): npulses = int(y)
            if (x == 'npulses_fine'): npulses_fine = int(y)
            if (x == 'SHslowFS'): SHslowFS = int(y)
            if (x == 'ntrim_values'): ntrim_values = int(y)
            if (x == 'ntrim_fine_values'): ntrim_fine_values = int(y)
            if (x == 'ADC_trim_min'): ADC_trim_min = int(y)
            if (x == 'ADC_trim_max'): ADC_trim_max = int(y)
            if (x == 'fast_trim_min'): fast_trim_min = int(y)
            if (x == 'fast_trim_max'): fast_trim_max = int(y)
            if (x == 'normalization'): normalization = int(y)

    #---linear distribution of discriminators #---pulse height array for each discriminator
    amplitude_set = [(calibration_pulse_amplitude_min + (discriminator - ADC_min) *(calibration_pulse_amplitude_max - calibration_pulse_amplitude_min) / (ADC_max - ADC_min)) for discriminator  in range (ADC_min,ADC_max)]
    amplitude_fast = calibration_pulse_amplitude_fast

    ADC_trim_step = int((ADC_trim_max-ADC_trim_min) / ntrim_values)
    ADC_trim_max = ADC_trim_min + ntrim_values * ADC_trim_step
    fast_trim_step = int((fast_trim_max-fast_trim_min)/ntrim_values)
    fast_trim_max = fast_trim_min + ntrim_values * fast_trim_step
#-------------------------------------------------------------------------------------------------------------------
    ADC_trim_set = [[[i1 for i1 in range (ADC_trim_min,ADC_trim_max,ADC_trim_step)] for i2 in range(ADC_max)] for i3 in range(ch_max)]
    fast_trim_set = [[i1 for i1 in range (fast_trim_min,fast_trim_max,fast_trim_step)] for i3 in range(ch_max)]
#-------------------------------------------------------------------------------------------------------------------
    print("Trim calibration...")
    #print(ADC_trim_set)
    #print(fast_trim_set)
    print("primary scan")
    count_map = get_trim_values_scan_map(smx,npulses,amplitude_set,amplitude_fast,ADC_trim_set,fast_trim_set,ntrim_values,ADC_min,ADC_max,ch_min,ch_max,SHslowFS)
    
    if (normalization == 1):
        for channel in range(128):
            for discriminator in range(32): 
                for i in range(len(count_map[channel][discriminator])):
                    if (count_map[channel][discriminator][i] > npulses):count_map[channel][discriminator][i] = npulses+1
                    
    ADC_trim_fine_set,fast_trim_fine_set = get_fine_trim_set(count_map, npulses, ADC_trim_set, fast_trim_set, ntrim_values, ntrim_fine_values,ADC_min,ADC_max,ch_min,ch_max)
    #print(ADC_trim_fine_set)
    #print(fast_trim_fine_set)
    print("fine scan")
    count_map = get_trim_values_scan_map(smx,npulses_fine,amplitude_set,amplitude_fast,ADC_trim_fine_set,fast_trim_fine_set,ntrim_fine_values,ADC_min,ADC_max,ch_min,ch_max,SHslowFS)
    
    if (normalization == 1):
        for channel in range(128):
            for discriminator in range(32): 
                for i in range(len(count_map[channel][discriminator])):
                    if (count_map[channel][discriminator][i] > npulses):count_map[channel][discriminator][i] = npulses
                    
    trim_values = get_trim_values(smx,count_map,npulses_fine,ADC_trim_fine_set,fast_trim_fine_set,ntrim_fine_values,ADC_min,ADC_max,ch_min,ch_max)
    write_trim_files(smx, trim_values, calibration_pulse_amplitude_min, calibration_pulse_amplitude_max, calibration_pulse_amplitude_fast)

def check_trim(smx):
    """ creates a count map for the whole range of amplitudes in order to check
        the linearity of the ADC counters; it is done with few points but for
        all ADC comparators; print the count map
    """
    list_ASICs, XYTER_registers_config, list_trim_calibration, ASIC_ul, ASIC_name, ASIC_position, registers_130, ana_chan_63, ana_chan_65, ana_chan_67 	= PFAD_update_configuration()
            
    amplitude_min=10
    amplitude_max=250
    amplitude_step=10
    #---ADC discriminators (31 ADC discriminators/channel)
    ADC_min = 0
    ADC_max = 31			# 31 ADC comparators + 1 FAST comparator
    #---channels:
    ch_min = 0
    ch_max = 128				# 128 channels from 0 to 127
    #---number of pulses
    npulses = 10
    #---slow shaper
    SHslowFS = 0
    
    normalization = 0
    histogramming = 1
    
    config_filename = Env.log_path + 'Check_trim/settings.txt'
    assert os.path.exists(config_filename)
    with open(config_filename) as f:
        for line in f:
            x, y = line.split()
            if (x == 'amplitude_min'): amplitude_min = int(y)
            if (x == 'amplitude_max'): amplitude_max = int(y)
            if (x == 'amplitude_step'): amplitude_step = int(y)
            if (x == 'ADC_min'): ADC_min = int(y)
            if (x == 'ADC_max'): ADC_max = int(y)
            if (x == 'ch_min'): ch_min = int(y)
            if (x == 'ch_max'): ch_max = int(y)
            if (x == 'npulses'): npulses = int(y)
            if (x == 'normalization'): normalization = int(y)
            if (x == 'histogramming'): histogramming = int(y)
            
    n_values = (amplitude_max-amplitude_min)/amplitude_step
    amplitude_set = [amplitude for amplitude in range(amplitude_min, amplitude_max,amplitude_step)]
    count_map = get_scurves_scan_map(smx, npulses, amplitude_set,ADC_min, ADC_max, ch_min, ch_max,SHslowFS)
    
    if (normalization ==1):
        for channel in range(128):
            for discriminator in range(32): 
                for i in range(len(count_map[channel][discriminator])):
                    if (count_map[channel][discriminator][i] > npulses):count_map[channel][discriminator][i] = npulses
                    
    runid = time.strftime("%y%m%d_%H%M%S", time.localtime())
    outfilename = Env.log_path + "Check_trim/Files/Check_trim_logging_{}_{}.txt".format(ASIC_name[smx.uplinks[0]], runid)
    outfile = open(outfilename, "w")
    
    if (histogramming == 1): 
        title = "Check_trim for {}".format(ASIC_name[smx.uplinks[0]])
        os.mkdir(Env.log_path + "Check_trim/Files/Check_trim_{}_{}".format(ASIC_name[smx.uplinks[0]], runid))
        file_name = Env.log_path + "Check_trim/Files/Check_trim_{}_{}/Check_trim_{}_{}".format(ASIC_name[smx.uplinks[0]], runid,ASIC_name[smx.uplinks[0]], runid)
        plot_check_trim(count_map, n_values, npulses, ch_min,ch_max,title, file_name)
    
    for channel in range (ch_min, ch_max):
        #print("ch: ", channel)
        outfile.write("ch: {}\n".format(channel))
        for discriminator in range(ADC_min, ADC_max + 1):
            if (discriminator == ADC_max):
                #print("fast:    ", end = "")
                outfile.write("fast:    ")
                discriminator = 31
            else:
                #print("disc: {:3d}".format(discriminator), end = "")
                outfile.write("disc: {:3d}".format(discriminator))
            for iamp in range(len(amplitude_set)):
                #print("{:5d}".format(count_map[channel][discriminator][iamp]),end = "")
                outfile.write("{:5d}".format(count_map[channel][discriminator][iamp]))
                         
            #print("")
            outfile.write("\n")

   
def fit_dataset_errfc_gaus(n,x,y0,maxy): 
    y = y0
    #errfc
    max_value = 0
    minx=x[0]
    maxx=x[len(x)-1]
    mean = (minx+maxx)/2
    width = 0
    if(y[0]<0.3*maxy):
        g = TGraph()
        f = TF1("f","[0]*(1+erf((x-[1])/[2]))",0,255)
        for i in range(n):
            if (max_value == 0 and y[i]>0.98*maxy): max_value == 1
            if (max_value == 1):
                y[i] = maxy
            g.SetPoint(i,x[i],y[i])
        #print(i,"    ",x[i],"   ",y[i])
        #print(g.GetN())
        #c = TCanvas("c","c",1800,1400)
        #g.SetMarkerStyle(20)
        #g.SetMarkerSize(5)
        #g.Draw("AP")

        f.SetParLimits(1,minx,maxx)
        f.SetParLimits(2,-100,100)
        g.Fit("f","MQ")
        #c.Print(Env.log_path + "Fast_ENC/test.png")
        amplitude = 2* f.GetParameter(0)
        mean = f.GetParameter(1)
        width = f.GetParameter(2)
        
        if (amplitude > maxy*1.5 or amplitude < maxy*1.5 or mean > maxx or mean < minx):
        #gaus
        #print("fit with gauss")
            g2 = TGraph()
            f2 = TF1("f2","gaus",0,255)
            f2.SetParLimits(1,minx,maxx)
            f2.SetParLimits(2,-100,100)
        
            for i in range(1,n):
                g2.SetPoint(i,(x[i-1]+x[i])/2,y[i]-y[i-1])
            g2.Fit("f2","MQ")
            mean = f2.GetParameter(1)
            width = f2.GetParameter(2)
    
            if (mean > maxx or mean < minx):
                mean = (minx+maxx)/2
                width = 0
    #print ("Mean erf:{}   Width erf:{}   Amplitude erf:{}    Mean gaus:{}   Width gaus:{}".format(mean,width,amplitude,mean2,width2))
    return mean, width

def plot_check_trim(count_map0, n, maxy, ch_min, ch_max,title,file_name):
    N = maxy * n
    count_map = count_map0
    c = TCanvas("c","c", 1800, 800)
    for channel in range(ch_min, ch_max):
        c.cd()
        h = TH1F(title+"_ch{}".format(channel), title+"_ch{}".format(channel), 33, 0, 33)
        for discriminator in range(32):
            h.SetBinContent(discriminator+1, sum(count_map[channel][discriminator])/N)
        h.SetLineWidth(4)
        h.SetStats(0)
        h.SetLineColor(2)
        h.Draw("")
        c.Print(file_name+"_ch{}.png".format(channel))

def plot_channels_histo(y,title,file_name):
    c = TCanvas(title, title, 1800, 800)
    c.cd()
    h = TH1F(title, title, 129, 0, 129)
    for i in range(128):
        h.SetBinContent(i+1, y[i])
    h.Draw()
    c.Print(file_name)


def fast_ENC(smx):
    """ gives a fast ENC analysis using the first 4 ADC comparators' noise
    """
    
    list_ASICs, XYTER_registers_config, list_trim_calibration, ASIC_ul, ASIC_name, ASIC_position, registers_130, ana_chan_63, ana_chan_65, ana_chan_67 	= PFAD_update_configuration()
            
    print("Fast ENC")
    amplitude_min=5
    amplitude_max=250
    amplitude_step=3
    #---ADC discriminators (31 ADC discriminators/channel)
    ADC_min = 0
    ADC_max = 4			# 31 ADC comparators + 1 FAST comparator
    #---channels:
    ch_min = 0
    ch_max = 128				# 128 channels from 0 to 127
    #---number of pulses
    npulses = 10
    #---slow shaper
    SHslowFS = 0
    
    normalization = 0
    histogramming = 1
    Muchmode = 0
    
    config_filename = Env.log_path + 'Fast_ENC/settings.txt'
    assert os.path.exists(config_filename)
    with open(config_filename) as f:
        for line in f:
            x, y = line.split()
            if (x == 'amplitude_min'): amplitude_min = int(y)
            if (x == 'amplitude_max'): amplitude_max = int(y)
            if (x == 'amplitude_step'): amplitude_step = int(y)
            if (x == 'ADC_min'): ADC_min = int(y)
            if (x == 'ADC_max'): ADC_max = int(y)
            if (x == 'ch_min'): ch_min = int(y)
            if (x == 'ch_max'): ch_max = int(y)
            if (x == 'npulses'): npulses = int(y)
            if (x == 'SHslowFS'): SHslowFS = int(y)
            if (x == 'normalization'): normalization = int(y)
            if (x == 'histogramming'): histogramming = int(y)
            if (x == 'MUCHmode'): MUCHmode = int(y)
    
    ENC_total = 0
    count_total = 0
    print("channels:[{} {}]".format(ch_min,ch_max))
    amplitude_set = [amplitude for amplitude in range(amplitude_min, amplitude_max,amplitude_step)]     
    count_map = get_scurves_scan_map(smx, npulses, amplitude_set,ADC_min, ADC_max, ch_min, ch_max,SHslowFS)
                                     
    if (normalization ==1):
        for channel in range(128):
            for discriminator in range(32): 
                for i in range(len(count_map[channel][discriminator])):
                    if (count_map[channel][discriminator][i] > npulses):count_map[channel][discriminator][i] = npulses
                    
    runid = time.strftime("%y%m%d_%H%M%S", time.localtime())
    x = [i for i in range(128)]
    y = [-1 for i in range(128)]
    outfilename = Env.log_path + "Fast_ENC/Files/Fast_ENC_logging_{}_{}.txt".format(ASIC_name[smx.uplinks[0]], runid)
    outfile = open(outfilename, "w")
    for channel in range (ch_min, ch_max):
        print("ch: {:3d}".format(channel), end = "    ")
        #print("ch: {:3d}".format(channel))
        outfile.write("ch: {:3d}    ".format(channel))
        enc_ave = 0
        enc_n = 0
        for discriminator in range(ADC_min, ADC_max):
            # fit and get ENC[discriminator]; add to enc_ave and increase enc_n
            # count_map[channel][discriminator] vs amplitude_set
            count_sum = sum(count_map[channel][discriminator])
            adc, enc = (0, 0)
            if (count_sum > npulses):
                adc,enc = fit_dataset_errfc_gaus(len(amplitude_set), amplitude_set, count_map[channel][discriminator],npulses)
                enc = enc * 349
                if (MUCHmode == 1): enc = enc*6
                if (adc > 0 and enc > 0):
                    enc_ave = enc_ave + enc
                    enc_n = enc_n + 1
            print("({:4.2f} {:4.2f})".format(adc, enc), end = "   ")
            #print("({:4.2f} {:4.2f})".format(adc, enc))
            outfile.write("({:4.2f} {:4.2f})    ".format(adc, enc))
        if (enc_n > 0):
            enc_ave = enc_ave / enc_n
        else:
            enc_ave = -1
        
        print("enc: {:4.2f}".format(enc_ave))
        outfile.write("enc: {:4.2f}\n".format(enc_ave))
        if (enc_ave>200 and enc_ave<10000):
            ENC_total = ENC_total + enc_ave
            count_total = count_total + 1
        y[channel] = enc_ave
    if (histogramming==1):
        title = "Fast ENC for {}".format(ASIC_name[smx.uplinks[0]])
        file_name = Env.log_path + "Fast_ENC/Files/Fast_ENC_{}_{}.png".format(ASIC_name[smx.uplinks[0]], runid)
        for ii in range (len(y)):
            if(y[ii]>10000): y[ii]=10000
        plot_channels_histo(y, title, file_name)
    if (count_total > 0): ENC_total = ENC_total/count_total
    print("ASIC ENC:  ", ENC_total)
    outfile.write("ASIC ENC: {:4.2f}\n".format(ENC_total))


def channel_test(smx):
    """ gives yes/no for the triggering of each channel - first 4 discriminators
        should all trigger with a large amplitude (240)
    """
    
    list_ASICs, XYTER_registers_config, list_trim_calibration, ASIC_ul, ASIC_name, ASIC_position, registers_130, ana_chan_63, ana_chan_65, ana_chan_67 	= PFAD_update_configuration()
            
            
    amplitude = 254
    #---ADC discriminators (31 ADC discriminators/channel)
    ADC_min = 0
    ADC_max = 4			# 31 ADC comparators + 1 FAST comparator
    #---channels:
    ch_min = 0
    ch_max = 128				# 128 channels from 0 to 127
    #---number of pulses
    npulses = 10
    #---slow shaper
    SHslowFS = 0
    
    config_filename = Env.log_path + 'Channel_test/settings.txt'
    assert os.path.exists(config_filename)
    with open(config_filename) as f:
        for line in f:
            x, y = line.split()
            if (x == 'amplitude'): amplitude = int(y)
            if (x == 'ADC_min'): ADC_min = int(y)
            if (x == 'ADC_max'): ADC_max = int(y)
            if (x == 'ch_min'): ch_min = int(y)
            if (x == 'ch_max'): ch_max = int(y)
            if (x == 'npulses'): npulses = int(y)
        
    amplitude_set = [amplitude]    
    count_map = get_scurves_scan_map(smx, npulses, amplitude_set, ADC_min,
                                     ADC_max, ch_min, ch_max, SHslowFS)
    runid = time.strftime("%y%m%d_%H%M%S", time.localtime())
    x = [i for i in range(128)]
    y = [-1 for i in range(128)]
    outfilename = Env.log_path + "Channel_test/Files/Channel_test_logging_{}_{}.txt".format(ASIC_name[smx.uplinks[0]], runid)
    outfile = open(outfilename, "w")
    for channel in range (ch_min, ch_max):
        fired = [0 for i in range(31)]
        print("ch: {:3d}".format(channel), end = "    ")
        outfile.write("ch: {:3d}    ".format(channel))
        for discriminator in range(ADC_min, ADC_max):
            if (count_map[channel][discriminator][0] > 0.9 * npulses):
                print("ok", end = "   ")
                outfile.write("ok    ")
                fired[discriminator] = 1
            else:
                print("-", end = "   ")
                outfile.write("-    ")
                fired[discriminator] = 0
        print("[{:3d}]".format(sum(fired)), end = "   ")
        outfile.write("[{:3d}]    ".format(sum(fired)))
        if (sum(fired) > 0):
            print("GOOD")
            outfile.write("GOOD\n")
        else:
            print("UNRESPONDING")
            outfile.write("UNRESPONDING\n")
        
        y[channel] = sum(fired)
    
    title = "Channel test for {}".format(ASIC_name[smx.uplinks[0]])
    file_name = Env.log_path + "Channel_test/Files/Channel_test_{}_{}.png".format(ASIC_name[smx.uplinks[0]], runid)
    plot_channels_histo(y, title, file_name)

def ENC_scurves_scan(smx):
    """ gives a fast ENC analysis using the first 4 ADC comparators' noise
    """
    
    list_ASICs, XYTER_registers_config, list_trim_calibration, ASIC_ul, ASIC_name, ASIC_position, registers_130, ana_chan_63, ana_chan_65, ana_chan_67 	= PFAD_update_configuration()
            
    print ("ENC - SCURVES scan")
    runid = time.strftime("%y%m%d_%H%M%S", time.localtime())
    time_start = time.time()
    amplitude_min = 0
    amplitude_max = 255
    amplitude_step = 1
    amplitude_n = 255
    #---ADC discriminators (31 ADC discriminators/channel)
    ADC_min = 0
    ADC_max = 31			# 31 ADC comparators + 1 FAST comparator
    #---channels:
    ch_min = 0
    ch_max = 128					# 128 channels from 0 to 127
    #---number of pulses
    npulses = 10
    #---slow shaper
    SHslowFS = 0
    
    MUCHmode = 0 
    normalization = 1
    
    config_filename = Env.log_path + 'S-curve_analysis/settings.txt'
    assert os.path.exists(config_filename)
    with open(config_filename) as f:
        for line in f:
            x, y = line.split()
            if (x == 'amplitude_min'): amplitude_min = int(y)
            if (x == 'amplitude_max'): amplitude_max = int(y)
            if (x == 'amplitude_step'): amplitude_step = int(y)
            if (x == 'amplitude_n'): amplitude_n = int(y)
            if (x == 'ADC_min'): ADC_min = int(y)
            if (x == 'ADC_max'): ADC_max = int(y)
            if (x == 'ch_min'): ch_min = int(y)
            if (x == 'ch_max'): ch_max = int(y)
            if (x == 'npulses'): npulses = int(y)
            if (x == 'SHslowFS'): SHslowFS = int(y)
            if (x == 'MUCHmode'): MUCHmode = int(y)
            if (x == 'normalization'): MUCHmode = int(y)
    
    print("channels:[{} {}]".format(ch_min,ch_max))
    amplitude_set = [amplitude for amplitude in range(amplitude_min, amplitude_max,amplitude_step)]     
    count_map = get_scurves_scan_map(smx, npulses, amplitude_set,ADC_min, ADC_max, ch_min, ch_max,SHslowFS)
    #print(count_map)
    rootfile = TFile.Open( Env.log_path + "S-curve_analysis/Files/" + ASIC_name[smx.uplinks[0]] + "_ENC_scurves_scan_"+ runid + ".root","recreate")
    h_scurve = [ [ TH1F("h_scurve_{}_{}".format(channel,discriminator),"h_scurve_{}_{}".format(channel,discriminator),amplitude_n,0,255) for discriminator in range(32) ] for channel in range(128) ]
    #Histrogram saving
    for channel in range (ch_min, ch_max):
        for discriminator in range(ADC_min, ADC_max+1):
            if (discriminator==ADC_max):
                discriminator=31
            for iamp in range(len(amplitude_set)):
                h_scurve[channel][discriminator].SetBinContent(amplitude_set[iamp],count_map[channel][discriminator][iamp])
            h_scurve[channel][discriminator].Write()
            
    #ENC        
    if (normalization == 1):
        for channel in range(128):
            for discriminator in range(32): 
                for i in range(len(count_map[channel][discriminator])):
                    if (count_map[channel][discriminator][i] > npulses):count_map[channel][discriminator][i] = npulses
    x = [i for i in range(128)]
    y = [-1 for i in range(128)]
    outfilename = Env.log_path + "S-curve_analysis/Files/S-curve_analysis_logging_{}_{}.txt".format(ASIC_name[smx.uplinks[0]], runid)
    outfile = open(outfilename, "w")
    for channel in range (ch_min, ch_max):
        print("ch: {:3d}".format(channel), end = "    ")
        #print("ch: {:3d}".format(channel))
        outfile.write("ch: {:3d}    ".format(channel))
        enc_ave = 0
        enc_n = 0

             
           
        for discriminator in range(ADC_min, ADC_max):
            # fit and get ENC[discriminator]; add to enc_ave and increase enc_n
            # count_map[channel][discriminator] vs amplitude_set
            count_sum = sum(count_map[channel][discriminator])
            adc, enc = (0, 0)
            if (count_sum > npulses):
                adc,enc = fit_dataset_errfc_gaus(len(amplitude_set), amplitude_set, count_map[channel][discriminator],npulses)
                enc = enc * 349
                if (MUCHmode == 1): enc = enc*6
                if (adc > 0 and enc > 0 and discriminator<25 and discriminator>5):
                    enc_ave = enc_ave + enc
                    enc_n = enc_n + 1
            print("({:4.2f} {:4.2f})".format(adc, enc), end = "   ")
            #print("({:4.2f} {:4.2f})".format(adc, enc))
            outfile.write("({:4.2f} {:4.2f})    ".format(adc, enc))
        if (enc_n > 0):
            enc_ave = enc_ave / enc_n
        else:
            enc_ave = -1
        print("enc: {:4.2f}".format(enc_ave))
        outfile.write("enc: {:4.2f}\n".format(enc_ave))
        y[channel] = enc_ave

            
    title = "ENC for {}".format(ASIC_name[smx.uplinks[0]])
    file_name = Env.log_path + "S-curve_analysis/Files/S-curve_analysis_ENC_{}_{}.png".format(ASIC_name[smx.uplinks[0]], runid)
    for ii in range (len(y)):
        if(y[ii]>10000): y[ii]=10000
    plot_channels_histo(y, title, file_name)






    
    
        


