import logging as log
import time
import math
import sys,os, numpy as np

from .hctsp import *
from .command import Command
from .ack_monitor import *

class SetupElement:
    def __init__(self, group, downlink, uplinks, clock_phase_iterator, data_phase_iterator, hctsp_master, hctsp_uplink, asics_map):
        """Class representing single element of the setup (structure) of the boards.

        It represents the lowest level of setup granularity.

        Parameters
        ----------
        group : int
            Index of the group (aka CROB index).
        downlink : int
            Index of the Elink downlink.
        uplinks : [int]
            List of Elink uplinks.
        clock_phase_iterator : iterator
            Iterator returning function for setting clock phase.
            Used during link synchronization.
            Must implement __getitem__.
        data_phase_iterator : iterator
            Iterator returning function for setting data phase.
            Used during link synchronization.
            Must implement __getitem__.
        hctsp_master : hctsp.Master
            Reference to the hctsp.Master object controlling given setup element.
        hctsp_uplink : hctsp.Uplink
            Reference to the hctsp.Uplink connected to given setup element.
        asics_map : dictionary
            Dictionary containing information about ASICs connections.
            The key is the ASIC address, and the value is a dictionary with detailed information.
            The value dictionary has 3 keys: 'ASIC uplinks', 'uplinks', 'uplinks map'.
            The value of `asic_uplinks` is a list with ASICs uplinks numbers.
            The value of `uplinks` is a list of concentration board uplinks numbers.
            The value of `uplinks_map` is list of tuples.
            Each tuple has 2 elements. The first one is the ASIC uplink number,
            and the second one is the concentration board uplink number.
            asic_map argument doesn't have to be provided. The scan_smx_asics_map() method
            can perform the scan and set it automatically for the SMX chips.
        """
        self.group = group
        self.downlink = downlink
        self.uplinks = uplinks
        self.clock_phase_iterator = clock_phase_iterator
        self.data_phase_iterator = data_phase_iterator
        self.hctsp_master = hctsp_master
        self.hctsp_uplink = hctsp_uplink
        self.asics_map = asics_map

        self.clock_phase_characteristic = {"Eye windows" : {}, "Optimal phase" : None, "Window length" : None}
        self.data_phase_characteristics = {"Uplinks": [], "Eye windows" : {}, "Optimal phases" : {}, "Windows lengths" : {}}

    def __str__(self):
        ret = """\nSetup Element:
  Group: {}
  Downlink: {}
  Uplinks: {}
  ASICs Map:""".format(
                self.group,
                self.downlink,
                self.uplinks,
                self.clock_phase_iterator,
                self.data_phase_iterator,
                self.hctsp_master,
                self.hctsp_uplink,
            )

        if self.asics_map:
            ret += "\n"
            for asic_addr in sorted(self.asics_map.keys()):
                ret += "    ASIC address {}: (ASIC uplink, uplink):".format(hex(asic_addr))
                for asic_uplink, uplink in self.asics_map[asic_addr]["uplinks map"]:
                    ret += " ({}, {}),".format(asic_uplink, uplink)
                ret = ret[0:-1]
                ret += "\n"
        else:
            ret += " None\n"

        if self.clock_phase_characteristic["Eye windows"]:
            ret += "  Clock Phase Characteristic:\n"
            ret += "    Optimal Phase: {}\n".format(self.clock_phase_characteristic["Optimal phase"])
            ret += "    Window Length: {}\n".format(self.clock_phase_characteristic["Window length"])
            ret += "    Eye Windows:\n"
            for u, eye in self.clock_phase_characteristic["Eye windows"].items():
                ret += "      Uplink {:2}: {}\n".format(u, eye)
        else:
            ret += "  Clock Phase Characteristic: None\n"

        if self.data_phase_characteristics["Eye windows"]:
            ret += "  Data phase characteristics:\n"
            for i, u in enumerate(self.data_phase_characteristics["Uplinks"]):
                ret += "    Uplink {}:\n".format(u)
                ret += "      Optimal Phase: {}\n".format(self.data_phase_characteristics["Optimal phases"][u])
                ret += "      Window Length: {}\n".format(self.data_phase_characteristics["Windows lengths"][u])
                ret += "      Eye Window: {}\n".format(self.data_phase_characteristics["Eye windows"][u])
        else:
            ret += "  Data Phase Characteristics: None\n"

        return ret

    def __repr__(self):
        return self.__str__()

    def check_sos(self, reset_uplinks=None, check_time=0.1, encoding_mode=SOS):
        """Check SOS detection in Elink uplinks associated with given Elink downlink.

        Parameters
        ----------
        reset_uplinks : None or bool
            If reset_uplinks is None, the list of the uplinks is not modified,
               if True, only the passed links will be left
               if False,  only the failed links will be left.
        check_time : real
            Time in seconds to sleep before checking sos detector status (default 0.1).

        Returns
        -------
        passed : bool, failed_uplinks : [int]

            passed is False if any of the uplinks failed SOS detection. True otherwise.

            failed_uplinks contains the failed uplinks indexes.
        """
        log.info("Checking SOS, encoding_mode: {}".format(mode_to_str(encoding_mode)))
        self.hctsp_master.set_encoding_mode(encoding_mode, self.group, self.downlink)

        self.hctsp_uplink.clear_sequence_detectors(self.uplinks)
        time.sleep(check_time)

        detected_n = {}
        for u in self.uplinks:
            detected_n[u] = self.hctsp_uplink.h.receivers[u].sequence_detectors_status.sos_detected.readfb()
        self.hctsp_uplink.h.dispatch()

        passed = True
        failed_uplinks = []
        for u,v in detected_n.items():
            if v() == 0 :
                passed = False
                failed_uplinks.append(u)
                log.debug(f"SOS not detected for group {self.group}, downlink {self.downlink}, uplink {u}")
            else:
                log.info(f"SOS detected for group {self.group}, downlink {self.downlink}, uplink {u}")


        if reset_uplinks is not None:
            if reset_uplinks:
                oper = "passed"
                self.uplinks = list(filter(lambda x: (x not in failed_uplinks), self.uplinks))
            else:
                oper = "failed"
                self.uplinks = list(filter(lambda x: (x in failed_uplinks), self.uplinks))
            log.info(f"Reassigning uplinks to uplinks which {oper} SOS detection")

        return passed, failed_uplinks

    def characterize_clock_phase(self,outfilename):
        """Scan over the clock phase and set the clock_phase_characteristic attribute."""
        log.info("Scanning clock phase")

        self.hctsp_master.set_encoding_mode(SOS, self.group, self.downlink)
        time.sleep(0.1)
        self.hctsp_master.set_encoding_mode(K28_1, self.group, self.downlink)

        det_stable = [[] for _ in range(len(self.uplinks))]
        for f in self.clock_phase_iterator:
            f()
            self.hctsp_uplink.clear_sequence_detectors(self.uplinks)

            for i,u in enumerate(self.uplinks):
                det_stable[i].append((self.hctsp_uplink.h.receivers[u].sequence_detectors_status.sos_stable0.readfb()))
            self.hctsp_uplink.h.dispatch()

        log.info(f"Clock phase scan results for group {self.group}, downlink {self.downlink}")
        res_all = [True,] * len(self.clock_phase_iterator)
        for i, u in enumerate(self.uplinks):
            is_ok = True
            window = ['_' if item() else 'X' for item in det_stable[i]]
            fcl = [True if item() else False for item in det_stable[i]]
            window = ''.join(window)
            self.clock_phase_characteristic["Eye windows"][u] = window
            res = find_center(fcl)[0]
            log.info(f"Eye window for uplink {u:<2}: {window}\nClock Delay: {res}")
            res_all = [x and y for x,y in zip(res_all,fcl)]
        clk_ok, window_len = find_center(res_all)
        self.clock_phase_characteristic["Optimal phase"] = clk_ok
        self.clock_phase_characteristic["Window length"] = window_len
        print(self.uplinks[0], clk_ok)
        assert os.path.exists(outfilename)
        outfile = open(outfilename, "a")
        outfile.write("{}    {}\n".format(self.uplinks[0], clk_ok))
        outfile.close()

    def initialize_clock_phase(self,outfilename):
        """Initialize Elink clock phase based on the clock_phase_characteristic attribute."""
        phase = 100
        assert os.path.exists(outfilename)
        with open(outfilename) as f:
            for line in f:
                x, y = line.split()
                if (self.uplinks[0]>=int(x) and self.uplinks[0]<int(x)+8): phase=int(y)
        #phase = self.clock_phase_characteristic["Optimal phase"]
        self.clock_phase_characteristic["Optimal phase"] = phase
        log.info("Setting the clock phase to {} for group {}, downlink {}".format(
            phase,
            self.group,
            self.downlink
        ))
        self.clock_phase_iterator[phase]()

    def characterize_data_phases(self, outfilename, clk_phase_var=(0,), stable_req=True):
        """Scan over the uplinks data phases and set the data_phase_characteristics attribute.

        Parameters
        ----------
        clk_phase_var : tuple or vector of integers
            To increase the resolution of data delay scan, it is possible to modify the clock
            phase according to the values given in that argument. That reduces the risk
            that the data metastability region is not detected.
        If stable_req : bool
            If False, the K28.1 character only needs to be detected.
        """
        log.info("Scanning data phases")

        self.hctsp_master.set_encoding_mode(SOS, self.group, self.downlink)
        time.sleep(0.1)
        self.hctsp_master.set_encoding_mode(K28_1, self.group, self.downlink)
        time.sleep(0.1)

        det_stable = [[] for _ in range(len(self.uplinks))]

        for f in self.data_phase_iterator:
            for u in self.uplinks:
                f(uplink=u)
            # Now vary the clock phase
            for clk_var in clk_phase_var:
                self.clock_phase_iterator[(self.clock_phase_characteristic["Optimal phase"] + clk_var) % self.clock_phase_iterator.len]()
                time.sleep(0.01)
                self.hctsp_uplink.clear_sequence_detectors(self.uplinks)
                time.sleep(0.1)
                for i, u in enumerate(self.uplinks):
                    if stable_req:
                        det_stable[i].append(self.hctsp_uplink.h.receivers[u].sequence_detectors_status.k28_1_stable1.readfb())
                    else:
                        det_stable[i].append(self.hctsp_uplink.h.receivers[u].sequence_detectors_status.k28_1_detected.readfb())
                self.hctsp_uplink.h.dispatch()

        log.info(f"Data phase scan results for group {self.group}, downlink {self.downlink}")
        res = []
        self.clock_phase_iterator[self.clock_phase_characteristic["Optimal phase"]]()
        for i, u in enumerate(self.uplinks):
            try:
                window = ['_' if item() else 'X' for item in det_stable[i]]
                fcl = [True if item() else False for item in det_stable[i]]
                window = ''.join(window)
                self.data_phase_characteristics["Eye windows"][u] = window
                data_delay, window_len = find_center(fcl)
                self.data_phase_characteristics["Uplinks"].append(u)
                self.data_phase_characteristics["Optimal phases"][u] = data_delay
                self.data_phase_characteristics["Windows lengths"][u] = window_len
                print(u, data_delay)
                assert os.path.exists(outfilename)
                outfile = open(outfilename, "a")
                outfile.write("{}    {}\n".format(u, data_delay))
                outfile.close()
                # The delay contains the data with clock variations
                data_delay //= len(clk_phase_var)
                log.info(f"Eye window for uplink {u:<2}: {window}\nData delay found: {data_delay}")
            except Exception as e:
                log.warning(f"Group {self.group}, downlink {self.downlink}, uplink {u}: " + str(e))
                pass

    def initialize_data_phases(self,outfilename):
        """Initialize Elink uplinks data phases based on the data_phase_characteristics attribute."""
        for u in self.data_phase_characteristics["Uplinks"]:
            phase = 70
            assert os.path.exists(outfilename)
            with open(outfilename) as f:
                for line in f:
                    x, y = line.split()
                    if (x==u): phase=y
            #phase = self.data_phase_characteristics["Optimal phases"][u]
            self.data_phase_characteristics["Optimal phases"][u] = phase
            #print(phase)
            log.info("Setting the data phase to {} for uplink {}".format(phase, u))
            self.data_phase_iterator[phase](uplink=u)

    def synchronize_elink(self):
        """Perform the SOS -> K28.1 -> EOS -> FRAME initilization sequence."""
        log.info("Performing Elink synchronization")

        set_enc_mode = lambda mode : self.hctsp_master.set_encoding_mode(mode=mode, groups=self.group, downlinks=self.downlink)

        set_enc_mode(SOS)
        time.sleep(0.01)
        set_enc_mode(K28_1)
        time.sleep(0.01)
        set_enc_mode(EOS)
        time.sleep(0.01)
        set_enc_mode(FRAME)

    def scan_smx_asics_map(self):
        """Scan SMX ASICs map and set the asics_map attribute."""
        log.info("Beginning SMX ASICs map scan")

        set_enc_mode = lambda mode : self.hctsp_master.set_encoding_mode(mode=mode, groups=self.group, downlinks=self.downlink)

        set_enc_mode(SOS)
        time.sleep(0.01)
        set_enc_mode(K28_1)
        time.sleep(0.01)
        set_enc_mode(EOS)
        time.sleep(0.01)
        set_enc_mode(FRAME)

        self.hctsp_uplink.set_uplinks_mask(self.uplinks)

        uplinks_map = []

        ack_monitor = self.hctsp_uplink.ack_monitor
        # Clear ACK register before scan.
        try:
            ack_monitor.check_read()
        except:
            pass

        command_slot = self.hctsp_master.software_command_slot
        all_uplinks_to_0_cmd = Command(
            1 << self.group,
            1 << self.downlink,
            7, # No ack concentration
            (WRADDR, WRDATA),
            (25 << 8) | 192,
            0b1111100000
        )
        scan_cmd = Command(
            1 << self.group,
            1 << self.downlink,
            0,
            (WRADDR, WRDATA),
            (25 << 8) | 192,
            0
        )

        # Iterate over ASICs.
        # Scan all adresses except broadcast address.
        for asic in range(0xF):

            # Iterate over uplinks of single ASIC.
            for asic_uplink in range(5):
                log.debug(
                    "Scanning ASIC {}, uplink {}".format(hex(asic), asic_uplink)
                )
                # Stuck all ASICs elinks uplinks to 0.
                command_slot.issue(all_uplinks_to_0_cmd)
                time.sleep(0.01)

                mask = 0b1111100000
                mask = mask ^ (1 << (5 + asic_uplink))
                scan_cmd.reevaluate(data=mask, chip_address=asic)
                command_slot.issue(scan_cmd)
                time.sleep(0.01)

                # Write it one more time to overcome some ASIC limitations.
                try:
                    command_slot.issue(scan_cmd)
                    time.sleep(0.01)
                    ack_monitor.check_write()
                except AckNotReceived:
                    continue

                uplink_number = ack_monitor.last_uplink_number
                log.info(
                    "Adding ASIC {}, ASIC uplink {}, uplink {}".format(hex(asic), asic_uplink, uplink_number)
                )

                if asic not in self.asics_map:
                    self.asics_map[asic] = {}
                    self.asics_map[asic]['ASIC uplinks'] = [asic_uplink]
                    self.asics_map[asic]['uplinks'] = [uplink_number]
                    self.asics_map[asic]['uplinks map'] = [(asic_uplink, uplink_number)]
                else:
                    self.asics_map[asic]['uplinks map'].append((asic_uplink, uplink_number))
        #print(self.asics_map)

    def write_smx_elink_masks(self):
        """Write Elink mask register for SMX. Spadic does not have this register."""
        log.info(f"Writing SMX Elink masks for group {self.group}, downlink {self.downlink}")

        self.hctsp_uplink.set_uplinks_mask(self.uplinks, update_mode=True)

        command_slot = self.hctsp_master.software_command_slot
        ack_monitor = self.hctsp_uplink.ack_monitor
        cmd = Command(
            1 << self.group,
            1 << self.downlink,
            0,
            (WRADDR, WRDATA),
            (25 << 8) | 192,
            0
        )

        for asic, info in self.asics_map.items():
            mask = 0
            for u in info["ASIC uplinks"]:
                mask |= 0x21 << u
            mask ^= 0x3FF
            cmd.reevaluate(data=mask, chip_address=asic)

            log.debug("Writing mask {} for ASIC {}".format(bin(mask), hex(asic)))

            command_slot.issue(cmd)
            time.sleep(0.01)

            # Clear ACK register before check.
            try:
                ack_monitor.check_read()
            except:
                pass

            # Write it one more time and check response.
            command_slot.issue(cmd)
            time.sleep(0.01)
            ack_monitor.check_write()
