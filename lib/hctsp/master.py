import logging as log
from .hctsp import *
from .time_command_slot import TimeCommandSlot
from .software_command_slot import SoftwareCommandSlot


class Master():
    def __init__(self, agwb_handle, elink_clk_mode=CLK_160):
        self.h = agwb_handle
        self.elink_clk_mode = elink_clk_mode

        self.special_time_command_slot = TimeCommandSlot(self.h.special_time_command_slot)

        self.time_command_slots = []
        for i in range (0, 4):
            self.time_command_slots.append(TimeCommandSlot(self.h.time_command_slots[i]))


        self.software_command_slot = SoftwareCommandSlot(self.h.software_command_slot)

    def _read_command_cycle_timestamp(self):
        timestamp = self.h.command_cycle_timestamp_0.read()
        timestamp = (self.h.command_cycle_timestamp_1.read() << 32) + timestamp
        return timestamp

    def synchronize_cycle(self):
        log.info("Synchronizing cycle")

        # Multiplied by 2 because single command consists of 2 control frames.
        modulo_base = 2 * 15
        if self.elink_clk_mode == CLK_80:
            modulo_base = 2 * 30
        elif self.elink_clk_mode == CLK_40:
            modulo_base = 2 * 60

        command_cycle_timestamp = self._read_command_cycle_timestamp()
        log.debug(
            "Before synchronization cycle timestamp = {} (% {} = {})".format(
                command_cycle_timestamp,
                modulo_base,
                command_cycle_timestamp % modulo_base
            )
        )


        diff = modulo_base - command_cycle_timestamp % modulo_base
        if diff == modulo_base:
            log.debug("No need to synchronize")
            return

        self.h.command_cycle_pause_width.write(diff)

        command_cycle_timestamp = self._read_command_cycle_timestamp()
        log.debug(
            "After synchronization cycle timestamp = {} (% {} = {})".format(
                command_cycle_timestamp,
                modulo_base,
                command_cycle_timestamp % modulo_base
            )
        )

        if command_cycle_timestamp % modulo_base != 0:
            raise Exception("Command cycle synchronization failed!")

    def set_encoding_mode(self, mode, groups=None, downlinks=None):
        """Set encoding mode for given Elink downlinks.

        Parameters
        ----------
        mode : one of SOS, K28_1, EOS or FRAME
            Encoding mode.
        groups : int | [int]
            Group number or list of groups numbers.
            None is equivalent to all groups.
        downlinks : int | [int]
            Elink downlink number or list of Elink downlinks numbers.
            None is equivalent to all downlinks.
        """

        import agwb
        assert SOS <= mode <= FRAME

        if groups is None:
            groups = list(range(0, agwb.N_HCTSP_GROUPS))
        elif type(groups) == int:
            groups = [groups]

        if downlinks is None:
            downlinks = list(range(0, agwb.N_HCTSP_DOWNLINKS))
        elif type(downlinks) == int:
            downlinks = [downlinks]

        log.info("Setting encoding mode {} for groups {}, downlinks {}".format(mode_to_str(mode), groups, downlinks))

        for g in groups:
            assert 0 <= g < agwb.N_HCTSP_GROUPS, "Invalid group number"
            for d in downlinks:
                assert 0 <= d < agwb.N_HCTSP_DOWNLINKS, "Invalid Elink downlink number"
                self.h.encoding_modes[g * agwb.N_HCTSP_DOWNLINKS + d].writeb(mode)

        self.h.dispatch()
