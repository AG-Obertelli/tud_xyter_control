import logging as log
from .hctsp import *


class Command():
    def __init__(self, group_mask, downlink_mask, sequence_number, request_types, register_address, data=0, chip_address=0xF):
        assert group_mask < 2**8
        assert downlink_mask < 2**12
        assert 0 <= sequence_number < 15
        assert register_address < 2**15
        assert chip_address <= 0xF
        assert len(request_types) == 2
        for r in request_types:
            assert NO_OP <= r <= RDDATA

        self.group_mask = group_mask
        self.downlink_mask = downlink_mask
        self.sequence_number = sequence_number
        self.request_types = request_types
        self.register_address = register_address
        self.data = data
        self.chip_address = chip_address
        self.payloads = (self.register_address, self.data)
        self.crcs = [None, None]

        self._evaluate()

    def _evaluate(self):
        # chip address | sequence number | request type | request payload
        for i in range(0, 2):
            self.crcs[i] = crc15_d25(
                    (self.chip_address << 21) |
                    ((self.sequence_number + i)<< 17) |
                    (self.request_types[i] << 15) |
                    self.payloads[i]
                )

    def __str__(self):
        return  ("Command {{\n"
            "  group_mask       : {}\n"
            "  downlink mask    : {}\n"
            "  sequence_number  : {}\n"
            "  request_types    : ({}, {})\n"
            "  register address : {} ({})\n"
            "  data             : {} ({})\n"
            "  chip address     : {} ({})\n"
            "  CRCs             : [{}, {}]\n"
            "}}".format(
                bin(self.group_mask),
                bin(self.downlink_mask),
                self.sequence_number,
                request_type_to_str(self.request_types[0]), request_type_to_str(self.request_types[1]),
                self.register_address, bin(self.register_address),
                self.data, bin(self.data),
                self.chip_address, hex(self.chip_address),
                hex(self.crcs[0]), hex(self.crcs[1])
            )
        )

    def reevaluate(self, **kvargs):
        get_arg = lambda name : setattr(self, name, kvargs.pop(name, getattr(self, name)))

        get_arg('group_mask')
        get_arg('downlink_mask')
        get_arg('sequence_number')
        get_arg('request_types')
        get_arg('register_address')
        get_arg('data')
        get_arg('chip_address')
        self.payloads = (self.register_address, self.data)

        if kvargs:
            raise Exception("Unknown parameters: {}".format(kvargs))

        self._evaluate()

    def issue(self, handle):
        for i in range(0,2):
            handle.control_frame[i].writeb(
                (self.crcs[i] << 17) |
                (self.payloads[i] << 2) |
                self.request_types[i]
            )

        handle.control.writeb(
            (self.sequence_number << 24 ) |
            (self.group_mask << 16) |
            (self.downlink_mask << 4) |
            self.chip_address
        )

        handle.dispatch()
