import logging as log
import time
from .hctsp import *

class AckMissed(Exception):
    def __str__(self):
        return "At least one Ack frame missed"


class AckNotReceived(Exception):
    def __str__(self):
        return "Ack frame not received"


class AckInvalidCrc(Exception):
    def __str__(self):
        return "Invalid CRC in Ack frame"


class AckInvalidHeader(Exception):
    def __str__(self):
        return "Invalid header in Ack frame"


class AckNack(Exception):
    def __str__(Exception):
        return "Nack detected in Ack frame"


class AckMonitor():
    def __init__(self, agwb_handle):
        self.h = agwb_handle
        self.last_uplink_number = None

    def _check(self, check_function, timeout):
        """Check for Ack.

        Parameters
        ----------
        check_function
            Check function that should be called.
        timeout : int
            Timeout value in seconds (default None).
            If None, then the Ack is checked only once.
            Otherwise the software keeps polling for the timeout value, or
            returns earlier if valid Ack has been read or exception other
            than AckNotReceived has occurred.
        """
        assert timeout == None or timeout > 0

        start_time = time.time()

        while True:
            try:
                return check_function()
            except AckNotReceived:
                pass
                

            if timeout == None or (time.time() - start_time > timeout):
                raise AckNotReceived()
                

    def check_write(self, timeout=None):
        return self._check(self._check_write, timeout)

    def _check_write(self):
        log.debug("Checking for Ack (write ack)")

        frame = self.h.frame.read()
        frame = self._parse_frame(frame)

        expected_crc = crc4_d20(frame >> 4)
        actual_crc = frame & 0b1111

        if (frame >> 19) == 0b10001:
            if expected_crc == actual_crc:
                return
            else:
                #print("Invalid CRC in Ack frame")
                log.debug("Invalid CRC in Ack frame")
                raise AckInvalidCrc
        elif (frame >> 19) == 0b10010:
                #print("Ack - Nack")
                log.debug("Ack - Nack")
                raise AckNack

        log.debug("Write Ack invalid header {}".format(bin(frame >> 21)))
        #print("Write Ack invalid header {}".format(bin(frame >> 21)))
        raise AckInvalidHeader

    def check_read(self, timeout=None):
        return self._check(self._check_read, timeout)

    def _check_read(self):
        log.debug("Checking for RDdata_ack (read ack)")

        frame = self.h.frame.read()
        frame = self._parse_frame(frame)

        expected_crc = crc3_d21(frame >> 3)
        actual_crc = frame & 0b111

        if (frame >> 21) == 0b101:
            if expected_crc == actual_crc:
                return (frame >> 6) & 0b111111111111111
            else:
                #print("Invalid CRC in RDdata_ack frame")
                log.debug("Invalid CRC in RDdata_ack frame")
                raise AckInvalidCrc

        #print("RDdata_ack invalid header {}".format(bin(frame >> 21)))
        log.debug("RDdata_ack invalid header {}".format(bin(frame >> 21)))
        raise AckInvalidHeader

    def _parse_frame(self, frame):
        missed = frame & 1
        self.last_uplink_number = (frame & (0b111111 << 1)) >> 1
        valid = (frame & (1 << 7)) >> 7
        frame = (frame & (0xFFFFFF << 8)) >> 8

        if missed:
            raise AckMissed()

        if not valid:
            raise AckNotReceived()

        return frame

    def get_detected(self, path):
        """Get the number of detected valid ACKs.

        Useful for checking broadcast writes.
        Currently frames with sequence number equal 3 are detected.
        This means that the write command must have sequence number equal 2.

        Parameters
        ----------
        path : int
            Index of the detection path. There are 2 independent detection paths.
            The first one works for sequence number 2, the second one works for
            sequence number 4.

        Returns
        -------
        count, mask
            Count is the number of detected ACKs (number of '1' in the mask).
            Mask allows checking exactly on which uplinks the ACKs were detected.
        """
        assert 0 <= path <= 1

        detected_0 = self.h.detected_0[path].read()
        detected_1 = self.h.detected_1[path].read()

        mask = (detected_1 << 32) | detected_0

        count = 0
        tmp = mask
        for i in range(64):
            if tmp & 1:
                count += 1
            tmp = tmp >> 1

        return count, mask

    def clear_detected(self, path):
        """Clear detected mask.

        Parameters
        ----------
        path : int
            Index of the detection path. There are 2 independent detection paths.
            The first one works for sequence number 2, the second one works for
            sequence number 4.
        """
        assert 0 <= path <= 1
        self.h.clear_detected[path].write(1)
