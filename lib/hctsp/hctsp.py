import math

CLK_40 = 0
CLK_80 = 1
CLK_160 = 2

SOS = 0
K28_1 = 1
EOS = 2
FRAME = 3

NO_OP  = 0b00
WRADDR = 0b01
WRDATA = 0b10
RDDATA = 0b11


def mode_to_str(mode):
    assert SOS <= mode <= FRAME, "Invalid encoding mode"

    if mode == SOS:
        return "SOS"
    elif mode == K28_1:
        return "K.28.1"
    elif mode == EOS:
        return "EOS"
    elif mode == FRAME:
        return "FRAME"

def request_type_to_str(mode):
    assert NO_OP <= mode <= RDDATA, "Invalid encoding mode"

    if mode == NO_OP:
        return "NO_OP"
    elif mode == WRADDR:
        return "WRADDR"
    elif mode == WRDATA:
        return "WRDATA"
    elif mode == RDDATA:
        return "RDDATA"

def find_center(dta):
    """Given a list of (True, False) values, find and return center of longest "True" sequence.

    Return tuple (center index, longest True sequence length)
    """

    # Find first "False" position
    start = -1
    for i in range(0, len(dta)):
        if dta[i] is False:
            start = i

    if start == -1:
        return int(len(dta) / 2), len(dta)

    # Now we are looking for the longest area of True values
    was_true = False
    longest = -1
    lb = -1
    le = -1
    for i in range(0, len(dta)):
        j = (start + i + 1) % len(dta)
        if dta[j]:
            if was_true:
                # We are in the sequence of True values
                ie = j
                ilen += 1
            else:
                ib = j
                ie = j
                ilen = 1
                was_true = True
        else:
            # End of sequence of True values
            if was_true:
                if ilen > longest:
                    lb = ib
                    le = ie
                    longest = ilen
            was_true = False
    # Please note that it is granted, that we ended scanning at the False value!
    # We started AFTER the last False position, so the last checked value will
    # be that last False position. Therefore it is sure that the "else"
    # sequence will be executed after the last sequence of True values!
    if longest == -1:
        raise Exception("No True values in scanned data!")
    if lb < le:
        res = int((le + lb) / 2)
    else:
        res = int(((lb + le + len(dta)) / 2) % len(dta))

    return res, longest


def mean_circular(data, wrap):
    """
    Find an average value of data representing circular arithmetics (e.g. angles, hours)
    This function uses a popular "mean of angles" method:
                   mean(sum_i_from_1_to_N sin(a[i]))
    mean = arctan2 ---------------------------------
                   mean(sum_i_from_1_to_N cos(a[i]))

    It is important to note that in circular arithmetics mean value is not always
    defined (e.g. mean between 0 and 180 deg is 90 or 270?)
    :param data: vector of values to be averaged
    :param wrap: maximum value possible for data vector (wraparound value)
    :return: mean value
    """
    assert isinstance(data, (list, tuple))

    if 1 == len(data):
        return data[0]

    scale = 2 * math.pi / wrap
    data_rad = [i * scale for i in data]
    cos = 0
    sin = 0
    for val in data_rad:
        cos += math.cos(val)
        sin += math.sin(val)

    cos /= len(data)
    sin /= len(data)
    if sin >= 0 and cos >= 0:
        mean = math.atan(sin/cos)
    elif cos < 0:
        mean = math.atan(sin/cos) + math.pi
    else:  # sin < 0 and cos > 0
        mean = math.atan(sin/cos) + 2 * math.pi

    return mean / scale


def _crc15_d25(data_in, crc_in=0b111111111111111):
    """Used only internally to bootstrap the faster method."""

    # change into lists of bits
    # zfill is used to fill the array with 0 up to vector length
    data_in=[int(n) for n in bin(data_in)[2:].zfill(25)]
    lfsr_q=[int(n) for n in bin(crc_in)[2:].zfill(15)]

    # in python index 0 is the leftmost
    # in vhdl index 0 is the LSB
    # got to swap bit order to match vhdl indices
    data_in=data_in[::-1]
    lfsr_q=lfsr_q[::-1]
    
    # these lines are taken form vhdl fucntion of the same name
    # just the xor symbols and brackets were changed to pythonic
    lfsr_c = [0]*15

    lfsr_c[0]  = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[4] ^ lfsr_q[7] ^ lfsr_q[9] ^ lfsr_q[10] ^ lfsr_q[11] ^ data_in[0] ^ data_in[1] ^ data_in[2] ^ data_in[3] ^ data_in[4] ^ data_in[6] ^ data_in[7] ^ data_in[9] ^ data_in[10] ^ data_in[11] ^ data_in[12] ^ data_in[14] ^ data_in[17] ^ data_in[19] ^ data_in[20] ^ data_in[21]
    lfsr_c[1]  = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[3] ^ lfsr_q[5] ^ lfsr_q[8] ^ lfsr_q[10] ^ lfsr_q[11] ^ lfsr_q[12] ^ data_in[1] ^ data_in[2] ^ data_in[3] ^ data_in[4] ^ data_in[5] ^ data_in[7] ^ data_in[8] ^ data_in[10] ^ data_in[11] ^ data_in[12] ^ data_in[13] ^ data_in[15] ^ data_in[18] ^ data_in[20] ^ data_in[21] ^ data_in[22]
    lfsr_c[2]  = lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[3] ^ lfsr_q[4] ^ lfsr_q[6] ^ lfsr_q[9] ^ lfsr_q[11] ^ lfsr_q[12] ^ lfsr_q[13] ^ data_in[2] ^ data_in[3] ^ data_in[4] ^ data_in[5] ^ data_in[6] ^ data_in[8] ^ data_in[9] ^ data_in[11] ^ data_in[12] ^ data_in[13] ^ data_in[14] ^ data_in[16] ^ data_in[19] ^ data_in[21] ^ data_in[22] ^ data_in[23]
    lfsr_c[3]  = lfsr_q[1] ^ lfsr_q[3] ^ lfsr_q[5] ^ lfsr_q[9] ^ lfsr_q[11] ^ lfsr_q[12] ^ lfsr_q[13] ^ lfsr_q[14] ^ data_in[0] ^ data_in[1] ^ data_in[2] ^ data_in[5] ^ data_in[11] ^ data_in[13] ^ data_in[15] ^ data_in[19] ^ data_in[21] ^ data_in[22] ^ data_in[23] ^ data_in[24]
    lfsr_c[4]  = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[6] ^ lfsr_q[7] ^ lfsr_q[9] ^ lfsr_q[11] ^ lfsr_q[12] ^ lfsr_q[13] ^ lfsr_q[14] ^ data_in[0] ^ data_in[4] ^ data_in[7] ^ data_in[9] ^ data_in[10] ^ data_in[11] ^ data_in[16] ^ data_in[17] ^ data_in[19] ^ data_in[21] ^ data_in[22] ^ data_in[23] ^ data_in[24]
    lfsr_c[5]  = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[7] ^ lfsr_q[8] ^ lfsr_q[10] ^ lfsr_q[12] ^ lfsr_q[13] ^ lfsr_q[14] ^ data_in[1] ^ data_in[5] ^ data_in[8] ^ data_in[10] ^ data_in[11] ^ data_in[12] ^ data_in[17] ^ data_in[18] ^ data_in[20] ^ data_in[22] ^ data_in[23] ^ data_in[24]
    lfsr_c[6]  = lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[3] ^ lfsr_q[8] ^ lfsr_q[9] ^ lfsr_q[11] ^ lfsr_q[13] ^ lfsr_q[14] ^ data_in[2] ^ data_in[6] ^ data_in[9] ^ data_in[11] ^ data_in[12] ^ data_in[13] ^ data_in[18] ^ data_in[19] ^ data_in[21] ^ data_in[23] ^ data_in[24]
    lfsr_c[7]  = lfsr_q[1] ^ lfsr_q[3] ^ lfsr_q[7] ^ lfsr_q[11] ^ lfsr_q[12] ^ lfsr_q[14] ^ data_in[0] ^ data_in[1] ^ data_in[2] ^ data_in[4] ^ data_in[6] ^ data_in[9] ^ data_in[11] ^ data_in[13] ^ data_in[17] ^ data_in[21] ^ data_in[22] ^ data_in[24]
    lfsr_c[8]  = lfsr_q[1] ^ lfsr_q[7] ^ lfsr_q[8] ^ lfsr_q[9] ^ lfsr_q[10] ^ lfsr_q[11] ^ lfsr_q[12] ^ lfsr_q[13] ^ data_in[0] ^ data_in[4] ^ data_in[5] ^ data_in[6] ^ data_in[9] ^ data_in[11] ^ data_in[17] ^ data_in[18] ^ data_in[19] ^ data_in[20] ^ data_in[21] ^ data_in[22] ^ data_in[23]
    lfsr_c[9]  = lfsr_q[0] ^ lfsr_q[2] ^ lfsr_q[8] ^ lfsr_q[9] ^ lfsr_q[10] ^ lfsr_q[11] ^ lfsr_q[12] ^ lfsr_q[13] ^ lfsr_q[14] ^ data_in[1] ^ data_in[5] ^ data_in[6] ^ data_in[7] ^ data_in[10] ^ data_in[12] ^ data_in[18] ^ data_in[19] ^ data_in[20] ^ data_in[21] ^ data_in[22] ^ data_in[23] ^ data_in[24]
    lfsr_c[10] = lfsr_q[0] ^ lfsr_q[2] ^ lfsr_q[3] ^ lfsr_q[4] ^ lfsr_q[7] ^ lfsr_q[12] ^ lfsr_q[13] ^ lfsr_q[14] ^ data_in[0] ^ data_in[1] ^ data_in[3] ^ data_in[4] ^ data_in[8] ^ data_in[9] ^ data_in[10] ^ data_in[12] ^ data_in[13] ^ data_in[14] ^ data_in[17] ^ data_in[22] ^ data_in[23] ^ data_in[24]
    lfsr_c[11] = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[3] ^ lfsr_q[4] ^ lfsr_q[5] ^ lfsr_q[8] ^ lfsr_q[13] ^ lfsr_q[14] ^ data_in[1] ^ data_in[2] ^ data_in[4] ^ data_in[5] ^ data_in[9] ^ data_in[10] ^ data_in[11] ^ data_in[13] ^ data_in[14] ^ data_in[15] ^ data_in[18] ^ data_in[23] ^ data_in[24]
    lfsr_c[12] = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[4] ^ lfsr_q[5] ^ lfsr_q[6] ^ lfsr_q[9] ^ lfsr_q[14] ^ data_in[2] ^ data_in[3] ^ data_in[5] ^ data_in[6] ^ data_in[10] ^ data_in[11] ^ data_in[12] ^ data_in[14] ^ data_in[15] ^ data_in[16] ^ data_in[19] ^ data_in[24]
    lfsr_c[13] = lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[3] ^ lfsr_q[5] ^ lfsr_q[6] ^ lfsr_q[7] ^ lfsr_q[10] ^ data_in[3] ^ data_in[4] ^ data_in[6] ^ data_in[7] ^ data_in[11] ^ data_in[12] ^ data_in[13] ^ data_in[15] ^ data_in[16] ^ data_in[17] ^ data_in[20]
    lfsr_c[14] = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[3] ^ lfsr_q[6] ^ lfsr_q[8] ^ lfsr_q[9] ^ lfsr_q[10] ^ data_in[0] ^ data_in[1] ^ data_in[2] ^ data_in[3] ^ data_in[5] ^ data_in[6] ^ data_in[8] ^ data_in[9] ^ data_in[10] ^ data_in[11] ^ data_in[13] ^ data_in[16] ^ data_in[18] ^ data_in[19] ^ data_in[20]

    # get back to pythonic bit order
    lfsr_c=lfsr_c[::-1]

    # convert list of bits to integer
    out = 0
    for bit in lfsr_c:
        out = (out << 1) | bit

    return out


def crc4_d20( data_in, crc_in=0b1111):

    # change into lists of bits
    # zfill is used to fill the array with 0 up to vector length
    data_in=[int(n) for n in bin(data_in)[2:].zfill(20)]
    lfsr_q=[int(n) for n in bin(crc_in)[2:].zfill(4)]

    # in python index 0 is the leftmost
    # in vhdl index 0 is the LSB
    # got to swap bit order to match vhdl indices
    data_in=data_in[::-1]
    lfsr_q=lfsr_q[::-1]

    # these lines are taken form vhdl fucntion of the same name
    # just the xor symbols and brackets were changed to pythonic
    lfsr_c = [0]*4
    lfsr_c[0] = lfsr_q[2] ^ lfsr_q[3] ^ data_in[0] ^ data_in[3] ^ data_in[4] ^ data_in[6] ^ data_in[8] ^ data_in[9] ^ data_in[10] ^ data_in[11] ^ data_in[15] ^ data_in[18] ^ data_in[19];
    lfsr_c[1] = lfsr_q[0] ^ lfsr_q[2] ^ data_in[0] ^ data_in[1] ^ data_in[3] ^ data_in[5] ^ data_in[6] ^ data_in[7] ^ data_in[8] ^ data_in[12] ^ data_in[15] ^ data_in[16] ^ data_in[18];
    lfsr_c[2] = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[3] ^ data_in[1] ^ data_in[2] ^ data_in[4] ^ data_in[6] ^ data_in[7] ^ data_in[8] ^ data_in[9] ^ data_in[13] ^ data_in[16] ^ data_in[17] ^ data_in[19];
    lfsr_c[3] = lfsr_q[1] ^ lfsr_q[2] ^ data_in[2] ^ data_in[3] ^ data_in[5] ^ data_in[7] ^ data_in[8] ^ data_in[9] ^ data_in[10] ^ data_in[14] ^ data_in[17] ^ data_in[18];

    # get back to pythonic bit order
    lfsr_c=lfsr_c[::-1]

    # convert list of bits to integer
    out = 0
    for bit in lfsr_c:
        out = (out << 1) | bit
 
    return out

def crc3_d21( data_in, crc_in=0b111):

    # change into lists of bits
    # zfill is used to fill the array with 0 up to vector length
    data_in=[int(n) for n in bin(data_in)[2:].zfill(21)]
    lfsr_q=[int(n) for n in bin(crc_in)[2:].zfill(3)]

    # in python index 0 is the leftmost
    # in vhdl index 0 is the LSB
    # got to swap bit order to match vhdl indices
    data_in=data_in[::-1]
    lfsr_q=lfsr_q[::-1]

    # these lines are taken form vhdl fucntion of the same name
    # just the xor symbols and brackets were changed to pythonic
    lfsr_c = [0]*3
    lfsr_c[0] = lfsr_q[0] ^ data_in[0] ^ data_in[2] ^ data_in[3] ^ data_in[4] ^ data_in[7] ^ data_in[9] ^ data_in[10] ^ data_in[11] ^ data_in[14] ^ data_in[16] ^ data_in[17] ^ data_in[18];
    lfsr_c[1] = lfsr_q[1] ^ data_in[0] ^ data_in[1] ^ data_in[2] ^ data_in[5] ^ data_in[7] ^ data_in[8] ^ data_in[9] ^ data_in[12] ^ data_in[14] ^ data_in[15] ^ data_in[16] ^ data_in[19];
    lfsr_c[2] = lfsr_q[2] ^ data_in[1] ^ data_in[2] ^ data_in[3] ^ data_in[6] ^ data_in[8] ^ data_in[9] ^ data_in[10] ^ data_in[13] ^ data_in[15] ^ data_in[16] ^ data_in[17] ^ data_in[20];

    # get back to pythonic bit order
    lfsr_c=lfsr_c[::-1]

    # convert list of bits to integer
    out = 0
    for bit in lfsr_c:
        out = (out << 1) | bit
 
    return out


_crc15_d25_cx  = _crc15_d25(0)
_crc15_d25_ct = []
for i in range(25):
    _crc15_d25_ct.append(_crc15_d25_cx ^ _crc15_d25(1<<i))


def crc15_d25(val):
    rval = _crc15_d25_cx
    mask = 0x1
    for i in range(25):
        rval ^= _crc15_d25_ct[i] if val&mask else 0
        mask <<= 1
    return rval


if __name__ == '__main__':
    print("CRC15_D25: {0:015b}".format(_crc15_d25(0x0)))
    print("CRC4_D20:  {0:04b}".format(crc4_d20(0x0)))
    print("CRC3_D21:  {0:03b}".format(crc3_d21(0x0)))

    import time

    range_ = 2**20

    slow_crcs = []
    start_time = time.time()
    for i in range(range_):
        slow_crcs.append(_crc15_d25(i))
    end_time = time.time()
    print("Slow result : {}".format(end_time - start_time))

    fast_crcs = []
    start_time = time.time()
    for i in range(range_):
        fast_crcs.append(crc15_d25(i))
    end_time = time.time()
    print("Fast result : {}".format(end_time - start_time))

    assert slow_crcs == fast_crcs
