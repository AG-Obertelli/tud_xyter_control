import os
import logging as log

from .ack_monitor import AckMonitor


class Uplink:
    def __init__(self, agwb_handle):
        self.h = agwb_handle

        self.ack_monitor = AckMonitor(self.h.ack_monitor)

    def clear_sequence_detectors(self, uplinks=None):
        """Clear sequence detectors in uplink.

        Parameters
        ----------
        uplinks : int | [int]
            Uplink number or list of uplinks numbers.
            None is equivalent to all uplinks.
        """
        import agwb
        if uplinks is None:
            uplinks = list(range(0, agwb.N_HCTSP_UPLINKS))
        elif type(uplinks) == int:
            uplinks = [uplinks]

        log.debug("Clearing sequence detectors for uplinks: {}".format(uplinks))

        for u in uplinks:
            assert 0 <= u < agwb.N_HCTSP_UPLINKS, "Invalid Elink uplink number"
            self.h.receivers[u].clear_sequence_detectors.writeb(1)

        self.h.dispatch()

    def set_uplinks_mask(self, uplinks=None, update_mode=None):
        """Set uplinks mask.

        Parameters
        ----------
        uplinks : int | [int]
            Uplink number or list of uplink numbers.
            None is equivalent to all uplinks.
        update_mode : None | boolean
            Defines how the uplinks argument is treated
            None  - the new mask is created, only the uplinks present in the list are enabled
            True  - the uplinks present in the list are enabled (the mask is updated accordingly)
            False - the uplinks present in the list are disabled (the mask is updated accordingly)
        """
        import agwb
        if uplinks is None:
            uplinks = list(range(0, agwb.N_HCTSP_UPLINKS))
        elif type(uplinks) == int:
            uplinks = [uplinks]

        if update_mode is not None:
            # We need the old mask (it will be updated)
            m0 = self.h.uplinks_mask_0.readb()
            m1 = self.h.uplinks_mask_1.readb()
            oldmask = m0() | (m1() << 32)

        mask = 0
        for u in uplinks:
            assert 0 <= u < agwb.N_HCTSP_UPLINKS, "Invalid uplink number"
            mask |= 1 << u

        if update_mode is not None:
            if update_mode:
                log.info("Enabling uplinks {}".format(uplinks))
                mask |= oldmask
            else:
                log.info("Disabling uplinks {}".format(uplinks))
                mask = oldmask & ~mask
        else:
            log.info("Setting uplinks mask {}".format(uplinks))

        self.h.uplinks_mask_0.writeb(mask & (2**32 - 1))
        self.h.uplinks_mask_1.writeb(mask >> 32)

        self.h.dispatch()
