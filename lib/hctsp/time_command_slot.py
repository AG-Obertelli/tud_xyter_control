import logging as log
from .hctsp import *

class TimeCommandSlot:
    def __init__(self, agwb_handle):
        self.h = agwb_handle

    def issue(self, timestamp, period, command):
        assert timestamp < 2**32
        assert period < 2**32

        log.debug("Issuing command - timestamp {}, period {}, {}".format(timestamp, period, command))

        self.h.timestamp.writeb(timestamp)
        self.h.period.writeb(period)

        command.issue(self.h)

    def disarm(self):
        log.debug("Disarming")
        self.h.timestamp.write(0)
