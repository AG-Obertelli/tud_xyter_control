import logging as log
from .hctsp import *

class SoftwareCommandSlot:
    def __init__(self, agwb_handle):
        self.h = agwb_handle

    def issue(self, command):
        log.debug("Issuing command - {}".format(command))

        command.issue(self.h)
