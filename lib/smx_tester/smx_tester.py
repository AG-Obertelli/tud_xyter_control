import logging as log
import os
import random
import time
from pprint import pformat

import agwb
from hctsp import *

from .elinks import Elinks
from .utils import *

from .raw_fifo import *

class SmxTester:
    """Class representing SMX tester board.

    Single instance represents single physical board.
    """

    def __init__(self, handle, elink_clk_mode=CLK_160):
        """
        Parameters
        ----------
        handle : Agwb_top
            Handle to the instance of the top object generated by the agwb.
        elink_clk_mode : CLK_80 or CLK_160
            Elink clk mode.
        """
        self.handle = handle

        # Create all required objects.
        self.elinks = Elinks(self.handle.elinks)

        self.hctsp_uplink_80 = Uplink(self.handle.uplink_80)
        self.hctsp_master_80 = Master(self.handle.hctsp_master_80, CLK_80)

        self.hctsp_uplink_160 = Uplink(self.handle.uplink_160)
        self.hctsp_master_160 = Master(self.handle.hctsp_master_160, CLK_160)

        self.set_elink_clk_mode(elink_clk_mode)

        self.rawfifos = [ raw_fifo(self.handle.rawfifos[i], self.handle.rawfifos_time[i]) for i in range(agwb.N_HCTSP_UPLINKS) ]

    def write_read_test(self):
        """Method for writing and reading random value from the rw_test_reg register.

        It does not perform any action based on the comparison.
        Instead it returns tuple (write_value, read_value), so that user can decide what to do.
        """
        rw_test_reg = self.handle.rw_test_reg

        log.info("Performing write read test")
        write_value = random.randint(0, 2 ** 32 - 1)
        rw_test_reg.write(write_value)
        read_value = rw_test_reg.read()

        if write_value != read_value:
            log.warning(
                "write_value (%s) differs from read_value (%s)",
                hex(write_value),
                hex(read_value),
            )
        else:
            log.info("Write read test ok")

        return write_value, read_value

    def set_elink_clk_mode(self, mode):
        assert CLK_40 <= mode <= CLK_160

        if mode == CLK_40:
            raise Exception("Elink clk mode 40 MHz currently not supported.")

        if mode == CLK_80:
            log.info("Setting Elink clock mode to 80 MHz")

            self.hctsp_master = self.hctsp_master_80
            self.hctsp_uplink = self.hctsp_uplink_80

        elif mode == CLK_160:
            log.info("Setting Elink clock mode to 160 MHz")

            self.hctsp_master = self.hctsp_master_160
            self.hctsp_uplink = self.hctsp_uplink_160

        self.handle.elink_clk_select.write(mode)

    def set_downlink_clock(self, downlink, phase):
        elink_clk_mode = self.handle.elink_clk_select.read()
        self.elinks.set_downlink_clock(downlink, elink_clk_mode, phase)

    class ClockPhaseIter():
        def __init__(self, function, output, elink_clk_mode):
            assert CLK_40 <= elink_clk_mode <= CLK_160

            self.function = function
            self.output = output
            if elink_clk_mode == CLK_40:
                self.len = 320
            elif elink_clk_mode == CLK_80:
                self.len = 160
            else:
                self.len = 80

        def __len__(self):
            return self.len

        def __getitem__(self, x):
            if 0 <= x < self.len:
                return lambda : self.function(self.output, x)
            else:
                raise IndexError()

    class DataPhaseIter():
        def __init__(self, function, elink_clk_mode):
            assert CLK_40 <= elink_clk_mode <= CLK_160

            self.function = function
            self.tap_delay = 78 # Tap delay in ps.

            if elink_clk_mode == CLK_40:
                self.bit_select = 8
                freq = 40e6
            elif elink_clk_mode == CLK_80:
                self.bit_select = 4
                freq = 80e6
            else:
                self.bit_select = 2
                freq = 160e6

            self.tap_bit_select = 1 / freq * 1e12 / 2 / self.bit_select
            log.debug("DataPhaseIter tap_bit_select: {} ps".format(self.tap_bit_select))

            self.len = self.bit_select * 20

            possibilities = []
            for b in range(self.bit_select):
                # Iterating more than 20 makes no sense as it overlaps with bit select.
                for d in range(20):
                    possibilities.append((b, d))

            delays = [b * self.tap_bit_select + d * self.tap_delay for b, d in possibilities]
            delays_dict = dict(zip(delays, possibilities))

            self.lut = []
            for d in sorted(delays_dict):
                v = list(delays_dict[d])
                v.append("{} ps".format(d))
                self.lut.append(tuple(v))
            log.debug("DataPhaseIter Look Up Table:\n{}".format(pformat(self.lut)))

        def __len__(self):
            return self.len

        def __getitem__(self, x):
            if 0 <= x < self.len:
                return lambda uplink : self.function(uplink=uplink, bit_select=self.lut[x][0], delay=self.lut[x][1])
            else:
                raise IndexError()

    def verify_eos(self):
        """
        This function verifies if EOS is received. If not, the uplink is removed
        from the list of active uplinks.
        """
        to_remove = []
        for u in self.uplinks:
            self.uplink.clear_sequence_detectors(u)
        time.sleep(0.01)

        for u in self.uplinks:
            if self.uplink.handle.receivers[u].sequence_detectors_status.eos_stable1.read() != 1:
                to_remove.append(u)
                log.warning("Uplink " + str(u) + " didn't respond with EOS")
        for u in to_remove:
            self.uplinks.remove(u)

    def scan_setup(self):
        """Scan setup.

        Returns
        -------
        List of SetupElements.
        """
        log.info("Scanning setup")
        ret = []

        elink_clk_mode = self.handle.elink_clk_select.read()
        data_phase_iterator = self.DataPhaseIter(self.elinks.set_data_delay, elink_clk_mode)

        for d in range(agwb.N_HCTSP_DOWNLINKS):
            for d_ in range(agwb.N_HCTSP_DOWNLINKS):
                self.elinks.disable_downlink_clock(d_)

            self.set_downlink_clock(d, 0)

            clock_phase_iterator = self.ClockPhaseIter(self.set_downlink_clock, d, elink_clk_mode)
            se = SetupElement(
                0,
                d,
                list(range(agwb.N_HCTSP_UPLINKS)),
                clock_phase_iterator,
                data_phase_iterator,
                self.hctsp_master,
                self.hctsp_uplink,
                {},
            )
            se.check_sos(reset_uplinks=True)

            if se.uplinks:
                ret.append(se)

        log.debug("Setup scan results:\n{}".format(ret))
        return ret

    def check_warings(self, links=list(range(agwb.N_HCTSP_UPLINKS))):

        detailed = {}

        # each bit in these registers informs whether there is a warning for that uplink
        # unused bits are zeroed
        w = self.handle.tester.ul.uplink_warnings0.read()
        w |= self.handle.tester.ul.uplink_warnings1.read() << 32

        for l in links:
            if w & (1 << l):
                detailed[l] = self.warnings_decode(
                    self.handle.tester.ul.warnings[l].read()
                )

        return detailed

    def warnings_decode(self, regvalue):
        dic = {}
        dic["sx_throttling"] = True if regvalue & (1 << 0) else False
        dic["sx_sync_alert"] = True if regvalue & (1 << 1) else False
        dic["sx_alert"] = True if regvalue & (1 << 2) else False
        dic["missing_sync"] = True if regvalue & (1 << 3) else False
        dic["dpb_bitslip"] = True if regvalue & (1 << 4) else False
        dic["code_err"] = True if regvalue & (1 << 5) else False
        dic["disp_err"] = True if regvalue & (1 << 6) else False

        dic["sx_status_dp_fifo_throttling_alert"] = (
            True if regvalue & (1 << 7) else False
        )
        dic["sx_status_link_sync_alert"] = True if regvalue & (1 << 8) else False
        dic["sx_status_fe_chan_trottling_alert"] = (
            True if regvalue & (1 << 9) else False
        )
        dic["sx_status_cd_crc_error"] = True if regvalue & (1 << 10) else False
        dic["sx_status_cd_fifo_full"] = True if regvalue & (1 << 11) else False
        dic["sx_status_cd_data_collector_error"] = (
            True if regvalue & (1 << 12) else False
        )
        dic["sx_status_thg_fifo_full"] = True if regvalue & (1 << 13) else False
        dic["sx_status_monitor_over_threshold"] = (
            True if regvalue & (1 << 14) else False
        )
        dic["sx_status_dp_fifo_throtl_alert_counter_full"] = (
            True if regvalue & (1 << 15) else False
        )
        dic["sx_status_fe_chan_throtl_alert_counter_full"] = (
            True if regvalue & (1 << 16) else False
        )
        dic["sx_status_seu_counter_full"] = True if regvalue & (1 << 17) else False

        return dic

    def warnings_print(self, dic):

        for k, d in dic.items():
            log.info("Uplink: %d" % k)
            log.info("Received from SX ACK frame:")
            log.info("   Throttling: %d" % d["sx_throttling"])
            log.info("   Sync Alert: %d" % d["sx_sync_alert"])
            log.info("   Alert: %d" % d["sx_alert"])

            log.info("Received from SX ALERT frame:")
            log.info(
                "   dp_fifo_throttling_alert: %d          "
                % d["sx_status_dp_fifo_throttling_alert"]
            )
            log.info(
                "   link_sync_alert: %d                   "
                % d["sx_status_link_sync_alert"]
            )
            log.info(
                "   fe_chan_trottling_alert: %d           "
                % d["sx_status_fe_chan_trottling_alert"]
            )
            log.info(
                "   cd_crc_error: %d                      "
                % d["sx_status_cd_crc_error"]
            )
            log.info(
                "   cd_fifo_full: %d                      "
                % d["sx_status_cd_fifo_full"]
            )
            log.info(
                "   cd_data_collector_error: %d           "
                % d["sx_status_cd_data_collector_error"]
            )
            log.info(
                "   thg_fifo_full: %d                     "
                % d["sx_status_thg_fifo_full"]
            )
            log.info(
                "   monitor_over_threshold: %d            "
                % d["sx_status_monitor_over_threshold"]
            )
            log.info(
                "   dp_fifo_throtl_alert_counter_full: %d "
                % d["sx_status_dp_fifo_throtl_alert_counter_full"]
            )
            log.info(
                "   fe_chan_throtl_alert_counter_full: %d "
                % d["sx_status_fe_chan_throtl_alert_counter_full"]
            )
            log.info(
                "   seu_counter_full: %d                  "
                % d["sx_status_seu_counter_full"]
            )

            log.info("Generated by DPB:")
            log.info("   Missing sync: %d" % d["missing_sync"])
            log.info("   UL bitslip: %d" % d["dpb_bitslip"])
            log.info("   UL 8b/10b encoder code error: %d" % d["code_err"])
            log.info("   UL 8b/10b encoder disparity error: %d" % d["disp_err"])

    def read_system_timestamp(self):
        timestamp_0 = self.handle.system_timestamp_0.read()
        timestamp_1 = self.handle.system_timestamp_1.read()

        return (timestamp_1 << 32) | timestamp_0

    def fifos_data_count(self, uplinks=None):
        """Get count of data in hit frame FIFOs.

        Parameters
        ----------
        uplinks : None | int | [int] (The default is None)
            Uplink number or list of uplinks numbers.
            None is equivalent to all uplinks.
        Returns
        -------
        count : int
        """
        if type(uplinks) == int:
            uplinks = [uplinks]
        if uplinks == None:
            uplinks = list(range(agwb.N_HCTSP_UPLINKS))

        count = 0
        for u in uplinks:
            count += self.rawfifos[u].get_data_count()

        return count

    def read_fifos_data(self, count, uplinks=None, withsystime=True):
        """Read data from hit frame FIFOs.

        Parameters
        ----------
        count : int
            Maximum count of data to be read from each selected uplink.
        uplinks : None | int | [int] (The default is None)
            Uplink number or list of uplinks numbers.
            None is equivalent to all uplinks.
        Returns
        -------
        Dictionary containing lists of data.
        """
        if type(uplinks) == int:
            uplinks = [uplinks]
        if uplinks == None:
            uplinks = list(range(agwb.N_HCTSP_UPLINKS))

        log.info("Reading hit frame FIFOs")

        ret = {}
        for u in uplinks:
            ret[u] = self.rawfifos[u].readout(count, withsystime=withsystime)
            print("Appending UL {} with {} frames".format(u, count))

        return ret

    def reset_hit_fifos(self, uplinks=None):
        """Reset hit frame FIFOs.

        Parameters
        ----------
        uplinks : None | int | [int] (The default is None)
            Uplink number or list of uplinks numbers.
            None is equivalent to all uplinks.
        """
        if type(uplinks) == int:
            uplinks = [uplinks]
        if uplinks == None:
            uplinks = list(range(agwb.N_HCTSP_UPLINKS))

        log.info("Resetting hit frame FIFOs {}".format(uplinks))
        for u in uplinks:
            self.rawfifos[u].reset()
