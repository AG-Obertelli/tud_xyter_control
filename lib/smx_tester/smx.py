import time
from .utils import *
from hctsp import *

class Smx():
    """Class implementing the SMX object in the tester."""

    def __init__(
        self,
        group,
        downlink,
        address,
        asic_uplinks,
        uplinks,
        uplinks_map,
        command_slot,
        ack_monitor
    ):
        self.group = group
        self.downlink = downlink
        self.address = address
        self.asic_uplinks = asic_uplinks
        self.uplinks = uplinks
        self.uplinks_map = uplinks_map
        self.command_slot = command_slot
        self.ack_monitor = ack_monitor

        self.cmd = Command(1 << self.group, 1 << self.downlink, 0, (WRADDR, WRDATA), 0, 0, self.address)

    def read(self, row, col):
        log.info("Reading SMX {}, column {}, row {}".format(hex(self.address), col, row))
        self.cmd.reevaluate(
            sequence_number=1,
            request_types=(RDDATA, NO_OP),
            register_address=(col << 8) | row
        )
        self.command_slot.issue(self.cmd)

        return self.ack_monitor.check_read(timeout = 0.1)

    def write(self, row, col, data):
        log.info("Writing SMX {}, column {}, row {}, data {}".format(hex(self.address), col, row, hex(data)))
        self.cmd.reevaluate(
            sequence_number=0,
            request_types=(WRADDR, WRDATA),
            register_address=(col << 8) | row,
            data=data
        )
        self.command_slot.issue(self.cmd)

        return self.ack_monitor.check_write(timeout = 1)

def smxes_from_setup_element(se):
    """Create Smx objects based on setup_element.

    Returns
    -------
    List of Smx objects.
    """
    ret = []

    for asic in sorted(se.asics_map):
        smx = Smx(
            se.group,
            se.downlink,
            asic,
            se.asics_map[asic]["ASIC uplinks"],
            se.asics_map[asic]["uplinks"],
            se.asics_map[asic]["uplinks map"],
            se.hctsp_master.software_command_slot,
            se.hctsp_uplink.ack_monitor,
        )
        ret.append(smx)
    return ret
