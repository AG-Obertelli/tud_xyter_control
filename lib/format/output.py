import logging as log


def get_bits(raw, left, right):
  bits = left+1-right
  return (raw >> right)&((2**bits)-1)

class frame:
  def __init__(self, frame):
    self.frame = frame

  def __str__(self):

    s = f"{self.__class__.__name__:10s} | 0x{self.frame:08x} | "

    for k,v in self.__dict__.items():
      if k!="frame":
        s += f"{k}=0x{v:0x} "
    
    return s

class hit(frame):
  def __init__(self, frame):
    super().__init__(frame)

    self.em    = get_bits(self.frame, 0, 0)
    self.ts    = get_bits(self.frame, 9, 1) | (get_bits(self.frame, 30, 30) << 9)
    self.adc   = get_bits(self.frame, 14, 10)
    self.chan  = get_bits(self.frame, 21, 15)
    self.elink = get_bits(self.frame, 27, 22)

class exthit(frame):
  def __init__(self, hit, tsmsb):
    super().__init__(hit.frame)

    self.em    = hit.em
    self.adc   = hit.adc
    self.chan  = hit.chan
    self.elink = hit.elink

    self.tmsbframe = tsmsb.frame
    
    self.tsorig = hit.ts
    self.ts     = hit.ts | tsmsb.tsmsb<<10

class tsmsb(frame):
  def __init__(self, frame):
    super().__init__(frame)

    # bits 39 downto 10 of the timestamp
    self.tsmsb = get_bits(self.frame, 28, 0)

class epoch(frame):
  def __init__(self, frame):
    super().__init__(frame)

    # bits 29 downto 0 of the timestamp
    self.epoch = get_bits(self.frame, 28, 0)

class last(frame):
  def __init__(self, frame):
    super().__init__(frame)

    self.err          = get_bits(self.frame, 0, 0)
    self.fifoafull    = get_bits(self.frame, 1, 1)
    self.fifooverflow = get_bits(self.frame, 2, 2)
    self.binoverflow  = get_bits(self.frame, 3, 3)
    self.binclear     = get_bits(self.frame, 4, 4)

class empty(frame):
  def __init__(self, frame):
    super().__init__(frame)


class mcs:
  def __init__(self, epoch):
    self.id = epoch.epoch

    self.hits         = []

    self.complete     = False
    self.err          = 0
    self.fifoafull    = 0
    self.fifooverflow = 0
    self.binoverflow  = 0
    self.binclear     = 0

  def close(self, lastframe ):
    self.err          = lastframe.err
    self.fifoafull    = lastframe.fifoafull
    self.fifooverflow = lastframe.fifooverflow
    self.binoverflow  = lastframe.binoverflow
    self.binclear     = lastframe.binclear

    self.complete     = True

def discriminator( frame ):

  if frame & 1<<31:
    # "100x"
    if get_bits(frame, 31, 29) == 4:
      return tsmsb(frame)

    # "101x"
    elif get_bits(frame, 31, 29) == 5:
      return epoch(frame)

    # "110x"
    elif get_bits(frame, 31, 29) == 6:
      raise Exception("Unexpected frame header: 110x")
    
    else:
      # "1110"
      if get_bits(frame, 31, 28) == 14:
        return last(frame)
      
      # "1111"
      else:
        return empty(frame)
    
  else:
    # "0xxx"
    return hit(frame)


# takes a list of output format frames
# returns a list of extended hits only 
# ignores division into microslices
def extend_hits( frames ):
  # find first tsmsb
  # hits without tsmsb are ignored
  for i in range(len(frames)):
    f = frames[i]
    if type(f) is tsmsb:
      msb = f
      startidx = i+1
      break
  else:
    raise Exception("Didn't find any TSMSB frames!")

  exthits = []

  # extend timestamps
  for i in range(startidx, len(frames)):
    f = frames[i]
    if type(f) is tsmsb:
      msb = f
    elif type(f) is hit:
      exthits.append(exthit(f,msb))

  return exthits


def parse_microslices(frames):
  idx = 0
  microslices = []

  while True:
    
    # find first epoch
    for i in range(idx, len(frames)):
      f = discriminator(frames[i])
      if type(f) is epoch:
        microslices.append(mcs(f))
        idx = i+1
        break
      else:
        log.info(f"Dropping a frame {f}")
    else:
      log.error("Didn't find any EPOCH frames!")
      # raise Exception("Didn't find any EPOCH frames!")

    # TSMSB should go right after the EPOCH
    if type(f) != tsmsb:
      # raise Exception("The EPOCH frame was not followed by TSMSB!")
      log.error("The EPOCH frame was not followed by TSMSB!")

    idx += 1

    # parse frame until we find the last frame
    for i in range(idx, len(frames)):
      f = discriminator(frames[i])
      
      if type(f) is last:
        microslices[-1].close(f)
        idx = i+1
        break

      if type(f) is tsmsb:
        msb = f
      
      elif type(f) is hit:
        microslices[-1].hits.append(exthit(hit(f),msb))

    # there should be an epoch frame (or empty frame) after the last frame
    for i in range(idx, len(frames)):
      f = discriminator(frames[i])

      # dont care about empty frames
      if type(f) == empty:
        continue

      # there is an epoch
      if type(f) == epoch:
        # keep the idx at the epoch!
        # we want to use this frame in the next loop iteration!
        idx = i
        break

      # this is not compliant with CRI data protocol!
      else:
        # raise Exception(f"Unexpected frame type found: {type(f)} between last and epoch")
        log.error(f"Unexpected frame type found: {type(f)} between last and epoch")
    

