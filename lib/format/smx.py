
import logging as log


def get_bits(raw, left, right):
  bits = left+1-right
  return (raw >> right)&((2**bits)-1)


class frame:
  def __init__(self, frame):
    self.frame = frame

  def __str__(self):
    s = f"{self.__class__.__name__:10s} | 0x{self.frame:06x} | "

    for k,v in self.__dict__.items():
      if k!="frame":
        s += f"{k}=0x{v:0x} "
    
    return s


class hit(frame):
  def __init__(self, frame):
      super().__init__(frame)

      self.chan = get_bits(frame, 22, 16)
      self.adc  = get_bits(frame, 15, 11)
      self.ts   = get_bits(frame, 10, 1)
      self.em   = get_bits(frame, 0, 0)

class dummy(frame):
  def __init__(self, frame):
      super().__init__(frame)

class exthit(frame):
  def __init__(self, hit, tsmsb):
    super().__init__(hit.frame)

    self.em    = hit.em
    self.adc   = hit.adc
    self.chan  = hit.chan

    self.tmsbframe = tsmsb.frame
    
    self.ts    = hit.ts | tsmsb.tsmsb<<10


class tsmsb(frame):
  def __init__(self, frame):
      super().__init__(frame)

      # todo: verify CRC
      self.crc = get_bits(frame, 3, 0)
      self.t1 = get_bits(frame, 9, 4)
      self.t2 = get_bits(frame, 15, 10)
      self.t3 = get_bits(frame, 21, 16)

      t1 = self.t1
      t2 = self.t2
      t3 = self.t3

      if (t1==t2) and (t2==t3):
        self.tsmsb = t1

      else:
        log.warning("The timestamp fields in TSMSB frame differ!")
        if (t1==t2):
          self.tsmsb = t1

        elif (t1==t3):
          self.tsmsb = t1

        elif (t2==t3):
          self.tsmsb = t2

        else:
          raise Exception("Invalid TSMSB found! All the timestamp values differ.")


class rddata_ack(frame):
  def __init__(self, frame):
      super().__init__(frame)
      
      self.reg = get_bits(frame, 20, 7)
      self.seq = get_bits(frame, 6, 4)
      self.crc = get_bits(frame, 3, 0)

class ack(frame):
  def __init__(self, frame):
      super().__init__(frame)
      
      self.ack  = get_bits(frame, 20, 19)
      self.seq  = get_bits(frame, 18, 15)
      self.cp   = get_bits(frame, 14, 14)
      self.stat = get_bits(frame, 13, 10)
      self.ts   = get_bits(frame, 9, 4)
      self.crc  = get_bits(frame, 3, 0)

class alert_ack(frame):
  def __init__(self, frame):
      super().__init__(frame)
      
      self.ack  = get_bits(frame, 20, 19)
      self.stat = get_bits(frame, 18, 8)
      self.crc  = get_bits(frame, 3, 0)

def determinator( frame ):

  if get_bits(frame,23,23) == 1:
    # 11X XX
    if get_bits(frame,22,22) == 1:
      return tsmsb(frame)
    
    else:
      # 101 XX
      if get_bits(frame,21,21) == 1:
        return rddata_ack(frame)

      else:
        # 100 11
        if get_bits(frame,20,19) == 3:
          return alert_ack(frame)
        
        # 100 01
        elif get_bits(frame,20,19) == 1:
          return ack(frame)
        
        # 100 10
        elif get_bits(frame,20,19) == 2:
          return ack(frame)
        
        # 100 00
        else:
          raise Exception("Invalid frame header found 10000!")

  else:
    # 0XXX XX
    if get_bits(frame,15,11) != 0:
      return hit(frame)
    
    else:
      return dummy(frame)

def determinator_list( frames ):
  determined = []

  for frame in frames:
    determined.append(determinator(frame))

  return determined

# takes a list of rawdata format frames
# returns a list of extended hits only 
def extend_hits( frames ):
  # find first tsmsb
  # hits without tsmsb are ignored
  for i in range(len(frames)):
    f = frames[i]
    if type(f) is tsmsb:
      msb = f
      startidx = i+1
      break
  else:
    raise Exception("Didn't find any TSMSB frames!")

  exthits = []

  # extend timestamps
  for i in range(startidx, len(frames)):
    f = frames[i]
    if type(f) is tsmsb:
      msb = f
    elif type(f) is hit:
      exthits.append(exthit(f,msb))

  return exthits
