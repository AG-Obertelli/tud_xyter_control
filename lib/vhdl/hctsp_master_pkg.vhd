--- This file has been automatically generated
--- by the agwb (https://github.com/wzab/agwb).
--- Please don't edit it manually, unless you really have to do it
library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library general_cores;
use general_cores.wishbone_pkg.all;

library work;
use work.agwb_pkg.all;

package hctsp_master_pkg is

  constant C_hctsp_master_ADDR_BITS : integer := 6;

  constant c_hctsp_master_ver_id : std_logic_vector(31 downto 0) := x"8731b1c8";
  constant v_hctsp_master_ver_id : t_ver_id_variants(0 downto 0) := (0 => x"8731b1c8");
  constant c_fetched_count_size : integer := 1;
  constant c_command_cycle_timestamp_0_size : integer := 1;
  constant c_command_cycle_timestamp_1_size : integer := 1;
  constant c_command_cycle_pause_width_size : integer := 1;
  constant c_encoding_modes_size : integer := 5;
  constant c_time_command_slots_size : integer := 4;
  constant c_special_time_command_slot_size : integer := 1;
  constant c_software_command_slot_size : integer := 1;


  constant C_fetched_count_REG_ADDR: unsigned := x"00000002";
  subtype t_fetched_count is std_logic_vector(31 downto 0);
  constant C_command_cycle_timestamp_0_REG_ADDR: unsigned := x"00000003";
  subtype t_command_cycle_timestamp_0 is std_logic_vector(31 downto 0);
  constant C_command_cycle_timestamp_1_REG_ADDR: unsigned := x"00000004";
  subtype t_command_cycle_timestamp_1 is std_logic_vector(31 downto 0);
  constant C_command_cycle_pause_width_REG_ADDR: unsigned := x"00000005";
  subtype t_command_cycle_pause_width is std_logic_vector(5 downto 0);
  constant C_encoding_modes_REG_ADDR: unsigned := x"00000006";
  subtype t_encoding_modes is std_logic_vector(1 downto 0);
  type ut_encoding_modes_array is array( natural range <> ) of t_encoding_modes;
  subtype t_encoding_modes_array is ut_encoding_modes_array(c_encoding_modes_size - 1 downto 0);


end hctsp_master_pkg;

package body hctsp_master_pkg is

end hctsp_master_pkg;
