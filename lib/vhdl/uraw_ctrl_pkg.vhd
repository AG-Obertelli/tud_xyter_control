--- This file has been automatically generated
--- by the agwb (https://github.com/wzab/agwb).
--- Please don't edit it manually, unless you really have to do it
library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library general_cores;
use general_cores.wishbone_pkg.all;

library work;
use work.agwb_pkg.all;

package uraw_ctrl_pkg is

  constant C_uraw_ctrl_ADDR_BITS : integer := 5;

  constant c_uraw_ctrl_ver_id : std_logic_vector(31 downto 0) := x"9fea4734";
  constant v_uraw_ctrl_ver_id : t_ver_id_variants(0 downto 0) := (0 => x"9fea4734");
  constant c_uraw_uplinks_size : integer := 10;
  constant c_uraw_filter_pattern_size : integer := 1;
  constant c_uraw_filter_mask_size : integer := 1;
  constant c_uraw_inhibit_pattern_size : integer := 1;
  constant c_uraw_inhibit_mask_size : integer := 1;
  constant c_uraw_fifo_size : integer := 1;


  constant C_uraw_uplinks_REG_ADDR: unsigned := x"00000002";
  subtype t_uraw_uplinks is unsigned(6 downto 0);
  type ut_uraw_uplinks_array is array( natural range <> ) of t_uraw_uplinks;
  subtype t_uraw_uplinks_array is ut_uraw_uplinks_array(c_uraw_uplinks_size - 1 downto 0);
  constant C_uraw_filter_pattern_REG_ADDR: unsigned := x"0000000c";
  subtype t_uraw_filter_pattern is std_logic_vector(23 downto 0);
  constant C_uraw_filter_mask_REG_ADDR: unsigned := x"0000000d";
  subtype t_uraw_filter_mask is std_logic_vector(23 downto 0);
  constant C_uraw_inhibit_pattern_REG_ADDR: unsigned := x"0000000e";
  subtype t_uraw_inhibit_pattern is std_logic_vector(23 downto 0);
  constant C_uraw_inhibit_mask_REG_ADDR: unsigned := x"0000000f";
  subtype t_uraw_inhibit_mask is std_logic_vector(23 downto 0);


end uraw_ctrl_pkg;

package body uraw_ctrl_pkg is

end uraw_ctrl_pkg;
