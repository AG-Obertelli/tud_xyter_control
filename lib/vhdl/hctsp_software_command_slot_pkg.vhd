--- This file has been automatically generated
--- by the agwb (https://github.com/wzab/agwb).
--- Please don't edit it manually, unless you really have to do it
library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library general_cores;
use general_cores.wishbone_pkg.all;

library work;
use work.agwb_pkg.all;

package hctsp_software_command_slot_pkg is

  constant C_hctsp_software_command_slot_ADDR_BITS : integer := 3;

  constant c_hctsp_software_command_slot_ver_id : std_logic_vector(31 downto 0) := x"8bbf1a8a";
  constant v_hctsp_software_command_slot_ver_id : t_ver_id_variants(0 downto 0) := (0 => x"8bbf1a8a");
  constant c_control_size : integer := 1;
  constant c_control_frame_size : integer := 2;


  constant C_control_REG_ADDR: unsigned := x"00000002";
  type t_control is record
    chip_address:std_logic_vector(3 downto 0);
    downlink_mask:std_logic_vector(11 downto 0);
    group_mask:std_logic_vector(7 downto 0);
    sequence_number:std_logic_vector(3 downto 0);
  end record;
  
  function to_control(x : std_logic_vector) return t_control;
  function to_slv(x : t_control) return std_logic_vector;
  constant C_control_frame_REG_ADDR: unsigned := x"00000003";
  type t_control_frame is record
    request_type:std_logic_vector(1 downto 0);
    request_payload:std_logic_vector(14 downto 0);
    crc:std_logic_vector(14 downto 0);
  end record;
  
  function to_control_frame(x : std_logic_vector) return t_control_frame;
  function to_slv(x : t_control_frame) return std_logic_vector;
  type ut_control_frame_array is array( natural range <> ) of t_control_frame;
  subtype t_control_frame_array is ut_control_frame_array(c_control_frame_size - 1 downto 0);


end hctsp_software_command_slot_pkg;

package body hctsp_software_command_slot_pkg is
  function to_control(x : std_logic_vector) return t_control is
    variable res : t_control;
  begin
    res.chip_address := std_logic_vector(x(3 downto 0));
    res.downlink_mask := std_logic_vector(x(15 downto 4));
    res.group_mask := std_logic_vector(x(23 downto 16));
    res.sequence_number := std_logic_vector(x(27 downto 24));
    return res;
  end function;
  
  function to_slv(x : t_control) return std_logic_vector is
    variable res : std_logic_vector(27 downto 0);
  begin
    res(3 downto 0) := std_logic_vector(x.chip_address);
    res(15 downto 4) := std_logic_vector(x.downlink_mask);
    res(23 downto 16) := std_logic_vector(x.group_mask);
    res(27 downto 24) := std_logic_vector(x.sequence_number);
    return res;
  end function;
  
  function to_control_frame(x : std_logic_vector) return t_control_frame is
    variable res : t_control_frame;
  begin
    res.request_type := std_logic_vector(x(1 downto 0));
    res.request_payload := std_logic_vector(x(16 downto 2));
    res.crc := std_logic_vector(x(31 downto 17));
    return res;
  end function;
  
  function to_slv(x : t_control_frame) return std_logic_vector is
    variable res : std_logic_vector(31 downto 0);
  begin
    res(1 downto 0) := std_logic_vector(x.request_type);
    res(16 downto 2) := std_logic_vector(x.request_payload);
    res(31 downto 17) := std_logic_vector(x.crc);
    return res;
  end function;
  

end hctsp_software_command_slot_pkg;
