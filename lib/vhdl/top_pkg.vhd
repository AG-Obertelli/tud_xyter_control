--- This file has been automatically generated
--- by the agwb (https://github.com/wzab/agwb).
--- Please don't edit it manually, unless you really have to do it
library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library general_cores;
use general_cores.wishbone_pkg.all;

library work;
use work.agwb_pkg.all;

package top_pkg is

  constant C_top_ADDR_BITS : integer := 12;

  constant c_top_ver_id : std_logic_vector(31 downto 0) := x"7637da40";
  constant v_top_ver_id : t_ver_id_variants(0 downto 0) := (0 => x"7637da40");
  constant c_rw_test_reg_size : integer := 1;
  constant c_elink_clk_select_size : integer := 1;
  constant c_main_status_size : integer := 1;
  constant c_main_ctrl_size : integer := 1;
  constant c_mac_ls3bytes_size : integer := 1;
  constant c_mac_ms3bytes_size : integer := 1;
  constant c_mac_status_size : integer := 1;
  constant c_j1b_ctrl_size : integer := 1;
  constant c_system_timestamp_0_size : integer := 1;
  constant c_system_timestamp_1_size : integer := 1;
  constant c_uplink_80_size : integer := 1;
  constant c_uplink_160_size : integer := 1;
  constant c_rawfifos_size : integer := 40;
  constant c_rawfifos_time_size : integer := 40;
  constant c_elinks_size : integer := 1;
  constant c_hctsp_master_80_size : integer := 1;
  constant c_hctsp_master_160_size : integer := 1;
  constant c_uraw_ctrl_size : integer := 1;
  constant c_i2c_master_size : integer := 1;


  constant C_rw_test_reg_REG_ADDR: unsigned := x"00000002";
  subtype t_rw_test_reg is std_logic_vector(31 downto 0);
  constant C_elink_clk_select_REG_ADDR: unsigned := x"00000003";
  subtype t_elink_clk_select is std_logic_vector(1 downto 0);
  constant C_main_status_REG_ADDR: unsigned := x"00000004";
  type t_main_status is record
    extjc_lock:std_logic_vector(0 downto 0);
    extjc_unlock_cnt:std_logic_vector(7 downto 0);
  end record;
  
  function to_main_status(x : std_logic_vector) return t_main_status;
  function to_slv(x : t_main_status) return std_logic_vector;
  constant C_main_ctrl_REG_ADDR: unsigned := x"00000005";
  type t_main_ctrl is record
    gbt_ic_enable:std_logic_vector(0 downto 0);
    i2c_sfp_select:std_logic_vector(0 downto 0);
    use_sw_jc:std_logic_vector(0 downto 0);
    extjc_unlock_cnt_rst:std_logic_vector(0 downto 0);
    extjc_hwrst:std_logic_vector(0 downto 0);
  end record;
  
  function to_main_ctrl(x : std_logic_vector) return t_main_ctrl;
  function to_slv(x : t_main_ctrl) return std_logic_vector;
  constant C_mac_ls3bytes_REG_ADDR: unsigned := x"00000006";
  subtype t_mac_ls3bytes is std_logic_vector(31 downto 0);
  constant C_mac_ms3bytes_REG_ADDR: unsigned := x"00000007";
  subtype t_mac_ms3bytes is std_logic_vector(31 downto 0);
  constant C_mac_status_REG_ADDR: unsigned := x"00000008";
  subtype t_mac_status is std_logic_vector(31 downto 0);
  constant C_j1b_ctrl_REG_ADDR: unsigned := x"00000009";
  type t_j1b_ctrl is record
    break_loop:std_logic_vector(0 downto 0);
    reset:std_logic_vector(0 downto 0);
  end record;
  
  function to_j1b_ctrl(x : std_logic_vector) return t_j1b_ctrl;
  function to_slv(x : t_j1b_ctrl) return std_logic_vector;
  constant C_system_timestamp_0_REG_ADDR: unsigned := x"0000000a";
  subtype t_system_timestamp_0 is std_logic_vector(31 downto 0);
  constant C_system_timestamp_1_REG_ADDR: unsigned := x"0000000b";
  subtype t_system_timestamp_1 is std_logic_vector(31 downto 0);


end top_pkg;

package body top_pkg is
  function to_main_status(x : std_logic_vector) return t_main_status is
    variable res : t_main_status;
  begin
    res.extjc_lock := std_logic_vector(x(0 downto 0));
    res.extjc_unlock_cnt := std_logic_vector(x(8 downto 1));
    return res;
  end function;
  
  function to_slv(x : t_main_status) return std_logic_vector is
    variable res : std_logic_vector(8 downto 0);
  begin
    res(0 downto 0) := std_logic_vector(x.extjc_lock);
    res(8 downto 1) := std_logic_vector(x.extjc_unlock_cnt);
    return res;
  end function;
  
  function to_main_ctrl(x : std_logic_vector) return t_main_ctrl is
    variable res : t_main_ctrl;
  begin
    res.gbt_ic_enable := std_logic_vector(x(0 downto 0));
    res.i2c_sfp_select := std_logic_vector(x(1 downto 1));
    res.use_sw_jc := std_logic_vector(x(2 downto 2));
    res.extjc_unlock_cnt_rst := std_logic_vector(x(3 downto 3));
    res.extjc_hwrst := std_logic_vector(x(4 downto 4));
    return res;
  end function;
  
  function to_slv(x : t_main_ctrl) return std_logic_vector is
    variable res : std_logic_vector(4 downto 0);
  begin
    res(0 downto 0) := std_logic_vector(x.gbt_ic_enable);
    res(1 downto 1) := std_logic_vector(x.i2c_sfp_select);
    res(2 downto 2) := std_logic_vector(x.use_sw_jc);
    res(3 downto 3) := std_logic_vector(x.extjc_unlock_cnt_rst);
    res(4 downto 4) := std_logic_vector(x.extjc_hwrst);
    return res;
  end function;
  
  function to_j1b_ctrl(x : std_logic_vector) return t_j1b_ctrl is
    variable res : t_j1b_ctrl;
  begin
    res.break_loop := std_logic_vector(x(0 downto 0));
    res.reset := std_logic_vector(x(1 downto 1));
    return res;
  end function;
  
  function to_slv(x : t_j1b_ctrl) return std_logic_vector is
    variable res : std_logic_vector(1 downto 0);
  begin
    res(0 downto 0) := std_logic_vector(x.break_loop);
    res(1 downto 1) := std_logic_vector(x.reset);
    return res;
  end function;
  

end top_pkg;
