library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
package top_const_pkg is
constant C_top_system_ver : std_logic_vector(31 downto 0) := x"866b7a54";
constant C_N_HCTSP_DOWNLINKS : integer := 5; -- 5
constant C_N_HCTSP_UPLINKS : integer := 40; -- 40
constant C_N_ELINK_WARN : integer := 7; -- 7
constant C_N_RAW_FIFO_L2COUNT : integer := 14; -- 14
constant C_URAW_FIFO_L2_DEPTH : integer := 10; -- 10
constant C_N_OF_SX_AT_DLINK : integer := 8; -- 8
constant C_N_HCTSP_GROUPS : integer := 1; -- 1
end package;
