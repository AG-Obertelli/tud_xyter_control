--- This file has been automatically generated
--- by the agwb (https://github.com/wzab/agwb).
--- Please don't edit it manually, unless you really have to do it
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library general_cores;
use general_cores.wishbone_pkg.all;
library work;
use work.agwb_pkg.all;
use work.uraw_fifo_ctrl_pkg.all;

entity uraw_fifo_ctrl is
  generic (
    g_rcv_mac_l32_size : integer := c_rcv_mac_l32_size;
    g_rcv_mac_m16_size : integer := c_rcv_mac_m16_size;
    g_rcv_ip_size : integer := c_rcv_ip_size;
    g_dport_size : integer := c_dport_size;
    g_max_words_size : integer := c_max_words_size;
    g_min_words_size : integer := c_min_words_size;
    g_flush_size : integer := c_flush_size;

    g_ver_id : std_logic_vector(31 downto 0) := c_uraw_fifo_ctrl_ver_id;
    g_registered : integer := 0
  );
  port (
    slave_i : in t_wishbone_slave_in;
    slave_o : out t_wishbone_slave_out;

    rcv_mac_l32_o : out  t_rcv_mac_l32;
    rcv_mac_m16_o : out  t_rcv_mac_m16;
    rcv_ip_o : out  t_rcv_ip;
    dport_o : out  t_dport;
    max_words_o : out  t_max_words;
    min_words_o : out  t_min_words;
    flush_o : out  t_flush;

    rst_n_i : in std_logic;
    clk_sys_i : in std_logic
    );

end uraw_fifo_ctrl;

architecture gener of uraw_fifo_ctrl is
  signal int_rcv_mac_l32_o : t_rcv_mac_l32;
  signal int_rcv_mac_m16_o : t_rcv_mac_m16;
  signal int_rcv_ip_o : t_rcv_ip;
  signal int_dport_o : t_dport;
  signal int_max_words_o : t_max_words;
  signal int_min_words_o : t_min_words;
  signal int_flush_o : t_flush;


  -- Internal WB declaration
  signal int_regs_wb_m_o : t_wishbone_master_out;
  signal int_regs_wb_m_i : t_wishbone_master_in;
  signal int_addr : std_logic_vector(4-1 downto 0);
  signal wb_up_o : t_wishbone_slave_out_array(1-1 downto 0);
  signal wb_up_i : t_wishbone_slave_in_array(1-1 downto 0);
  signal wb_up_r_o : t_wishbone_slave_out_array(1-1 downto 0);
  signal wb_up_r_i : t_wishbone_slave_in_array(1-1 downto 0);
  signal wb_m_o : t_wishbone_master_out_array(1-1 downto 0);
  signal wb_m_i : t_wishbone_master_in_array(1-1 downto 0) := (others => c_WB_SLAVE_OUT_ERR);

  -- Constants
  constant c_address : t_wishbone_address_array(1-1  downto 0) := (0=>"00000000000000000000000000000000");
  constant c_mask : t_wishbone_address_array(1-1 downto 0) := (0=>"00000000000000000000000000000000");
begin
  
  assert g_rcv_mac_l32_size <= c_rcv_mac_l32_size report "g_rcv_mac_l32_size must be not greater than c_rcv_mac_l32_size=1" severity failure;
  assert g_rcv_mac_m16_size <= c_rcv_mac_m16_size report "g_rcv_mac_m16_size must be not greater than c_rcv_mac_m16_size=1" severity failure;
  assert g_rcv_ip_size <= c_rcv_ip_size report "g_rcv_ip_size must be not greater than c_rcv_ip_size=1" severity failure;
  assert g_dport_size <= c_dport_size report "g_dport_size must be not greater than c_dport_size=1" severity failure;
  assert g_max_words_size <= c_max_words_size report "g_max_words_size must be not greater than c_max_words_size=1" severity failure;
  assert g_min_words_size <= c_min_words_size report "g_min_words_size must be not greater than c_min_words_size=1" severity failure;
  assert g_flush_size <= c_flush_size report "g_flush_size must be not greater than c_flush_size=1" severity failure;

  wb_up_i(0) <= slave_i;
  slave_o <= wb_up_o(0);
  int_addr <= int_regs_wb_m_o.adr(4-1 downto 0);

-- Conditional adding of xwb_register   
  gr1: if g_registered = 2 generate
    grl1: for i in 0 to 1-1 generate
      xwb_register_1: entity general_cores.xwb_register
      generic map (
        g_WB_MODE => CLASSIC)
      port map (
        rst_n_i  => rst_n_i,
        clk_i    => clk_sys_i,
        slave_i  => wb_up_i(i),
        slave_o  => wb_up_o(i),
        master_i => wb_up_r_o(i),
        master_o => wb_up_r_i(i));
    end generate grl1;
  end generate gr1;

  gr2: if g_registered /= 2 generate
      wb_up_r_i <= wb_up_i;
      wb_up_o <= wb_up_r_o;
  end generate gr2;

-- Main crossbar
  xwb_crossbar_1: entity general_cores.xwb_crossbar
  generic map (
     g_num_masters => 1,
     g_num_slaves  => 1,
     g_registered  => (g_registered = 1),
     g_address     => c_address,
     g_mask        => c_mask
  )
  port map (
     clk_sys_i => clk_sys_i,
     rst_n_i   => rst_n_i,
     slave_i   => wb_up_r_i,
     slave_o   => wb_up_r_o,
     master_i  => wb_m_i,
     master_o  => wb_m_o,
     sdb_sel_o => open
  );

-- Process for register access
  process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if rst_n_i = '0' then
        -- Reset of the core
        int_regs_wb_m_i <= c_DUMMY_WB_MASTER_IN;

      else
        -- Clearing of trigger bits (if there are any)

        -- Normal operation
        int_regs_wb_m_i.rty <= '0';
        int_regs_wb_m_i.ack <= '0';
        int_regs_wb_m_i.err <= '0';

        if (int_regs_wb_m_o.cyc = '1') and (int_regs_wb_m_o.stb = '1')
            and (int_regs_wb_m_i.err = '0') and (int_regs_wb_m_i.rty = '0')
            and (int_regs_wb_m_i.ack = '0') then
          int_regs_wb_m_i.err <= '1'; -- in case of missed address
          -- Access, now we handle consecutive registers
          -- Set the error state so it is output when none register is accessed
          int_regs_wb_m_i.dat <= x"A5A5A5A5";
          int_regs_wb_m_i.ack <= '0';
          int_regs_wb_m_i.err <= '1';
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_rcv_mac_l32_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(2 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(31 downto 0) <= std_logic_vector(int_rcv_mac_l32_o);
              if int_regs_wb_m_o.we = '1' then
                int_rcv_mac_l32_o <= std_logic_vector(int_regs_wb_m_o.dat(31 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_rcv_mac_l32_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_rcv_mac_m16_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(3 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(15 downto 0) <= std_logic_vector(int_rcv_mac_m16_o);
              if int_regs_wb_m_o.we = '1' then
                int_rcv_mac_m16_o <= std_logic_vector(int_regs_wb_m_o.dat(15 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_rcv_mac_m16_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_rcv_ip_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(4 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(31 downto 0) <= std_logic_vector(int_rcv_ip_o);
              if int_regs_wb_m_o.we = '1' then
                int_rcv_ip_o <= std_logic_vector(int_regs_wb_m_o.dat(31 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_rcv_ip_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_dport_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(5 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(15 downto 0) <= std_logic_vector(int_dport_o);
              if int_regs_wb_m_o.we = '1' then
                int_dport_o <= std_logic_vector(int_regs_wb_m_o.dat(15 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_dport_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_max_words_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(6 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(9 downto 0) <= std_logic_vector(int_max_words_o);
              if int_regs_wb_m_o.we = '1' then
                int_max_words_o <= std_logic_vector(int_regs_wb_m_o.dat(9 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_max_words_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_min_words_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(7 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(9 downto 0) <= std_logic_vector(int_min_words_o);
              if int_regs_wb_m_o.we = '1' then
                int_min_words_o <= std_logic_vector(int_regs_wb_m_o.dat(9 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_min_words_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_flush_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(8 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(0 downto 0) <= std_logic_vector(int_flush_o);
              if int_regs_wb_m_o.we = '1' then
                int_flush_o <= std_logic_vector(int_regs_wb_m_o.dat(0 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_flush_size


          if int_addr = "0000" then
             int_regs_wb_m_i.dat <= x"2112a660";
             if int_regs_wb_m_o.we = '1' then
                int_regs_wb_m_i.err <= '1';
                int_regs_wb_m_i.ack <= '0';
             else
                int_regs_wb_m_i.ack <= '1';
                int_regs_wb_m_i.err <= '0';
             end if;
          end if;
          if int_addr = "0001" then
             int_regs_wb_m_i.dat <= g_ver_id;
             if int_regs_wb_m_o.we = '1' then
                int_regs_wb_m_i.err <= '1';
                int_regs_wb_m_i.ack <= '0';
             else
                int_regs_wb_m_i.ack <= '1';
                int_regs_wb_m_i.err <= '0';
             end if;
          end if;
        end if;
      end if;
    end if;
  end process;
  rcv_mac_l32_o <= int_rcv_mac_l32_o;
  rcv_mac_m16_o <= int_rcv_mac_m16_o;
  rcv_ip_o <= int_rcv_ip_o;
  dport_o <= int_dport_o;
  max_words_o <= int_max_words_o;
  min_words_o <= int_min_words_o;
  flush_o <= int_flush_o;
  wb_m_i(0) <= int_regs_wb_m_i;
  int_regs_wb_m_o  <= wb_m_o(0);

end architecture;
