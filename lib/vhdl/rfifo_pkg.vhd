--- This file has been automatically generated
--- by the agwb (https://github.com/wzab/agwb).
--- Please don't edit it manually, unless you really have to do it
library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library general_cores;
use general_cores.wishbone_pkg.all;

library work;
use work.agwb_pkg.all;

package rfifo_pkg is

  constant C_rfifo_ADDR_BITS : integer := 3;

  constant c_rfifo_ver_id : std_logic_vector(31 downto 0) := x"c52d4b05";
  constant v_rfifo_ver_id : t_ver_id_variants(0 downto 0) := (0 => x"c52d4b05");
  constant c_ctrl_trig_clear : std_logic_vector(1 downto 0) := "10";
  constant c_ctrl_size : integer := 1;
  constant c_data_size : integer := 1;
  constant c_used_size : integer := 1;


  constant C_ctrl_REG_ADDR: unsigned := x"00000002";
  type t_ctrl is record
    rst:std_logic_vector(0 downto 0);
    wr_en:std_logic_vector(0 downto 0);
  end record;
  
  function to_ctrl(x : std_logic_vector) return t_ctrl;
  function to_slv(x : t_ctrl) return std_logic_vector;
  constant C_data_REG_ADDR: unsigned := x"00000003";
  subtype t_data is std_logic_vector(31 downto 0);
  constant C_used_REG_ADDR: unsigned := x"00000004";
  subtype t_used is unsigned(31 downto 0);


end rfifo_pkg;

package body rfifo_pkg is
  function to_ctrl(x : std_logic_vector) return t_ctrl is
    variable res : t_ctrl;
  begin
    res.rst := std_logic_vector(x(0 downto 0));
    res.wr_en := std_logic_vector(x(1 downto 1));
    return res;
  end function;
  
  function to_slv(x : t_ctrl) return std_logic_vector is
    variable res : std_logic_vector(1 downto 0);
  begin
    res(0 downto 0) := std_logic_vector(x.rst);
    res(1 downto 1) := std_logic_vector(x.wr_en);
    return res;
  end function;
  

end rfifo_pkg;
