--- This file has been automatically generated
--- by the agwb (https://github.com/wzab/agwb).
--- Please don't edit it manually, unless you really have to do it
library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library general_cores;
use general_cores.wishbone_pkg.all;

library work;
use work.agwb_pkg.all;

package uraw_fifo_ctrl_pkg is

  constant C_uraw_fifo_ctrl_ADDR_BITS : integer := 4;

  constant c_uraw_fifo_ctrl_ver_id : std_logic_vector(31 downto 0) := x"19a2021c";
  constant v_uraw_fifo_ctrl_ver_id : t_ver_id_variants(0 downto 0) := (0 => x"19a2021c");
  constant c_rcv_mac_l32_size : integer := 1;
  constant c_rcv_mac_m16_size : integer := 1;
  constant c_rcv_ip_size : integer := 1;
  constant c_dport_size : integer := 1;
  constant c_max_words_size : integer := 1;
  constant c_min_words_size : integer := 1;
  constant c_flush_size : integer := 1;


  constant C_rcv_mac_l32_REG_ADDR: unsigned := x"00000002";
  subtype t_rcv_mac_l32 is std_logic_vector(31 downto 0);
  constant C_rcv_mac_m16_REG_ADDR: unsigned := x"00000003";
  subtype t_rcv_mac_m16 is std_logic_vector(15 downto 0);
  constant C_rcv_ip_REG_ADDR: unsigned := x"00000004";
  subtype t_rcv_ip is std_logic_vector(31 downto 0);
  constant C_dport_REG_ADDR: unsigned := x"00000005";
  subtype t_dport is std_logic_vector(15 downto 0);
  constant C_max_words_REG_ADDR: unsigned := x"00000006";
  subtype t_max_words is std_logic_vector(9 downto 0);
  constant C_min_words_REG_ADDR: unsigned := x"00000007";
  subtype t_min_words is std_logic_vector(9 downto 0);
  constant C_flush_REG_ADDR: unsigned := x"00000008";
  subtype t_flush is std_logic_vector(0 downto 0);


end uraw_fifo_ctrl_pkg;

package body uraw_fifo_ctrl_pkg is

end uraw_fifo_ctrl_pkg;
