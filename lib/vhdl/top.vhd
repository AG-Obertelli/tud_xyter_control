--- This file has been automatically generated
--- by the agwb (https://github.com/wzab/agwb).
--- Please don't edit it manually, unless you really have to do it
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library general_cores;
use general_cores.wishbone_pkg.all;
library work;
use work.agwb_pkg.all;
use work.top_pkg.all;

entity top is
  generic (
    g_rw_test_reg_size : integer := c_rw_test_reg_size;
    g_elink_clk_select_size : integer := c_elink_clk_select_size;
    g_main_status_size : integer := c_main_status_size;
    g_main_ctrl_size : integer := c_main_ctrl_size;
    g_mac_ls3bytes_size : integer := c_mac_ls3bytes_size;
    g_mac_ms3bytes_size : integer := c_mac_ms3bytes_size;
    g_mac_status_size : integer := c_mac_status_size;
    g_j1b_ctrl_size : integer := c_j1b_ctrl_size;
    g_system_timestamp_0_size : integer := c_system_timestamp_0_size;
    g_system_timestamp_1_size : integer := c_system_timestamp_1_size;
    g_uplink_80_size : integer := c_uplink_80_size;
    g_uplink_160_size : integer := c_uplink_160_size;
    g_rawfifos_size : integer := c_rawfifos_size;
    g_rawfifos_time_size : integer := c_rawfifos_time_size;
    g_elinks_size : integer := c_elinks_size;
    g_hctsp_master_80_size : integer := c_hctsp_master_80_size;
    g_hctsp_master_160_size : integer := c_hctsp_master_160_size;
    g_uraw_ctrl_size : integer := c_uraw_ctrl_size;
    g_i2c_master_size : integer := c_i2c_master_size;

    g_ver_id : std_logic_vector(31 downto 0) := c_top_ver_id;
    g_registered : integer := 0
  );
  port (
    slave_i : in t_wishbone_slave_in_array(3-1 downto 0);
    slave_o : out t_wishbone_slave_out_array(3-1 downto 0);
    uplink_80_wb_m_o : out t_wishbone_master_out;
    uplink_80_wb_m_i : in t_wishbone_master_in := c_WB_SLAVE_OUT_ERR;
    uplink_160_wb_m_o : out t_wishbone_master_out;
    uplink_160_wb_m_i : in t_wishbone_master_in := c_WB_SLAVE_OUT_ERR;
    rawfifos_wb_m_o : out t_wishbone_master_out_array( g_rawfifos_size - 1 downto 0 );
    rawfifos_wb_m_i : in t_wishbone_master_in_array( g_rawfifos_size - 1 downto 0 ) := ( others => c_WB_SLAVE_OUT_ERR);
    rawfifos_time_wb_m_o : out t_wishbone_master_out_array( g_rawfifos_time_size - 1 downto 0 );
    rawfifos_time_wb_m_i : in t_wishbone_master_in_array( g_rawfifos_time_size - 1 downto 0 ) := ( others => c_WB_SLAVE_OUT_ERR);
    elinks_wb_m_o : out t_wishbone_master_out;
    elinks_wb_m_i : in t_wishbone_master_in := c_WB_SLAVE_OUT_ERR;
    hctsp_master_80_wb_m_o : out t_wishbone_master_out;
    hctsp_master_80_wb_m_i : in t_wishbone_master_in := c_WB_SLAVE_OUT_ERR;
    hctsp_master_160_wb_m_o : out t_wishbone_master_out;
    hctsp_master_160_wb_m_i : in t_wishbone_master_in := c_WB_SLAVE_OUT_ERR;
    uraw_ctrl_wb_m_o : out t_wishbone_master_out;
    uraw_ctrl_wb_m_i : in t_wishbone_master_in := c_WB_SLAVE_OUT_ERR;
    i2c_master_wb_m_o : out t_wishbone_master_out;
    i2c_master_wb_m_i : in t_wishbone_master_in := c_WB_SLAVE_OUT_ERR;

    rw_test_reg_o : out  t_rw_test_reg;
    elink_clk_select_o : out  t_elink_clk_select;
    main_status_i : in  t_main_status;
    main_ctrl_o : out  t_main_ctrl;
    mac_ls3bytes_o : out  t_mac_ls3bytes;
    mac_ls3bytes_o_stb : out std_logic;
    mac_ms3bytes_o : out  t_mac_ms3bytes;
    mac_status_i : in  t_mac_status;
    j1b_ctrl_o : out  t_j1b_ctrl;
    system_timestamp_0_i : in  t_system_timestamp_0;
    system_timestamp_0_i_ack : out std_logic;
    system_timestamp_1_i : in  t_system_timestamp_1;

    rst_n_i : in std_logic;
    clk_sys_i : in std_logic
    );

end top;

architecture gener of top is
  signal int_rw_test_reg_o : t_rw_test_reg;
  signal int_elink_clk_select_o : t_elink_clk_select;
  signal int_main_ctrl_o : t_main_ctrl := to_main_ctrl(std_logic_vector(to_unsigned(0,5))); -- Hex value: 0x0
  signal int_mac_ls3bytes_o : t_mac_ls3bytes := std_logic_vector(to_unsigned(10556824,32)); -- Hex value: 0xa11598
  signal int_mac_ls3bytes_o_stb : std_logic;
  signal int_mac_ms3bytes_o : t_mac_ms3bytes := std_logic_vector(to_unsigned(134619,32)); -- Hex value: 0x20ddb
  signal int_j1b_ctrl_o : t_j1b_ctrl := to_j1b_ctrl(std_logic_vector(to_unsigned(0,2))); -- Hex value: 0x0


  -- Internal WB declaration
  signal int_regs_wb_m_o : t_wishbone_master_out;
  signal int_regs_wb_m_i : t_wishbone_master_in;
  signal int_addr : std_logic_vector(4-1 downto 0);
  signal wb_up_o : t_wishbone_slave_out_array(3-1 downto 0);
  signal wb_up_i : t_wishbone_slave_in_array(3-1 downto 0);
  signal wb_up_r_o : t_wishbone_slave_out_array(3-1 downto 0);
  signal wb_up_r_i : t_wishbone_slave_in_array(3-1 downto 0);
  signal wb_m_o : t_wishbone_master_out_array(88-1 downto 0);
  signal wb_m_i : t_wishbone_master_in_array(88-1 downto 0) := (others => c_WB_SLAVE_OUT_ERR);

  -- Constants
  constant c_address : t_wishbone_address_array(88-1  downto 0) := (0=>"00000000000000000000110000000000",1=>"00000000000000000000100000000000",2=>"00000000000000000000011000000000",3=>"00000000000000000000011000001000",4=>"00000000000000000000011000010000",5=>"00000000000000000000011000011000",6=>"00000000000000000000011000100000",7=>"00000000000000000000011000101000",8=>"00000000000000000000011000110000",9=>"00000000000000000000011000111000",10=>"00000000000000000000011001000000",11=>"00000000000000000000011001001000",12=>"00000000000000000000011001010000",13=>"00000000000000000000011001011000",14=>"00000000000000000000011001100000",15=>"00000000000000000000011001101000",16=>"00000000000000000000011001110000",17=>"00000000000000000000011001111000",18=>"00000000000000000000011010000000",19=>"00000000000000000000011010001000",20=>"00000000000000000000011010010000",21=>"00000000000000000000011010011000",22=>"00000000000000000000011010100000",23=>"00000000000000000000011010101000",24=>"00000000000000000000011010110000",25=>"00000000000000000000011010111000",26=>"00000000000000000000011011000000",27=>"00000000000000000000011011001000",28=>"00000000000000000000011011010000",29=>"00000000000000000000011011011000",30=>"00000000000000000000011011100000",31=>"00000000000000000000011011101000",32=>"00000000000000000000011011110000",33=>"00000000000000000000011011111000",34=>"00000000000000000000011100000000",35=>"00000000000000000000011100001000",36=>"00000000000000000000011100010000",37=>"00000000000000000000011100011000",38=>"00000000000000000000011100100000",39=>"00000000000000000000011100101000",40=>"00000000000000000000011100110000",41=>"00000000000000000000011100111000",42=>"00000000000000000000010000000000",43=>"00000000000000000000010000001000",44=>"00000000000000000000010000010000",45=>"00000000000000000000010000011000",46=>"00000000000000000000010000100000",47=>"00000000000000000000010000101000",48=>"00000000000000000000010000110000",49=>"00000000000000000000010000111000",50=>"00000000000000000000010001000000",51=>"00000000000000000000010001001000",52=>"00000000000000000000010001010000",53=>"00000000000000000000010001011000",54=>"00000000000000000000010001100000",55=>"00000000000000000000010001101000",56=>"00000000000000000000010001110000",57=>"00000000000000000000010001111000",58=>"00000000000000000000010010000000",59=>"00000000000000000000010010001000",60=>"00000000000000000000010010010000",61=>"00000000000000000000010010011000",62=>"00000000000000000000010010100000",63=>"00000000000000000000010010101000",64=>"00000000000000000000010010110000",65=>"00000000000000000000010010111000",66=>"00000000000000000000010011000000",67=>"00000000000000000000010011001000",68=>"00000000000000000000010011010000",69=>"00000000000000000000010011011000",70=>"00000000000000000000010011100000",71=>"00000000000000000000010011101000",72=>"00000000000000000000010011110000",73=>"00000000000000000000010011111000",74=>"00000000000000000000010100000000",75=>"00000000000000000000010100001000",76=>"00000000000000000000010100010000",77=>"00000000000000000000010100011000",78=>"00000000000000000000010100100000",79=>"00000000000000000000010100101000",80=>"00000000000000000000010100110000",81=>"00000000000000000000010100111000",82=>"00000000000000000000001110000000",83=>"00000000000000000000001101000000",84=>"00000000000000000000001100000000",85=>"00000000000000000000001011100000",86=>"00000000000000000000000000000000",87=>"00000000000000000000001011011000");
  constant c_mask : t_wishbone_address_array(88-1 downto 0) := (0=>"00000000000000000000110000000000",1=>"00000000000000000000110000000000",2=>"00000000000000000000111111111000",3=>"00000000000000000000111111111000",4=>"00000000000000000000111111111000",5=>"00000000000000000000111111111000",6=>"00000000000000000000111111111000",7=>"00000000000000000000111111111000",8=>"00000000000000000000111111111000",9=>"00000000000000000000111111111000",10=>"00000000000000000000111111111000",11=>"00000000000000000000111111111000",12=>"00000000000000000000111111111000",13=>"00000000000000000000111111111000",14=>"00000000000000000000111111111000",15=>"00000000000000000000111111111000",16=>"00000000000000000000111111111000",17=>"00000000000000000000111111111000",18=>"00000000000000000000111111111000",19=>"00000000000000000000111111111000",20=>"00000000000000000000111111111000",21=>"00000000000000000000111111111000",22=>"00000000000000000000111111111000",23=>"00000000000000000000111111111000",24=>"00000000000000000000111111111000",25=>"00000000000000000000111111111000",26=>"00000000000000000000111111111000",27=>"00000000000000000000111111111000",28=>"00000000000000000000111111111000",29=>"00000000000000000000111111111000",30=>"00000000000000000000111111111000",31=>"00000000000000000000111111111000",32=>"00000000000000000000111111111000",33=>"00000000000000000000111111111000",34=>"00000000000000000000111111111000",35=>"00000000000000000000111111111000",36=>"00000000000000000000111111111000",37=>"00000000000000000000111111111000",38=>"00000000000000000000111111111000",39=>"00000000000000000000111111111000",40=>"00000000000000000000111111111000",41=>"00000000000000000000111111111000",42=>"00000000000000000000111111111000",43=>"00000000000000000000111111111000",44=>"00000000000000000000111111111000",45=>"00000000000000000000111111111000",46=>"00000000000000000000111111111000",47=>"00000000000000000000111111111000",48=>"00000000000000000000111111111000",49=>"00000000000000000000111111111000",50=>"00000000000000000000111111111000",51=>"00000000000000000000111111111000",52=>"00000000000000000000111111111000",53=>"00000000000000000000111111111000",54=>"00000000000000000000111111111000",55=>"00000000000000000000111111111000",56=>"00000000000000000000111111111000",57=>"00000000000000000000111111111000",58=>"00000000000000000000111111111000",59=>"00000000000000000000111111111000",60=>"00000000000000000000111111111000",61=>"00000000000000000000111111111000",62=>"00000000000000000000111111111000",63=>"00000000000000000000111111111000",64=>"00000000000000000000111111111000",65=>"00000000000000000000111111111000",66=>"00000000000000000000111111111000",67=>"00000000000000000000111111111000",68=>"00000000000000000000111111111000",69=>"00000000000000000000111111111000",70=>"00000000000000000000111111111000",71=>"00000000000000000000111111111000",72=>"00000000000000000000111111111000",73=>"00000000000000000000111111111000",74=>"00000000000000000000111111111000",75=>"00000000000000000000111111111000",76=>"00000000000000000000111111111000",77=>"00000000000000000000111111111000",78=>"00000000000000000000111111111000",79=>"00000000000000000000111111111000",80=>"00000000000000000000111111111000",81=>"00000000000000000000111111111000",82=>"00000000000000000000111110000000",83=>"00000000000000000000111111000000",84=>"00000000000000000000111111000000",85=>"00000000000000000000111111100000",86=>"00000000000000000000111111110000",87=>"00000000000000000000111111111000");
begin
  
  assert g_rw_test_reg_size <= c_rw_test_reg_size report "g_rw_test_reg_size must be not greater than c_rw_test_reg_size=1" severity failure;
  assert g_elink_clk_select_size <= c_elink_clk_select_size report "g_elink_clk_select_size must be not greater than c_elink_clk_select_size=1" severity failure;
  assert g_main_status_size <= c_main_status_size report "g_main_status_size must be not greater than c_main_status_size=1" severity failure;
  assert g_main_ctrl_size <= c_main_ctrl_size report "g_main_ctrl_size must be not greater than c_main_ctrl_size=1" severity failure;
  assert g_mac_ls3bytes_size <= c_mac_ls3bytes_size report "g_mac_ls3bytes_size must be not greater than c_mac_ls3bytes_size=1" severity failure;
  assert g_mac_ms3bytes_size <= c_mac_ms3bytes_size report "g_mac_ms3bytes_size must be not greater than c_mac_ms3bytes_size=1" severity failure;
  assert g_mac_status_size <= c_mac_status_size report "g_mac_status_size must be not greater than c_mac_status_size=1" severity failure;
  assert g_j1b_ctrl_size <= c_j1b_ctrl_size report "g_j1b_ctrl_size must be not greater than c_j1b_ctrl_size=1" severity failure;
  assert g_system_timestamp_0_size <= c_system_timestamp_0_size report "g_system_timestamp_0_size must be not greater than c_system_timestamp_0_size=1" severity failure;
  assert g_system_timestamp_1_size <= c_system_timestamp_1_size report "g_system_timestamp_1_size must be not greater than c_system_timestamp_1_size=1" severity failure;
  assert g_uplink_80_size <= c_uplink_80_size report "g_uplink_80_size must be not greater than c_uplink_80_size=1" severity failure;
  assert g_uplink_160_size <= c_uplink_160_size report "g_uplink_160_size must be not greater than c_uplink_160_size=1" severity failure;
  assert g_rawfifos_size <= c_rawfifos_size report "g_rawfifos_size must be not greater than c_rawfifos_size=40" severity failure;
  assert g_rawfifos_time_size <= c_rawfifos_time_size report "g_rawfifos_time_size must be not greater than c_rawfifos_time_size=40" severity failure;
  assert g_elinks_size <= c_elinks_size report "g_elinks_size must be not greater than c_elinks_size=1" severity failure;
  assert g_hctsp_master_80_size <= c_hctsp_master_80_size report "g_hctsp_master_80_size must be not greater than c_hctsp_master_80_size=1" severity failure;
  assert g_hctsp_master_160_size <= c_hctsp_master_160_size report "g_hctsp_master_160_size must be not greater than c_hctsp_master_160_size=1" severity failure;
  assert g_uraw_ctrl_size <= c_uraw_ctrl_size report "g_uraw_ctrl_size must be not greater than c_uraw_ctrl_size=1" severity failure;
  assert g_i2c_master_size <= c_i2c_master_size report "g_i2c_master_size must be not greater than c_i2c_master_size=1" severity failure;

  wb_up_i <= slave_i;
  slave_o <= wb_up_o;
  int_addr <= int_regs_wb_m_o.adr(4-1 downto 0);

-- Conditional adding of xwb_register   
  gr1: if g_registered = 2 generate
    grl1: for i in 0 to 3-1 generate
      xwb_register_1: entity general_cores.xwb_register
      generic map (
        g_WB_MODE => CLASSIC)
      port map (
        rst_n_i  => rst_n_i,
        clk_i    => clk_sys_i,
        slave_i  => wb_up_i(i),
        slave_o  => wb_up_o(i),
        master_i => wb_up_r_o(i),
        master_o => wb_up_r_i(i));
    end generate grl1;
  end generate gr1;

  gr2: if g_registered /= 2 generate
      wb_up_r_i <= wb_up_i;
      wb_up_o <= wb_up_r_o;
  end generate gr2;

-- Main crossbar
  xwb_crossbar_1: entity general_cores.xwb_crossbar
  generic map (
     g_num_masters => 3,
     g_num_slaves  => 88,
     g_registered  => (g_registered = 1),
     g_address     => c_address,
     g_mask        => c_mask
  )
  port map (
     clk_sys_i => clk_sys_i,
     rst_n_i   => rst_n_i,
     slave_i   => wb_up_r_i,
     slave_o   => wb_up_r_o,
     master_i  => wb_m_i,
     master_o  => wb_m_o,
     sdb_sel_o => open
  );

-- Process for register access
  process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if rst_n_i = '0' then
        -- Reset of the core
        int_regs_wb_m_i <= c_DUMMY_WB_MASTER_IN;
        int_main_ctrl_o <= to_main_ctrl(std_logic_vector(to_unsigned(0,5))); -- Hex value: 0x0
        int_mac_ls3bytes_o <= std_logic_vector(to_unsigned(10556824,32)); -- Hex value: 0xa11598
        int_mac_ms3bytes_o <= std_logic_vector(to_unsigned(134619,32)); -- Hex value: 0x20ddb
        int_j1b_ctrl_o <= to_j1b_ctrl(std_logic_vector(to_unsigned(0,2))); -- Hex value: 0x0

      else
        -- Clearing of trigger bits (if there are any)

        -- Normal operation
        int_regs_wb_m_i.rty <= '0';
        int_regs_wb_m_i.ack <= '0';
        int_regs_wb_m_i.err <= '0';
        int_mac_ls3bytes_o_stb <= '0';
        system_timestamp_0_i_ack <= '0';

        if (int_regs_wb_m_o.cyc = '1') and (int_regs_wb_m_o.stb = '1')
            and (int_regs_wb_m_i.err = '0') and (int_regs_wb_m_i.rty = '0')
            and (int_regs_wb_m_i.ack = '0') then
          int_regs_wb_m_i.err <= '1'; -- in case of missed address
          -- Access, now we handle consecutive registers
          -- Set the error state so it is output when none register is accessed
          int_regs_wb_m_i.dat <= x"A5A5A5A5";
          int_regs_wb_m_i.ack <= '0';
          int_regs_wb_m_i.err <= '1';
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_rw_test_reg_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(2 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(31 downto 0) <= std_logic_vector(int_rw_test_reg_o);
              if int_regs_wb_m_o.we = '1' then
                int_rw_test_reg_o <= std_logic_vector(int_regs_wb_m_o.dat(31 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_rw_test_reg_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_elink_clk_select_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(3 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(1 downto 0) <= std_logic_vector(int_elink_clk_select_o);
              if int_regs_wb_m_o.we = '1' then
                int_elink_clk_select_o <= std_logic_vector(int_regs_wb_m_o.dat(1 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_elink_clk_select_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_main_status_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(4 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(8 downto 0) <= to_slv(main_status_i);
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_main_status_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_main_ctrl_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(5 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(4 downto 0) <= to_slv(int_main_ctrl_o);
              if int_regs_wb_m_o.we = '1' then
                int_main_ctrl_o <= to_main_ctrl(int_regs_wb_m_o.dat(4 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_main_ctrl_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_mac_ls3bytes_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(6 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(31 downto 0) <= std_logic_vector(int_mac_ls3bytes_o);
              if int_regs_wb_m_o.we = '1' then
                int_mac_ls3bytes_o <= std_logic_vector(int_regs_wb_m_o.dat(31 downto 0));
                if int_regs_wb_m_i.ack = '0' then
                  int_mac_ls3bytes_o_stb <= '1';
                end if;
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_mac_ls3bytes_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_mac_ms3bytes_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(7 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(31 downto 0) <= std_logic_vector(int_mac_ms3bytes_o);
              if int_regs_wb_m_o.we = '1' then
                int_mac_ms3bytes_o <= std_logic_vector(int_regs_wb_m_o.dat(31 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_mac_ms3bytes_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_mac_status_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(8 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(31 downto 0) <= std_logic_vector(mac_status_i);
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_mac_status_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_j1b_ctrl_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(9 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(1 downto 0) <= to_slv(int_j1b_ctrl_o);
              if int_regs_wb_m_o.we = '1' then
                int_j1b_ctrl_o <= to_j1b_ctrl(int_regs_wb_m_o.dat(1 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_j1b_ctrl_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_system_timestamp_0_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(10 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(31 downto 0) <= std_logic_vector(system_timestamp_0_i);
              if int_regs_wb_m_i.ack = '0' then
                 system_timestamp_0_i_ack <= '1';
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_system_timestamp_0_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_system_timestamp_1_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(11 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(31 downto 0) <= std_logic_vector(system_timestamp_1_i);
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_system_timestamp_1_size


          if int_addr = "0000" then
             int_regs_wb_m_i.dat <= x"1ed91fca";
             if int_regs_wb_m_o.we = '1' then
                int_regs_wb_m_i.err <= '1';
                int_regs_wb_m_i.ack <= '0';
             else
                int_regs_wb_m_i.ack <= '1';
                int_regs_wb_m_i.err <= '0';
             end if;
          end if;
          if int_addr = "0001" then
             int_regs_wb_m_i.dat <= g_ver_id;
             if int_regs_wb_m_o.we = '1' then
                int_regs_wb_m_i.err <= '1';
                int_regs_wb_m_i.ack <= '0';
             else
                int_regs_wb_m_i.ack <= '1';
                int_regs_wb_m_i.err <= '0';
             end if;
          end if;
        end if;
      end if;
    end if;
  end process;
  rw_test_reg_o <= int_rw_test_reg_o;
  elink_clk_select_o <= int_elink_clk_select_o;
  main_ctrl_o <= int_main_ctrl_o;
  mac_ls3bytes_o <= int_mac_ls3bytes_o;
  mac_ls3bytes_o_stb <= int_mac_ls3bytes_o_stb;
  mac_ms3bytes_o <= int_mac_ms3bytes_o;
  j1b_ctrl_o <= int_j1b_ctrl_o;
  bg1: if g_uplink_80_size > 0 generate
    wb_m_i(0) <= uplink_80_wb_m_i;
    uplink_80_wb_m_o  <= wb_m_o(0);
  end generate; -- g_uplink_80_size
  bg2: if g_uplink_160_size > 0 generate
    wb_m_i(1) <= uplink_160_wb_m_i;
    uplink_160_wb_m_o  <= wb_m_o(1);
  end generate; -- g_uplink_160_size
  bg3: for i in 0 to g_rawfifos_size - 1 generate
    wb_m_i(2 + i) <= rawfifos_wb_m_i(i);
    rawfifos_wb_m_o(i)  <= wb_m_o(2 + i);
  end generate; -- for g_rawfifos_size
  bg4: for i in 0 to g_rawfifos_time_size - 1 generate
    wb_m_i(42 + i) <= rawfifos_time_wb_m_i(i);
    rawfifos_time_wb_m_o(i)  <= wb_m_o(42 + i);
  end generate; -- for g_rawfifos_time_size
  bg5: if g_elinks_size > 0 generate
    wb_m_i(82) <= elinks_wb_m_i;
    elinks_wb_m_o  <= wb_m_o(82);
  end generate; -- g_elinks_size
  bg6: if g_hctsp_master_80_size > 0 generate
    wb_m_i(83) <= hctsp_master_80_wb_m_i;
    hctsp_master_80_wb_m_o  <= wb_m_o(83);
  end generate; -- g_hctsp_master_80_size
  bg7: if g_hctsp_master_160_size > 0 generate
    wb_m_i(84) <= hctsp_master_160_wb_m_i;
    hctsp_master_160_wb_m_o  <= wb_m_o(84);
  end generate; -- g_hctsp_master_160_size
  bg8: if g_uraw_ctrl_size > 0 generate
    wb_m_i(85) <= uraw_ctrl_wb_m_i;
    uraw_ctrl_wb_m_o  <= wb_m_o(85);
  end generate; -- g_uraw_ctrl_size
  wb_m_i(86) <= int_regs_wb_m_i;
  int_regs_wb_m_o  <= wb_m_o(86);
  bg9: if g_i2c_master_size > 0 generate
    wb_m_i(87) <= i2c_master_wb_m_i;
    i2c_master_wb_m_o  <= wb_m_o(87);
  end generate; -- g_i2c_master_size

end architecture;
