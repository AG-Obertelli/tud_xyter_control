--- This file has been automatically generated
--- by the agwb (https://github.com/wzab/agwb).
--- Please don't edit it manually, unless you really have to do it
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library general_cores;
use general_cores.wishbone_pkg.all;
library work;
use work.agwb_pkg.all;
use work.hctsp_uplink_pkg.all;

entity hctsp_uplink is
  generic (
    g_uplinks_mask_0_size : integer := c_uplinks_mask_0_size;
    g_uplinks_mask_1_size : integer := c_uplinks_mask_1_size;
    g_strict_mode_0_size : integer := c_strict_mode_0_size;
    g_strict_mode_1_size : integer := c_strict_mode_1_size;
    g_receivers_size : integer := c_receivers_size;
    g_ack_monitor_size : integer := c_ack_monitor_size;

    g_ver_id : std_logic_vector(31 downto 0) := c_hctsp_uplink_ver_id;
    g_registered : integer := 0
  );
  port (
    slave_i : in t_wishbone_slave_in;
    slave_o : out t_wishbone_slave_out;
    receivers_wb_m_o : out t_wishbone_master_out_array( g_receivers_size - 1 downto 0 );
    receivers_wb_m_i : in t_wishbone_master_in_array( g_receivers_size - 1 downto 0 ) := ( others => c_WB_SLAVE_OUT_ERR);
    ack_monitor_wb_m_o : out t_wishbone_master_out;
    ack_monitor_wb_m_i : in t_wishbone_master_in := c_WB_SLAVE_OUT_ERR;

    uplinks_mask_0_o : out  t_uplinks_mask_0;
    uplinks_mask_1_o : out  t_uplinks_mask_1;
    strict_mode_0_o : out  t_strict_mode_0;
    strict_mode_1_o : out  t_strict_mode_1;

    rst_n_i : in std_logic;
    clk_sys_i : in std_logic
    );

end hctsp_uplink;

architecture gener of hctsp_uplink is
  signal int_uplinks_mask_0_o : t_uplinks_mask_0 := std_logic_vector(to_unsigned(0,32)); -- Hex value: 0x0
  signal int_uplinks_mask_1_o : t_uplinks_mask_1 := std_logic_vector(to_unsigned(0,8)); -- Hex value: 0x0
  signal int_strict_mode_0_o : t_strict_mode_0 := std_logic_vector(to_unsigned(0,32)); -- Hex value: 0x0
  signal int_strict_mode_1_o : t_strict_mode_1 := std_logic_vector(to_unsigned(0,8)); -- Hex value: 0x0


  -- Internal WB declaration
  signal int_regs_wb_m_o : t_wishbone_master_out;
  signal int_regs_wb_m_i : t_wishbone_master_in;
  signal int_addr : std_logic_vector(3-1 downto 0);
  signal wb_up_o : t_wishbone_slave_out_array(1-1 downto 0);
  signal wb_up_i : t_wishbone_slave_in_array(1-1 downto 0);
  signal wb_up_r_o : t_wishbone_slave_out_array(1-1 downto 0);
  signal wb_up_r_i : t_wishbone_slave_in_array(1-1 downto 0);
  signal wb_m_o : t_wishbone_master_out_array(42-1 downto 0);
  signal wb_m_i : t_wishbone_master_in_array(42-1 downto 0) := (others => c_WB_SLAVE_OUT_ERR);

  -- Constants
  constant c_address : t_wishbone_address_array(42-1  downto 0) := (0=>"00000000000000000000001000000000",1=>"00000000000000000000001000001000",2=>"00000000000000000000001000010000",3=>"00000000000000000000001000011000",4=>"00000000000000000000001000100000",5=>"00000000000000000000001000101000",6=>"00000000000000000000001000110000",7=>"00000000000000000000001000111000",8=>"00000000000000000000001001000000",9=>"00000000000000000000001001001000",10=>"00000000000000000000001001010000",11=>"00000000000000000000001001011000",12=>"00000000000000000000001001100000",13=>"00000000000000000000001001101000",14=>"00000000000000000000001001110000",15=>"00000000000000000000001001111000",16=>"00000000000000000000001010000000",17=>"00000000000000000000001010001000",18=>"00000000000000000000001010010000",19=>"00000000000000000000001010011000",20=>"00000000000000000000001010100000",21=>"00000000000000000000001010101000",22=>"00000000000000000000001010110000",23=>"00000000000000000000001010111000",24=>"00000000000000000000001011000000",25=>"00000000000000000000001011001000",26=>"00000000000000000000001011010000",27=>"00000000000000000000001011011000",28=>"00000000000000000000001011100000",29=>"00000000000000000000001011101000",30=>"00000000000000000000001011110000",31=>"00000000000000000000001011111000",32=>"00000000000000000000001100000000",33=>"00000000000000000000001100001000",34=>"00000000000000000000001100010000",35=>"00000000000000000000001100011000",36=>"00000000000000000000001100100000",37=>"00000000000000000000001100101000",38=>"00000000000000000000001100110000",39=>"00000000000000000000001100111000",40=>"00000000000000000000000111110000",41=>"00000000000000000000000000000000");
  constant c_mask : t_wishbone_address_array(42-1 downto 0) := (0=>"00000000000000000000001111111000",1=>"00000000000000000000001111111000",2=>"00000000000000000000001111111000",3=>"00000000000000000000001111111000",4=>"00000000000000000000001111111000",5=>"00000000000000000000001111111000",6=>"00000000000000000000001111111000",7=>"00000000000000000000001111111000",8=>"00000000000000000000001111111000",9=>"00000000000000000000001111111000",10=>"00000000000000000000001111111000",11=>"00000000000000000000001111111000",12=>"00000000000000000000001111111000",13=>"00000000000000000000001111111000",14=>"00000000000000000000001111111000",15=>"00000000000000000000001111111000",16=>"00000000000000000000001111111000",17=>"00000000000000000000001111111000",18=>"00000000000000000000001111111000",19=>"00000000000000000000001111111000",20=>"00000000000000000000001111111000",21=>"00000000000000000000001111111000",22=>"00000000000000000000001111111000",23=>"00000000000000000000001111111000",24=>"00000000000000000000001111111000",25=>"00000000000000000000001111111000",26=>"00000000000000000000001111111000",27=>"00000000000000000000001111111000",28=>"00000000000000000000001111111000",29=>"00000000000000000000001111111000",30=>"00000000000000000000001111111000",31=>"00000000000000000000001111111000",32=>"00000000000000000000001111111000",33=>"00000000000000000000001111111000",34=>"00000000000000000000001111111000",35=>"00000000000000000000001111111000",36=>"00000000000000000000001111111000",37=>"00000000000000000000001111111000",38=>"00000000000000000000001111111000",39=>"00000000000000000000001111111000",40=>"00000000000000000000001111110000",41=>"00000000000000000000001111111000");
begin
  
  assert g_uplinks_mask_0_size <= c_uplinks_mask_0_size report "g_uplinks_mask_0_size must be not greater than c_uplinks_mask_0_size=1" severity failure;
  assert g_uplinks_mask_1_size <= c_uplinks_mask_1_size report "g_uplinks_mask_1_size must be not greater than c_uplinks_mask_1_size=1" severity failure;
  assert g_strict_mode_0_size <= c_strict_mode_0_size report "g_strict_mode_0_size must be not greater than c_strict_mode_0_size=1" severity failure;
  assert g_strict_mode_1_size <= c_strict_mode_1_size report "g_strict_mode_1_size must be not greater than c_strict_mode_1_size=1" severity failure;
  assert g_receivers_size <= c_receivers_size report "g_receivers_size must be not greater than c_receivers_size=40" severity failure;
  assert g_ack_monitor_size <= c_ack_monitor_size report "g_ack_monitor_size must be not greater than c_ack_monitor_size=1" severity failure;

  wb_up_i(0) <= slave_i;
  slave_o <= wb_up_o(0);
  int_addr <= int_regs_wb_m_o.adr(3-1 downto 0);

-- Conditional adding of xwb_register   
  gr1: if g_registered = 2 generate
    grl1: for i in 0 to 1-1 generate
      xwb_register_1: entity general_cores.xwb_register
      generic map (
        g_WB_MODE => CLASSIC)
      port map (
        rst_n_i  => rst_n_i,
        clk_i    => clk_sys_i,
        slave_i  => wb_up_i(i),
        slave_o  => wb_up_o(i),
        master_i => wb_up_r_o(i),
        master_o => wb_up_r_i(i));
    end generate grl1;
  end generate gr1;

  gr2: if g_registered /= 2 generate
      wb_up_r_i <= wb_up_i;
      wb_up_o <= wb_up_r_o;
  end generate gr2;

-- Main crossbar
  xwb_crossbar_1: entity general_cores.xwb_crossbar
  generic map (
     g_num_masters => 1,
     g_num_slaves  => 42,
     g_registered  => (g_registered = 1),
     g_address     => c_address,
     g_mask        => c_mask
  )
  port map (
     clk_sys_i => clk_sys_i,
     rst_n_i   => rst_n_i,
     slave_i   => wb_up_r_i,
     slave_o   => wb_up_r_o,
     master_i  => wb_m_i,
     master_o  => wb_m_o,
     sdb_sel_o => open
  );

-- Process for register access
  process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if rst_n_i = '0' then
        -- Reset of the core
        int_regs_wb_m_i <= c_DUMMY_WB_MASTER_IN;
        int_uplinks_mask_0_o <= std_logic_vector(to_unsigned(0,32)); -- Hex value: 0x0
        int_uplinks_mask_1_o <= std_logic_vector(to_unsigned(0,8)); -- Hex value: 0x0
        int_strict_mode_0_o <= std_logic_vector(to_unsigned(0,32)); -- Hex value: 0x0
        int_strict_mode_1_o <= std_logic_vector(to_unsigned(0,8)); -- Hex value: 0x0

      else
        -- Clearing of trigger bits (if there are any)

        -- Normal operation
        int_regs_wb_m_i.rty <= '0';
        int_regs_wb_m_i.ack <= '0';
        int_regs_wb_m_i.err <= '0';

        if (int_regs_wb_m_o.cyc = '1') and (int_regs_wb_m_o.stb = '1')
            and (int_regs_wb_m_i.err = '0') and (int_regs_wb_m_i.rty = '0')
            and (int_regs_wb_m_i.ack = '0') then
          int_regs_wb_m_i.err <= '1'; -- in case of missed address
          -- Access, now we handle consecutive registers
          -- Set the error state so it is output when none register is accessed
          int_regs_wb_m_i.dat <= x"A5A5A5A5";
          int_regs_wb_m_i.ack <= '0';
          int_regs_wb_m_i.err <= '1';
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_uplinks_mask_0_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(2 + i, 3)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(31 downto 0) <= std_logic_vector(int_uplinks_mask_0_o);
              if int_regs_wb_m_o.we = '1' then
                int_uplinks_mask_0_o <= std_logic_vector(int_regs_wb_m_o.dat(31 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_uplinks_mask_0_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_uplinks_mask_1_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(3 + i, 3)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(7 downto 0) <= std_logic_vector(int_uplinks_mask_1_o);
              if int_regs_wb_m_o.we = '1' then
                int_uplinks_mask_1_o <= std_logic_vector(int_regs_wb_m_o.dat(7 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_uplinks_mask_1_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_strict_mode_0_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(4 + i, 3)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(31 downto 0) <= std_logic_vector(int_strict_mode_0_o);
              if int_regs_wb_m_o.we = '1' then
                int_strict_mode_0_o <= std_logic_vector(int_regs_wb_m_o.dat(31 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_strict_mode_0_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_strict_mode_1_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(5 + i, 3)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(7 downto 0) <= std_logic_vector(int_strict_mode_1_o);
              if int_regs_wb_m_o.we = '1' then
                int_strict_mode_1_o <= std_logic_vector(int_regs_wb_m_o.dat(7 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_strict_mode_1_size


          if int_addr = "000" then
             int_regs_wb_m_i.dat <= x"6629c4cc";
             if int_regs_wb_m_o.we = '1' then
                int_regs_wb_m_i.err <= '1';
                int_regs_wb_m_i.ack <= '0';
             else
                int_regs_wb_m_i.ack <= '1';
                int_regs_wb_m_i.err <= '0';
             end if;
          end if;
          if int_addr = "001" then
             int_regs_wb_m_i.dat <= g_ver_id;
             if int_regs_wb_m_o.we = '1' then
                int_regs_wb_m_i.err <= '1';
                int_regs_wb_m_i.ack <= '0';
             else
                int_regs_wb_m_i.ack <= '1';
                int_regs_wb_m_i.err <= '0';
             end if;
          end if;
        end if;
      end if;
    end if;
  end process;
  uplinks_mask_0_o <= int_uplinks_mask_0_o;
  uplinks_mask_1_o <= int_uplinks_mask_1_o;
  strict_mode_0_o <= int_strict_mode_0_o;
  strict_mode_1_o <= int_strict_mode_1_o;
  bg1: for i in 0 to g_receivers_size - 1 generate
    wb_m_i(0 + i) <= receivers_wb_m_i(i);
    receivers_wb_m_o(i)  <= wb_m_o(0 + i);
  end generate; -- for g_receivers_size
  bg2: if g_ack_monitor_size > 0 generate
    wb_m_i(40) <= ack_monitor_wb_m_i;
    ack_monitor_wb_m_o  <= wb_m_o(40);
  end generate; -- g_ack_monitor_size
  wb_m_i(41) <= int_regs_wb_m_i;
  int_regs_wb_m_o  <= wb_m_o(41);

end architecture;
