--- This file has been automatically generated
--- by the agwb (https://github.com/wzab/agwb).
--- Please don't edit it manually, unless you really have to do it
library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library general_cores;
use general_cores.wishbone_pkg.all;

library work;
use work.agwb_pkg.all;

package elinks_pkg is

  constant C_elinks_ADDR_BITS : integer := 7;

  constant c_elinks_ver_id : std_logic_vector(31 downto 0) := x"1035549d";
  constant v_elinks_ver_id : t_ver_id_variants(0 downto 0) := (0 => x"1035549d");
  constant c_clk_ctrl_size : integer := 1;
  constant c_clk_stat_size : integer := 1;
  constant c_in_ctrl_size : integer := 40;
  constant c_in_stat_size : integer := 40;
  constant c_downlink_mask_size : integer := 1;


  constant C_clk_ctrl_REG_ADDR: unsigned := x"00000002";
  type t_clk_ctrl is record
    output:std_logic_vector(2 downto 0);
    freq:std_logic_vector(1 downto 0);
    delay_stb:std_logic_vector(0 downto 0);
    delay:std_logic_vector(8 downto 0);
  end record;
  
  function to_clk_ctrl(x : std_logic_vector) return t_clk_ctrl;
  function to_slv(x : t_clk_ctrl) return std_logic_vector;
  constant C_clk_stat_REG_ADDR: unsigned := x"00000003";
  type t_clk_stat is record
    delay_ready:std_logic_vector(0 downto 0);
    pll_locked:std_logic_vector(0 downto 0);
  end record;
  
  function to_clk_stat(x : std_logic_vector) return t_clk_stat;
  function to_slv(x : t_clk_stat) return std_logic_vector;
  constant C_in_ctrl_REG_ADDR: unsigned := x"00000004";
  type t_in_ctrl is record
    din_delay:std_logic_vector(4 downto 0);
    bit_select:std_logic_vector(2 downto 0);
  end record;
  
  function to_in_ctrl(x : std_logic_vector) return t_in_ctrl;
  function to_slv(x : t_in_ctrl) return std_logic_vector;
  type ut_in_ctrl_array is array( natural range <> ) of t_in_ctrl;
  subtype t_in_ctrl_array is ut_in_ctrl_array(c_in_ctrl_size - 1 downto 0);
  constant C_in_stat_REG_ADDR: unsigned := x"0000002c";
  type t_in_stat is record
    din_delay_ready:std_logic_vector(0 downto 0);
  end record;
  
  function to_in_stat(x : std_logic_vector) return t_in_stat;
  function to_slv(x : t_in_stat) return std_logic_vector;
  type ut_in_stat_array is array( natural range <> ) of t_in_stat;
  subtype t_in_stat_array is ut_in_stat_array(c_in_stat_size - 1 downto 0);
  constant C_downlink_mask_REG_ADDR: unsigned := x"00000054";
  subtype t_downlink_mask is std_logic_vector(4 downto 0);


end elinks_pkg;

package body elinks_pkg is
  function to_clk_ctrl(x : std_logic_vector) return t_clk_ctrl is
    variable res : t_clk_ctrl;
  begin
    res.output := std_logic_vector(x(2 downto 0));
    res.freq := std_logic_vector(x(4 downto 3));
    res.delay_stb := std_logic_vector(x(5 downto 5));
    res.delay := std_logic_vector(x(14 downto 6));
    return res;
  end function;
  
  function to_slv(x : t_clk_ctrl) return std_logic_vector is
    variable res : std_logic_vector(14 downto 0);
  begin
    res(2 downto 0) := std_logic_vector(x.output);
    res(4 downto 3) := std_logic_vector(x.freq);
    res(5 downto 5) := std_logic_vector(x.delay_stb);
    res(14 downto 6) := std_logic_vector(x.delay);
    return res;
  end function;
  
  function to_clk_stat(x : std_logic_vector) return t_clk_stat is
    variable res : t_clk_stat;
  begin
    res.delay_ready := std_logic_vector(x(0 downto 0));
    res.pll_locked := std_logic_vector(x(1 downto 1));
    return res;
  end function;
  
  function to_slv(x : t_clk_stat) return std_logic_vector is
    variable res : std_logic_vector(1 downto 0);
  begin
    res(0 downto 0) := std_logic_vector(x.delay_ready);
    res(1 downto 1) := std_logic_vector(x.pll_locked);
    return res;
  end function;
  
  function to_in_ctrl(x : std_logic_vector) return t_in_ctrl is
    variable res : t_in_ctrl;
  begin
    res.din_delay := std_logic_vector(x(4 downto 0));
    res.bit_select := std_logic_vector(x(7 downto 5));
    return res;
  end function;
  
  function to_slv(x : t_in_ctrl) return std_logic_vector is
    variable res : std_logic_vector(7 downto 0);
  begin
    res(4 downto 0) := std_logic_vector(x.din_delay);
    res(7 downto 5) := std_logic_vector(x.bit_select);
    return res;
  end function;
  
  function to_in_stat(x : std_logic_vector) return t_in_stat is
    variable res : t_in_stat;
  begin
    res.din_delay_ready := std_logic_vector(x(0 downto 0));
    return res;
  end function;
  
  function to_slv(x : t_in_stat) return std_logic_vector is
    variable res : std_logic_vector(0 downto 0);
  begin
    res(0 downto 0) := std_logic_vector(x.din_delay_ready);
    return res;
  end function;
  

end elinks_pkg;
