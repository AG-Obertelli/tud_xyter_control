--- This file has been automatically generated
--- by the agwb (https://github.com/wzab/agwb).
--- Please don't edit it manually, unless you really have to do it
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library general_cores;
use general_cores.wishbone_pkg.all;
library work;
use work.agwb_pkg.all;
use work.uraw_ctrl_pkg.all;

entity uraw_ctrl is
  generic (
    g_uraw_uplinks_size : integer := c_uraw_uplinks_size;
    g_uraw_filter_pattern_size : integer := c_uraw_filter_pattern_size;
    g_uraw_filter_mask_size : integer := c_uraw_filter_mask_size;
    g_uraw_inhibit_pattern_size : integer := c_uraw_inhibit_pattern_size;
    g_uraw_inhibit_mask_size : integer := c_uraw_inhibit_mask_size;
    g_uraw_fifo_size : integer := c_uraw_fifo_size;

    g_ver_id : std_logic_vector(31 downto 0) := c_uraw_ctrl_ver_id;
    g_registered : integer := 0
  );
  port (
    slave_i : in t_wishbone_slave_in;
    slave_o : out t_wishbone_slave_out;
    uraw_fifo_wb_m_o : out t_wishbone_master_out;
    uraw_fifo_wb_m_i : in t_wishbone_master_in := c_WB_SLAVE_OUT_ERR;

    uraw_uplinks_o : out  ut_uraw_uplinks_array(g_uraw_uplinks_size - 1 downto 0);
    uraw_filter_pattern_o : out  t_uraw_filter_pattern;
    uraw_filter_mask_o : out  t_uraw_filter_mask;
    uraw_inhibit_pattern_o : out  t_uraw_inhibit_pattern;
    uraw_inhibit_mask_o : out  t_uraw_inhibit_mask;

    rst_n_i : in std_logic;
    clk_sys_i : in std_logic
    );

end uraw_ctrl;

architecture gener of uraw_ctrl is
  signal int_uraw_uplinks_o : ut_uraw_uplinks_array(g_uraw_uplinks_size - 1 downto 0) := (others => to_unsigned(40,7)); -- Hex value: 0x28
  signal int_uraw_filter_pattern_o : t_uraw_filter_pattern := std_logic_vector(to_unsigned(0,24)); -- Hex value: 0x0
  signal int_uraw_filter_mask_o : t_uraw_filter_mask := std_logic_vector(to_unsigned(0,24)); -- Hex value: 0x0
  signal int_uraw_inhibit_pattern_o : t_uraw_inhibit_pattern := std_logic_vector(to_unsigned(0,24)); -- Hex value: 0x0
  signal int_uraw_inhibit_mask_o : t_uraw_inhibit_mask := std_logic_vector(to_unsigned(16777215,24)); -- Hex value: 0xffffff


  -- Internal WB declaration
  signal int_regs_wb_m_o : t_wishbone_master_out;
  signal int_regs_wb_m_i : t_wishbone_master_in;
  signal int_addr : std_logic_vector(4-1 downto 0);
  signal wb_up_o : t_wishbone_slave_out_array(1-1 downto 0);
  signal wb_up_i : t_wishbone_slave_in_array(1-1 downto 0);
  signal wb_up_r_o : t_wishbone_slave_out_array(1-1 downto 0);
  signal wb_up_r_i : t_wishbone_slave_in_array(1-1 downto 0);
  signal wb_m_o : t_wishbone_master_out_array(2-1 downto 0);
  signal wb_m_i : t_wishbone_master_in_array(2-1 downto 0) := (others => c_WB_SLAVE_OUT_ERR);

  -- Constants
  constant c_address : t_wishbone_address_array(2-1  downto 0) := (0=>"00000000000000000000000000000000",1=>"00000000000000000000000000010000");
  constant c_mask : t_wishbone_address_array(2-1 downto 0) := (0=>"00000000000000000000000000010000",1=>"00000000000000000000000000010000");
begin
  
  assert g_uraw_uplinks_size <= c_uraw_uplinks_size report "g_uraw_uplinks_size must be not greater than c_uraw_uplinks_size=10" severity failure;
  assert g_uraw_filter_pattern_size <= c_uraw_filter_pattern_size report "g_uraw_filter_pattern_size must be not greater than c_uraw_filter_pattern_size=1" severity failure;
  assert g_uraw_filter_mask_size <= c_uraw_filter_mask_size report "g_uraw_filter_mask_size must be not greater than c_uraw_filter_mask_size=1" severity failure;
  assert g_uraw_inhibit_pattern_size <= c_uraw_inhibit_pattern_size report "g_uraw_inhibit_pattern_size must be not greater than c_uraw_inhibit_pattern_size=1" severity failure;
  assert g_uraw_inhibit_mask_size <= c_uraw_inhibit_mask_size report "g_uraw_inhibit_mask_size must be not greater than c_uraw_inhibit_mask_size=1" severity failure;
  assert g_uraw_fifo_size <= c_uraw_fifo_size report "g_uraw_fifo_size must be not greater than c_uraw_fifo_size=1" severity failure;

  wb_up_i(0) <= slave_i;
  slave_o <= wb_up_o(0);
  int_addr <= int_regs_wb_m_o.adr(4-1 downto 0);

-- Conditional adding of xwb_register   
  gr1: if g_registered = 2 generate
    grl1: for i in 0 to 1-1 generate
      xwb_register_1: entity general_cores.xwb_register
      generic map (
        g_WB_MODE => CLASSIC)
      port map (
        rst_n_i  => rst_n_i,
        clk_i    => clk_sys_i,
        slave_i  => wb_up_i(i),
        slave_o  => wb_up_o(i),
        master_i => wb_up_r_o(i),
        master_o => wb_up_r_i(i));
    end generate grl1;
  end generate gr1;

  gr2: if g_registered /= 2 generate
      wb_up_r_i <= wb_up_i;
      wb_up_o <= wb_up_r_o;
  end generate gr2;

-- Main crossbar
  xwb_crossbar_1: entity general_cores.xwb_crossbar
  generic map (
     g_num_masters => 1,
     g_num_slaves  => 2,
     g_registered  => (g_registered = 1),
     g_address     => c_address,
     g_mask        => c_mask
  )
  port map (
     clk_sys_i => clk_sys_i,
     rst_n_i   => rst_n_i,
     slave_i   => wb_up_r_i,
     slave_o   => wb_up_r_o,
     master_i  => wb_m_i,
     master_o  => wb_m_o,
     sdb_sel_o => open
  );

-- Process for register access
  process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if rst_n_i = '0' then
        -- Reset of the core
        int_regs_wb_m_i <= c_DUMMY_WB_MASTER_IN;
        int_uraw_uplinks_o <= (others => to_unsigned(40,7)); -- Hex value: 0x28
        int_uraw_filter_pattern_o <= std_logic_vector(to_unsigned(0,24)); -- Hex value: 0x0
        int_uraw_filter_mask_o <= std_logic_vector(to_unsigned(0,24)); -- Hex value: 0x0
        int_uraw_inhibit_pattern_o <= std_logic_vector(to_unsigned(0,24)); -- Hex value: 0x0
        int_uraw_inhibit_mask_o <= std_logic_vector(to_unsigned(16777215,24)); -- Hex value: 0xffffff

      else
        -- Clearing of trigger bits (if there are any)

        -- Normal operation
        int_regs_wb_m_i.rty <= '0';
        int_regs_wb_m_i.ack <= '0';
        int_regs_wb_m_i.err <= '0';

        if (int_regs_wb_m_o.cyc = '1') and (int_regs_wb_m_o.stb = '1')
            and (int_regs_wb_m_i.err = '0') and (int_regs_wb_m_i.rty = '0')
            and (int_regs_wb_m_i.ack = '0') then
          int_regs_wb_m_i.err <= '1'; -- in case of missed address
          -- Access, now we handle consecutive registers
          -- Set the error state so it is output when none register is accessed
          int_regs_wb_m_i.dat <= x"A5A5A5A5";
          int_regs_wb_m_i.ack <= '0';
          int_regs_wb_m_i.err <= '1';
          for i in 0 to g_uraw_uplinks_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(2 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(6 downto 0) <= std_logic_vector(int_uraw_uplinks_o( i ));
              if int_regs_wb_m_o.we = '1' then
                int_uraw_uplinks_o( i ) <= unsigned(int_regs_wb_m_o.dat(6 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_uraw_uplinks_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_uraw_filter_pattern_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(12 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(23 downto 0) <= std_logic_vector(int_uraw_filter_pattern_o);
              if int_regs_wb_m_o.we = '1' then
                int_uraw_filter_pattern_o <= std_logic_vector(int_regs_wb_m_o.dat(23 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_uraw_filter_pattern_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_uraw_filter_mask_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(13 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(23 downto 0) <= std_logic_vector(int_uraw_filter_mask_o);
              if int_regs_wb_m_o.we = '1' then
                int_uraw_filter_mask_o <= std_logic_vector(int_regs_wb_m_o.dat(23 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_uraw_filter_mask_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_uraw_inhibit_pattern_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(14 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(23 downto 0) <= std_logic_vector(int_uraw_inhibit_pattern_o);
              if int_regs_wb_m_o.we = '1' then
                int_uraw_inhibit_pattern_o <= std_logic_vector(int_regs_wb_m_o.dat(23 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_uraw_inhibit_pattern_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_uraw_inhibit_mask_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(15 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(23 downto 0) <= std_logic_vector(int_uraw_inhibit_mask_o);
              if int_regs_wb_m_o.we = '1' then
                int_uraw_inhibit_mask_o <= std_logic_vector(int_regs_wb_m_o.dat(23 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_uraw_inhibit_mask_size


          if int_addr = "0000" then
             int_regs_wb_m_i.dat <= x"108b888d";
             if int_regs_wb_m_o.we = '1' then
                int_regs_wb_m_i.err <= '1';
                int_regs_wb_m_i.ack <= '0';
             else
                int_regs_wb_m_i.ack <= '1';
                int_regs_wb_m_i.err <= '0';
             end if;
          end if;
          if int_addr = "0001" then
             int_regs_wb_m_i.dat <= g_ver_id;
             if int_regs_wb_m_o.we = '1' then
                int_regs_wb_m_i.err <= '1';
                int_regs_wb_m_i.ack <= '0';
             else
                int_regs_wb_m_i.ack <= '1';
                int_regs_wb_m_i.err <= '0';
             end if;
          end if;
        end if;
      end if;
    end if;
  end process;
  uraw_uplinks_o <= int_uraw_uplinks_o;
  uraw_filter_pattern_o <= int_uraw_filter_pattern_o;
  uraw_filter_mask_o <= int_uraw_filter_mask_o;
  uraw_inhibit_pattern_o <= int_uraw_inhibit_pattern_o;
  uraw_inhibit_mask_o <= int_uraw_inhibit_mask_o;
  wb_m_i(0) <= int_regs_wb_m_i;
  int_regs_wb_m_o  <= wb_m_o(0);
  bg1: if g_uraw_fifo_size > 0 generate
    wb_m_i(1) <= uraw_fifo_wb_m_i;
    uraw_fifo_wb_m_o  <= wb_m_o(1);
  end generate; -- g_uraw_fifo_size

end architecture;
