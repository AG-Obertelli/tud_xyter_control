--- This file has been automatically generated
--- by the agwb (https://github.com/wzab/agwb).
--- Please don't edit it manually, unless you really have to do it
library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library general_cores;
use general_cores.wishbone_pkg.all;

library work;
use work.agwb_pkg.all;

package hctsp_ack_monitor_pkg is

  constant C_hctsp_ack_monitor_ADDR_BITS : integer := 4;

  constant c_hctsp_ack_monitor_ver_id : std_logic_vector(31 downto 0) := x"60239d38";
  constant v_hctsp_ack_monitor_ver_id : t_ver_id_variants(0 downto 0) := (0 => x"60239d38");
  constant c_frame_size : integer := 1;
  constant c_clear_detected_size : integer := 2;
  constant c_detected_0_size : integer := 2;
  constant c_detected_1_size : integer := 2;


  constant C_frame_REG_ADDR: unsigned := x"00000002";
  type t_frame is record
    missed:std_logic_vector(0 downto 0);
    uplink_number:std_logic_vector(5 downto 0);
    valid:std_logic_vector(0 downto 0);
    frame:std_logic_vector(23 downto 0);
  end record;
  
  function to_frame(x : std_logic_vector) return t_frame;
  function to_slv(x : t_frame) return std_logic_vector;
  constant C_clear_detected_REG_ADDR: unsigned := x"00000003";
  subtype t_clear_detected is std_logic_vector(31 downto 0);
  type ut_clear_detected_array is array( natural range <> ) of t_clear_detected;
  subtype t_clear_detected_array is ut_clear_detected_array(c_clear_detected_size - 1 downto 0);
  constant C_detected_0_REG_ADDR: unsigned := x"00000005";
  subtype t_detected_0 is std_logic_vector(31 downto 0);
  type ut_detected_0_array is array( natural range <> ) of t_detected_0;
  subtype t_detected_0_array is ut_detected_0_array(c_detected_0_size - 1 downto 0);
  constant C_detected_1_REG_ADDR: unsigned := x"00000007";
  subtype t_detected_1 is std_logic_vector(7 downto 0);
  type ut_detected_1_array is array( natural range <> ) of t_detected_1;
  subtype t_detected_1_array is ut_detected_1_array(c_detected_1_size - 1 downto 0);


end hctsp_ack_monitor_pkg;

package body hctsp_ack_monitor_pkg is
  function to_frame(x : std_logic_vector) return t_frame is
    variable res : t_frame;
  begin
    res.missed := std_logic_vector(x(0 downto 0));
    res.uplink_number := std_logic_vector(x(6 downto 1));
    res.valid := std_logic_vector(x(7 downto 7));
    res.frame := std_logic_vector(x(31 downto 8));
    return res;
  end function;
  
  function to_slv(x : t_frame) return std_logic_vector is
    variable res : std_logic_vector(31 downto 0);
  begin
    res(0 downto 0) := std_logic_vector(x.missed);
    res(6 downto 1) := std_logic_vector(x.uplink_number);
    res(7 downto 7) := std_logic_vector(x.valid);
    res(31 downto 8) := std_logic_vector(x.frame);
    return res;
  end function;
  

end hctsp_ack_monitor_pkg;
