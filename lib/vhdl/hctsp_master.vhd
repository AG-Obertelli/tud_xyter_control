--- This file has been automatically generated
--- by the agwb (https://github.com/wzab/agwb).
--- Please don't edit it manually, unless you really have to do it
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library general_cores;
use general_cores.wishbone_pkg.all;
library work;
use work.agwb_pkg.all;
use work.hctsp_master_pkg.all;

entity hctsp_master is
  generic (
    g_fetched_count_size : integer := c_fetched_count_size;
    g_command_cycle_timestamp_0_size : integer := c_command_cycle_timestamp_0_size;
    g_command_cycle_timestamp_1_size : integer := c_command_cycle_timestamp_1_size;
    g_command_cycle_pause_width_size : integer := c_command_cycle_pause_width_size;
    g_encoding_modes_size : integer := c_encoding_modes_size;
    g_time_command_slots_size : integer := c_time_command_slots_size;
    g_special_time_command_slot_size : integer := c_special_time_command_slot_size;
    g_software_command_slot_size : integer := c_software_command_slot_size;

    g_ver_id : std_logic_vector(31 downto 0) := c_hctsp_master_ver_id;
    g_registered : integer := 0
  );
  port (
    slave_i : in t_wishbone_slave_in;
    slave_o : out t_wishbone_slave_out;
    time_command_slots_wb_m_o : out t_wishbone_master_out_array( g_time_command_slots_size - 1 downto 0 );
    time_command_slots_wb_m_i : in t_wishbone_master_in_array( g_time_command_slots_size - 1 downto 0 ) := ( others => c_WB_SLAVE_OUT_ERR);
    special_time_command_slot_wb_m_o : out t_wishbone_master_out;
    special_time_command_slot_wb_m_i : in t_wishbone_master_in := c_WB_SLAVE_OUT_ERR;
    software_command_slot_wb_m_o : out t_wishbone_master_out;
    software_command_slot_wb_m_i : in t_wishbone_master_in := c_WB_SLAVE_OUT_ERR;

    fetched_count_i : in  t_fetched_count;
    command_cycle_timestamp_0_i : in  t_command_cycle_timestamp_0;
    command_cycle_timestamp_0_i_ack : out std_logic;
    command_cycle_timestamp_1_i : in  t_command_cycle_timestamp_1;
    command_cycle_pause_width_o : out  t_command_cycle_pause_width;
    command_cycle_pause_width_o_stb : out std_logic;
    encoding_modes_o : out  ut_encoding_modes_array(g_encoding_modes_size - 1 downto 0);

    rst_n_i : in std_logic;
    clk_sys_i : in std_logic
    );

end hctsp_master;

architecture gener of hctsp_master is
  signal int_command_cycle_pause_width_o : t_command_cycle_pause_width;
  signal int_command_cycle_pause_width_o_stb : std_logic;
  signal int_encoding_modes_o : ut_encoding_modes_array(g_encoding_modes_size - 1 downto 0) := (others => std_logic_vector(to_unsigned(0,2))); -- Hex value: 0x0


  -- Internal WB declaration
  signal int_regs_wb_m_o : t_wishbone_master_out;
  signal int_regs_wb_m_i : t_wishbone_master_in;
  signal int_addr : std_logic_vector(4-1 downto 0);
  signal wb_up_o : t_wishbone_slave_out_array(1-1 downto 0);
  signal wb_up_i : t_wishbone_slave_in_array(1-1 downto 0);
  signal wb_up_r_o : t_wishbone_slave_out_array(1-1 downto 0);
  signal wb_up_r_i : t_wishbone_slave_in_array(1-1 downto 0);
  signal wb_m_o : t_wishbone_master_out_array(7-1 downto 0);
  signal wb_m_i : t_wishbone_master_in_array(7-1 downto 0) := (others => c_WB_SLAVE_OUT_ERR);

  -- Constants
  constant c_address : t_wishbone_address_array(7-1  downto 0) := (0=>"00000000000000000000000000100000",1=>"00000000000000000000000000101000",2=>"00000000000000000000000000110000",3=>"00000000000000000000000000111000",4=>"00000000000000000000000000000000",5=>"00000000000000000000000000011000",6=>"00000000000000000000000000010000");
  constant c_mask : t_wishbone_address_array(7-1 downto 0) := (0=>"00000000000000000000000000111000",1=>"00000000000000000000000000111000",2=>"00000000000000000000000000111000",3=>"00000000000000000000000000111000",4=>"00000000000000000000000000110000",5=>"00000000000000000000000000111000",6=>"00000000000000000000000000111000");
begin
  
  assert g_fetched_count_size <= c_fetched_count_size report "g_fetched_count_size must be not greater than c_fetched_count_size=1" severity failure;
  assert g_command_cycle_timestamp_0_size <= c_command_cycle_timestamp_0_size report "g_command_cycle_timestamp_0_size must be not greater than c_command_cycle_timestamp_0_size=1" severity failure;
  assert g_command_cycle_timestamp_1_size <= c_command_cycle_timestamp_1_size report "g_command_cycle_timestamp_1_size must be not greater than c_command_cycle_timestamp_1_size=1" severity failure;
  assert g_command_cycle_pause_width_size <= c_command_cycle_pause_width_size report "g_command_cycle_pause_width_size must be not greater than c_command_cycle_pause_width_size=1" severity failure;
  assert g_encoding_modes_size <= c_encoding_modes_size report "g_encoding_modes_size must be not greater than c_encoding_modes_size=5" severity failure;
  assert g_time_command_slots_size <= c_time_command_slots_size report "g_time_command_slots_size must be not greater than c_time_command_slots_size=4" severity failure;
  assert g_special_time_command_slot_size <= c_special_time_command_slot_size report "g_special_time_command_slot_size must be not greater than c_special_time_command_slot_size=1" severity failure;
  assert g_software_command_slot_size <= c_software_command_slot_size report "g_software_command_slot_size must be not greater than c_software_command_slot_size=1" severity failure;

  wb_up_i(0) <= slave_i;
  slave_o <= wb_up_o(0);
  int_addr <= int_regs_wb_m_o.adr(4-1 downto 0);

-- Conditional adding of xwb_register   
  gr1: if g_registered = 2 generate
    grl1: for i in 0 to 1-1 generate
      xwb_register_1: entity general_cores.xwb_register
      generic map (
        g_WB_MODE => CLASSIC)
      port map (
        rst_n_i  => rst_n_i,
        clk_i    => clk_sys_i,
        slave_i  => wb_up_i(i),
        slave_o  => wb_up_o(i),
        master_i => wb_up_r_o(i),
        master_o => wb_up_r_i(i));
    end generate grl1;
  end generate gr1;

  gr2: if g_registered /= 2 generate
      wb_up_r_i <= wb_up_i;
      wb_up_o <= wb_up_r_o;
  end generate gr2;

-- Main crossbar
  xwb_crossbar_1: entity general_cores.xwb_crossbar
  generic map (
     g_num_masters => 1,
     g_num_slaves  => 7,
     g_registered  => (g_registered = 1),
     g_address     => c_address,
     g_mask        => c_mask
  )
  port map (
     clk_sys_i => clk_sys_i,
     rst_n_i   => rst_n_i,
     slave_i   => wb_up_r_i,
     slave_o   => wb_up_r_o,
     master_i  => wb_m_i,
     master_o  => wb_m_o,
     sdb_sel_o => open
  );

-- Process for register access
  process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if rst_n_i = '0' then
        -- Reset of the core
        int_regs_wb_m_i <= c_DUMMY_WB_MASTER_IN;
        int_encoding_modes_o <= (others => std_logic_vector(to_unsigned(0,2))); -- Hex value: 0x0

      else
        -- Clearing of trigger bits (if there are any)

        -- Normal operation
        int_regs_wb_m_i.rty <= '0';
        int_regs_wb_m_i.ack <= '0';
        int_regs_wb_m_i.err <= '0';
        command_cycle_timestamp_0_i_ack <= '0';
        int_command_cycle_pause_width_o_stb <= '0';

        if (int_regs_wb_m_o.cyc = '1') and (int_regs_wb_m_o.stb = '1')
            and (int_regs_wb_m_i.err = '0') and (int_regs_wb_m_i.rty = '0')
            and (int_regs_wb_m_i.ack = '0') then
          int_regs_wb_m_i.err <= '1'; -- in case of missed address
          -- Access, now we handle consecutive registers
          -- Set the error state so it is output when none register is accessed
          int_regs_wb_m_i.dat <= x"A5A5A5A5";
          int_regs_wb_m_i.ack <= '0';
          int_regs_wb_m_i.err <= '1';
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_fetched_count_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(2 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(31 downto 0) <= std_logic_vector(fetched_count_i);
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_fetched_count_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_command_cycle_timestamp_0_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(3 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(31 downto 0) <= std_logic_vector(command_cycle_timestamp_0_i);
              if int_regs_wb_m_i.ack = '0' then
                 command_cycle_timestamp_0_i_ack <= '1';
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_command_cycle_timestamp_0_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_command_cycle_timestamp_1_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(4 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(31 downto 0) <= std_logic_vector(command_cycle_timestamp_1_i);
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_command_cycle_timestamp_1_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_command_cycle_pause_width_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(5 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(5 downto 0) <= std_logic_vector(int_command_cycle_pause_width_o);
              if int_regs_wb_m_o.we = '1' then
                int_command_cycle_pause_width_o <= std_logic_vector(int_regs_wb_m_o.dat(5 downto 0));
                if int_regs_wb_m_i.ack = '0' then
                  int_command_cycle_pause_width_o_stb <= '1';
                end if;
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_command_cycle_pause_width_size
          for i in 0 to g_encoding_modes_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(6 + i, 4)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(1 downto 0) <= std_logic_vector(int_encoding_modes_o( i ));
              if int_regs_wb_m_o.we = '1' then
                int_encoding_modes_o( i ) <= std_logic_vector(int_regs_wb_m_o.dat(1 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_encoding_modes_size


          if int_addr = "0000" then
             int_regs_wb_m_i.dat <= x"4f37fce9";
             if int_regs_wb_m_o.we = '1' then
                int_regs_wb_m_i.err <= '1';
                int_regs_wb_m_i.ack <= '0';
             else
                int_regs_wb_m_i.ack <= '1';
                int_regs_wb_m_i.err <= '0';
             end if;
          end if;
          if int_addr = "0001" then
             int_regs_wb_m_i.dat <= g_ver_id;
             if int_regs_wb_m_o.we = '1' then
                int_regs_wb_m_i.err <= '1';
                int_regs_wb_m_i.ack <= '0';
             else
                int_regs_wb_m_i.ack <= '1';
                int_regs_wb_m_i.err <= '0';
             end if;
          end if;
        end if;
      end if;
    end if;
  end process;
  command_cycle_pause_width_o <= int_command_cycle_pause_width_o;
  command_cycle_pause_width_o_stb <= int_command_cycle_pause_width_o_stb;
  encoding_modes_o <= int_encoding_modes_o;
  bg1: for i in 0 to g_time_command_slots_size - 1 generate
    wb_m_i(0 + i) <= time_command_slots_wb_m_i(i);
    time_command_slots_wb_m_o(i)  <= wb_m_o(0 + i);
  end generate; -- for g_time_command_slots_size
  wb_m_i(4) <= int_regs_wb_m_i;
  int_regs_wb_m_o  <= wb_m_o(4);
  bg2: if g_special_time_command_slot_size > 0 generate
    wb_m_i(5) <= special_time_command_slot_wb_m_i;
    special_time_command_slot_wb_m_o  <= wb_m_o(5);
  end generate; -- g_special_time_command_slot_size
  bg3: if g_software_command_slot_size > 0 generate
    wb_m_i(6) <= software_command_slot_wb_m_i;
    software_command_slot_wb_m_o  <= wb_m_o(6);
  end generate; -- g_software_command_slot_size

end architecture;
