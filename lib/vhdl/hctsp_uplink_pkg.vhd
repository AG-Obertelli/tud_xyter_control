--- This file has been automatically generated
--- by the agwb (https://github.com/wzab/agwb).
--- Please don't edit it manually, unless you really have to do it
library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library general_cores;
use general_cores.wishbone_pkg.all;

library work;
use work.agwb_pkg.all;

package hctsp_uplink_pkg is

  constant C_hctsp_uplink_ADDR_BITS : integer := 10;

  constant c_hctsp_uplink_ver_id : std_logic_vector(31 downto 0) := x"5705417e";
  constant v_hctsp_uplink_ver_id : t_ver_id_variants(0 downto 0) := (0 => x"5705417e");
  constant c_uplinks_mask_0_size : integer := 1;
  constant c_uplinks_mask_1_size : integer := 1;
  constant c_strict_mode_0_size : integer := 1;
  constant c_strict_mode_1_size : integer := 1;
  constant c_receivers_size : integer := 40;
  constant c_ack_monitor_size : integer := 1;


  constant C_uplinks_mask_0_REG_ADDR: unsigned := x"00000002";
  subtype t_uplinks_mask_0 is std_logic_vector(31 downto 0);
  constant C_uplinks_mask_1_REG_ADDR: unsigned := x"00000003";
  subtype t_uplinks_mask_1 is std_logic_vector(7 downto 0);
  constant C_strict_mode_0_REG_ADDR: unsigned := x"00000004";
  subtype t_strict_mode_0 is std_logic_vector(31 downto 0);
  constant C_strict_mode_1_REG_ADDR: unsigned := x"00000005";
  subtype t_strict_mode_1 is std_logic_vector(7 downto 0);


end hctsp_uplink_pkg;

package body hctsp_uplink_pkg is

end hctsp_uplink_pkg;
