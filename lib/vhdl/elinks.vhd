--- This file has been automatically generated
--- by the agwb (https://github.com/wzab/agwb).
--- Please don't edit it manually, unless you really have to do it
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library general_cores;
use general_cores.wishbone_pkg.all;
library work;
use work.agwb_pkg.all;
use work.elinks_pkg.all;

entity elinks is
  generic (
    g_clk_ctrl_size : integer := c_clk_ctrl_size;
    g_clk_stat_size : integer := c_clk_stat_size;
    g_in_ctrl_size : integer := c_in_ctrl_size;
    g_in_stat_size : integer := c_in_stat_size;
    g_downlink_mask_size : integer := c_downlink_mask_size;

    g_ver_id : std_logic_vector(31 downto 0) := c_elinks_ver_id;
    g_registered : integer := 0
  );
  port (
    slave_i : in t_wishbone_slave_in;
    slave_o : out t_wishbone_slave_out;

    clk_ctrl_o : out  t_clk_ctrl;
    clk_stat_i : in  t_clk_stat;
    in_ctrl_o : out  ut_in_ctrl_array(g_in_ctrl_size - 1 downto 0);
    in_stat_i : in  ut_in_stat_array(g_in_stat_size - 1 downto 0);
    downlink_mask_o : out  t_downlink_mask;

    rst_n_i : in std_logic;
    clk_sys_i : in std_logic
    );

end elinks;

architecture gener of elinks is
  signal int_clk_ctrl_o : t_clk_ctrl;
  signal int_in_ctrl_o : ut_in_ctrl_array(g_in_ctrl_size - 1 downto 0);
  signal int_downlink_mask_o : t_downlink_mask := std_logic_vector(to_unsigned(0,5)); -- Hex value: 0x0


  -- Internal WB declaration
  signal int_regs_wb_m_o : t_wishbone_master_out;
  signal int_regs_wb_m_i : t_wishbone_master_in;
  signal int_addr : std_logic_vector(7-1 downto 0);
  signal wb_up_o : t_wishbone_slave_out_array(1-1 downto 0);
  signal wb_up_i : t_wishbone_slave_in_array(1-1 downto 0);
  signal wb_up_r_o : t_wishbone_slave_out_array(1-1 downto 0);
  signal wb_up_r_i : t_wishbone_slave_in_array(1-1 downto 0);
  signal wb_m_o : t_wishbone_master_out_array(1-1 downto 0);
  signal wb_m_i : t_wishbone_master_in_array(1-1 downto 0) := (others => c_WB_SLAVE_OUT_ERR);

  -- Constants
  constant c_address : t_wishbone_address_array(1-1  downto 0) := (0=>"00000000000000000000000000000000");
  constant c_mask : t_wishbone_address_array(1-1 downto 0) := (0=>"00000000000000000000000000000000");
begin
  
  assert g_clk_ctrl_size <= c_clk_ctrl_size report "g_clk_ctrl_size must be not greater than c_clk_ctrl_size=1" severity failure;
  assert g_clk_stat_size <= c_clk_stat_size report "g_clk_stat_size must be not greater than c_clk_stat_size=1" severity failure;
  assert g_in_ctrl_size <= c_in_ctrl_size report "g_in_ctrl_size must be not greater than c_in_ctrl_size=40" severity failure;
  assert g_in_stat_size <= c_in_stat_size report "g_in_stat_size must be not greater than c_in_stat_size=40" severity failure;
  assert g_downlink_mask_size <= c_downlink_mask_size report "g_downlink_mask_size must be not greater than c_downlink_mask_size=1" severity failure;

  wb_up_i(0) <= slave_i;
  slave_o <= wb_up_o(0);
  int_addr <= int_regs_wb_m_o.adr(7-1 downto 0);

-- Conditional adding of xwb_register   
  gr1: if g_registered = 2 generate
    grl1: for i in 0 to 1-1 generate
      xwb_register_1: entity general_cores.xwb_register
      generic map (
        g_WB_MODE => CLASSIC)
      port map (
        rst_n_i  => rst_n_i,
        clk_i    => clk_sys_i,
        slave_i  => wb_up_i(i),
        slave_o  => wb_up_o(i),
        master_i => wb_up_r_o(i),
        master_o => wb_up_r_i(i));
    end generate grl1;
  end generate gr1;

  gr2: if g_registered /= 2 generate
      wb_up_r_i <= wb_up_i;
      wb_up_o <= wb_up_r_o;
  end generate gr2;

-- Main crossbar
  xwb_crossbar_1: entity general_cores.xwb_crossbar
  generic map (
     g_num_masters => 1,
     g_num_slaves  => 1,
     g_registered  => (g_registered = 1),
     g_address     => c_address,
     g_mask        => c_mask
  )
  port map (
     clk_sys_i => clk_sys_i,
     rst_n_i   => rst_n_i,
     slave_i   => wb_up_r_i,
     slave_o   => wb_up_r_o,
     master_i  => wb_m_i,
     master_o  => wb_m_o,
     sdb_sel_o => open
  );

-- Process for register access
  process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if rst_n_i = '0' then
        -- Reset of the core
        int_regs_wb_m_i <= c_DUMMY_WB_MASTER_IN;
        int_downlink_mask_o <= std_logic_vector(to_unsigned(0,5)); -- Hex value: 0x0

      else
        -- Clearing of trigger bits (if there are any)

        -- Normal operation
        int_regs_wb_m_i.rty <= '0';
        int_regs_wb_m_i.ack <= '0';
        int_regs_wb_m_i.err <= '0';

        if (int_regs_wb_m_o.cyc = '1') and (int_regs_wb_m_o.stb = '1')
            and (int_regs_wb_m_i.err = '0') and (int_regs_wb_m_i.rty = '0')
            and (int_regs_wb_m_i.ack = '0') then
          int_regs_wb_m_i.err <= '1'; -- in case of missed address
          -- Access, now we handle consecutive registers
          -- Set the error state so it is output when none register is accessed
          int_regs_wb_m_i.dat <= x"A5A5A5A5";
          int_regs_wb_m_i.ack <= '0';
          int_regs_wb_m_i.err <= '1';
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_clk_ctrl_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(2 + i, 7)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(14 downto 0) <= to_slv(int_clk_ctrl_o);
              if int_regs_wb_m_o.we = '1' then
                int_clk_ctrl_o <= to_clk_ctrl(int_regs_wb_m_o.dat(14 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_clk_ctrl_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_clk_stat_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(3 + i, 7)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(1 downto 0) <= to_slv(clk_stat_i);
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_clk_stat_size
          for i in 0 to g_in_ctrl_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(4 + i, 7)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(7 downto 0) <= to_slv(int_in_ctrl_o( i ));
              if int_regs_wb_m_o.we = '1' then
                int_in_ctrl_o( i ) <= to_in_ctrl(int_regs_wb_m_o.dat(7 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_in_ctrl_size
          for i in 0 to g_in_stat_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(44 + i, 7)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(0 downto 0) <= to_slv(in_stat_i( i ));
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_in_stat_size
          
          -- That's a single register that may be present (size=1) or not (size=0).
          -- The "for" loop works like "if".
          -- That's why we do not index the register inside the loop.
          for i in 0 to g_downlink_mask_size - 1 loop
            if int_addr = std_logic_vector(to_unsigned(84 + i, 7)) then
              int_regs_wb_m_i.dat <= (others => '0');
              int_regs_wb_m_i.dat(4 downto 0) <= std_logic_vector(int_downlink_mask_o);
              if int_regs_wb_m_o.we = '1' then
                int_downlink_mask_o <= std_logic_vector(int_regs_wb_m_o.dat(4 downto 0));
              end if;
              int_regs_wb_m_i.ack <= '1';
              int_regs_wb_m_i.err <= '0';
            end if;
          end loop; -- g_downlink_mask_size


          if int_addr = "0000000" then
             int_regs_wb_m_i.dat <= x"fe5b694f";
             if int_regs_wb_m_o.we = '1' then
                int_regs_wb_m_i.err <= '1';
                int_regs_wb_m_i.ack <= '0';
             else
                int_regs_wb_m_i.ack <= '1';
                int_regs_wb_m_i.err <= '0';
             end if;
          end if;
          if int_addr = "0000001" then
             int_regs_wb_m_i.dat <= g_ver_id;
             if int_regs_wb_m_o.we = '1' then
                int_regs_wb_m_i.err <= '1';
                int_regs_wb_m_i.ack <= '0';
             else
                int_regs_wb_m_i.ack <= '1';
                int_regs_wb_m_i.err <= '0';
             end if;
          end if;
        end if;
      end if;
    end if;
  end process;
  clk_ctrl_o <= int_clk_ctrl_o;
  in_ctrl_o <= int_in_ctrl_o;
  downlink_mask_o <= int_downlink_mask_o;
  wb_m_i(0) <= int_regs_wb_m_i;
  int_regs_wb_m_o  <= wb_m_o(0);

end architecture;
