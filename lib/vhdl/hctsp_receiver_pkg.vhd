--- This file has been automatically generated
--- by the agwb (https://github.com/wzab/agwb).
--- Please don't edit it manually, unless you really have to do it
library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library general_cores;
use general_cores.wishbone_pkg.all;

library work;
use work.agwb_pkg.all;

package hctsp_receiver_pkg is

  constant C_hctsp_receiver_ADDR_BITS : integer := 3;

  constant c_hctsp_receiver_ver_id : std_logic_vector(31 downto 0) := x"a81463f3";
  constant v_hctsp_receiver_ver_id : t_ver_id_variants(0 downto 0) := (0 => x"a81463f3");
  constant c_clear_sequence_detectors_size : integer := 1;
  constant c_sequence_detectors_status_size : integer := 1;
  constant c_errors_size : integer := 1;
  constant c_errors_clear_mask_size : integer := 1;
  constant c_errors_alarm_mask_size : integer := 1;


  constant C_clear_sequence_detectors_REG_ADDR: unsigned := x"00000002";
  subtype t_clear_sequence_detectors is std_logic_vector(0 downto 0);
  constant C_sequence_detectors_status_REG_ADDR: unsigned := x"00000003";
  type t_sequence_detectors_status is record
    sos_detected:std_logic_vector(0 downto 0);
    sos_stable0:std_logic_vector(0 downto 0);
    sos_stable1:std_logic_vector(0 downto 0);
    k28_1_detected:std_logic_vector(0 downto 0);
    k28_1_stable1:std_logic_vector(0 downto 0);
    eos_detected:std_logic_vector(0 downto 0);
    eos_stable1:std_logic_vector(0 downto 0);
    k28_5_detected:std_logic_vector(0 downto 0);
    k28_5_stable1:std_logic_vector(0 downto 0);
  end record;
  
  function to_sequence_detectors_status(x : std_logic_vector) return t_sequence_detectors_status;
  function to_slv(x : t_sequence_detectors_status) return std_logic_vector;
  constant C_errors_REG_ADDR: unsigned := x"00000004";
  type t_errors is record
    bitslip_change:std_logic_vector(0 downto 0);
    code_8b10b:std_logic_vector(0 downto 0);
    disparity_8b10b:std_logic_vector(0 downto 0);
    decoding_8b10b:std_logic_vector(0 downto 0);
    frame:std_logic_vector(0 downto 0);
    heart_beat:std_logic_vector(0 downto 0);
  end record;
  
  function to_errors(x : std_logic_vector) return t_errors;
  function to_slv(x : t_errors) return std_logic_vector;
  constant C_errors_clear_mask_REG_ADDR: unsigned := x"00000005";
  type t_errors_clear_mask is record
    bitslip_change:std_logic_vector(0 downto 0);
    code_8b10b:std_logic_vector(0 downto 0);
    disparity_8b10b:std_logic_vector(0 downto 0);
    decoding_8b10b:std_logic_vector(0 downto 0);
    frame:std_logic_vector(0 downto 0);
    heart_beat:std_logic_vector(0 downto 0);
  end record;
  
  function to_errors_clear_mask(x : std_logic_vector) return t_errors_clear_mask;
  function to_slv(x : t_errors_clear_mask) return std_logic_vector;
  constant C_errors_alarm_mask_REG_ADDR: unsigned := x"00000006";
  type t_errors_alarm_mask is record
    bitslip_change:std_logic_vector(0 downto 0);
    code_8b10b:std_logic_vector(0 downto 0);
    disparity_8b10b:std_logic_vector(0 downto 0);
    decoding_8b10b:std_logic_vector(0 downto 0);
    frame:std_logic_vector(0 downto 0);
    heart_beat:std_logic_vector(0 downto 0);
  end record;
  
  function to_errors_alarm_mask(x : std_logic_vector) return t_errors_alarm_mask;
  function to_slv(x : t_errors_alarm_mask) return std_logic_vector;


end hctsp_receiver_pkg;

package body hctsp_receiver_pkg is
  function to_sequence_detectors_status(x : std_logic_vector) return t_sequence_detectors_status is
    variable res : t_sequence_detectors_status;
  begin
    res.sos_detected := std_logic_vector(x(0 downto 0));
    res.sos_stable0 := std_logic_vector(x(1 downto 1));
    res.sos_stable1 := std_logic_vector(x(2 downto 2));
    res.k28_1_detected := std_logic_vector(x(3 downto 3));
    res.k28_1_stable1 := std_logic_vector(x(4 downto 4));
    res.eos_detected := std_logic_vector(x(5 downto 5));
    res.eos_stable1 := std_logic_vector(x(6 downto 6));
    res.k28_5_detected := std_logic_vector(x(7 downto 7));
    res.k28_5_stable1 := std_logic_vector(x(8 downto 8));
    return res;
  end function;
  
  function to_slv(x : t_sequence_detectors_status) return std_logic_vector is
    variable res : std_logic_vector(8 downto 0);
  begin
    res(0 downto 0) := std_logic_vector(x.sos_detected);
    res(1 downto 1) := std_logic_vector(x.sos_stable0);
    res(2 downto 2) := std_logic_vector(x.sos_stable1);
    res(3 downto 3) := std_logic_vector(x.k28_1_detected);
    res(4 downto 4) := std_logic_vector(x.k28_1_stable1);
    res(5 downto 5) := std_logic_vector(x.eos_detected);
    res(6 downto 6) := std_logic_vector(x.eos_stable1);
    res(7 downto 7) := std_logic_vector(x.k28_5_detected);
    res(8 downto 8) := std_logic_vector(x.k28_5_stable1);
    return res;
  end function;
  
  function to_errors(x : std_logic_vector) return t_errors is
    variable res : t_errors;
  begin
    res.bitslip_change := std_logic_vector(x(0 downto 0));
    res.code_8b10b := std_logic_vector(x(1 downto 1));
    res.disparity_8b10b := std_logic_vector(x(2 downto 2));
    res.decoding_8b10b := std_logic_vector(x(3 downto 3));
    res.frame := std_logic_vector(x(4 downto 4));
    res.heart_beat := std_logic_vector(x(5 downto 5));
    return res;
  end function;
  
  function to_slv(x : t_errors) return std_logic_vector is
    variable res : std_logic_vector(5 downto 0);
  begin
    res(0 downto 0) := std_logic_vector(x.bitslip_change);
    res(1 downto 1) := std_logic_vector(x.code_8b10b);
    res(2 downto 2) := std_logic_vector(x.disparity_8b10b);
    res(3 downto 3) := std_logic_vector(x.decoding_8b10b);
    res(4 downto 4) := std_logic_vector(x.frame);
    res(5 downto 5) := std_logic_vector(x.heart_beat);
    return res;
  end function;
  
  function to_errors_clear_mask(x : std_logic_vector) return t_errors_clear_mask is
    variable res : t_errors_clear_mask;
  begin
    res.bitslip_change := std_logic_vector(x(0 downto 0));
    res.code_8b10b := std_logic_vector(x(1 downto 1));
    res.disparity_8b10b := std_logic_vector(x(2 downto 2));
    res.decoding_8b10b := std_logic_vector(x(3 downto 3));
    res.frame := std_logic_vector(x(4 downto 4));
    res.heart_beat := std_logic_vector(x(5 downto 5));
    return res;
  end function;
  
  function to_slv(x : t_errors_clear_mask) return std_logic_vector is
    variable res : std_logic_vector(5 downto 0);
  begin
    res(0 downto 0) := std_logic_vector(x.bitslip_change);
    res(1 downto 1) := std_logic_vector(x.code_8b10b);
    res(2 downto 2) := std_logic_vector(x.disparity_8b10b);
    res(3 downto 3) := std_logic_vector(x.decoding_8b10b);
    res(4 downto 4) := std_logic_vector(x.frame);
    res(5 downto 5) := std_logic_vector(x.heart_beat);
    return res;
  end function;
  
  function to_errors_alarm_mask(x : std_logic_vector) return t_errors_alarm_mask is
    variable res : t_errors_alarm_mask;
  begin
    res.bitslip_change := std_logic_vector(x(0 downto 0));
    res.code_8b10b := std_logic_vector(x(1 downto 1));
    res.disparity_8b10b := std_logic_vector(x(2 downto 2));
    res.decoding_8b10b := std_logic_vector(x(3 downto 3));
    res.frame := std_logic_vector(x(4 downto 4));
    res.heart_beat := std_logic_vector(x(5 downto 5));
    return res;
  end function;
  
  function to_slv(x : t_errors_alarm_mask) return std_logic_vector is
    variable res : std_logic_vector(5 downto 0);
  begin
    res(0 downto 0) := std_logic_vector(x.bitslip_change);
    res(1 downto 1) := std_logic_vector(x.code_8b10b);
    res(2 downto 2) := std_logic_vector(x.disparity_8b10b);
    res(3 downto 3) := std_logic_vector(x.decoding_8b10b);
    res(4 downto 4) := std_logic_vector(x.frame);
    res(5 downto 5) := std_logic_vector(x.heart_beat);
    return res;
  end function;
  

end hctsp_receiver_pkg;
