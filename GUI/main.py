from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5 import uic
import sys, os, time, struct, code, threading, argparse, numpy as np
sys.path.append("../lib")
sys.path.append("../")
import pprint, time, pyautogui
import Environment as Env
import os, uhal, agwb
import subprocess
import threading
from PFAD_lib import init_cmd, initialise, set_trim, set_clk_phase_data_phase,\
                     circuit_monitoring_logging, check_trim, fast_ENC,\
                     read_registers, channel_test,ENC_scurves_scan, trim_calibration,\
                     PFAD_update_configuration
from DAQ_lib import DAQ_start
                     
class PFAD_GUI(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.ui = uic.loadUi('PFAD_gui_v1.ui',self)
        
        # List of ASICs to work with in the entire GUI
        
        self.smxes = []
        self.smx_tester = []
        self.smxes_active = []
        self.list_ASICs = 'default'
        self.XYTER_registers_config = 'default'
        self.list_trim_calibration = 'default'
        self.refresh_command()
        
        # Menu bar:    
        self.actionSave_Image.triggered.connect(lambda: self.save_image_command())
        self.actionQuit.triggered.connect(lambda: self.exit_command())
        self.actionControlhub.triggered.connect(lambda: self.controlhub_command())
        self.actionVivado.triggered.connect(lambda: self.vivado_command())
        self.actionEnvironment.triggered.connect(lambda: self.Environment_command())
        self.actionRefresh.triggered.connect(lambda: self.refresh_command())
        self.actionXYTER_Manual.triggered.connect(lambda: self.Manual_command())
        self.actionHelp.triggered.connect(lambda: self.GUI_Manual_command())
        
        
        self.pushButton_Cancel.clicked.connect(lambda: self.cancel_button())
        #self.pushButton_Cancel.setEnabled(False)
        self.pushButton_Main_command_RUN.clicked.connect(lambda: self.universal_command())
        
        self.commandLinkButton_LOGO.clicked.connect(lambda: self.refresh_command())
        self.commandLinkButton_LOGO_2.clicked.connect(lambda: self.refresh_command())
        self.commandLinkButton_LOGO_3.clicked.connect(lambda: self.refresh_command())
        self.commandLinkButton_LOGO_4.clicked.connect(lambda: self.refresh_command())
        
        # Main tab:
        self.commandLinkButton_Main_reset.clicked.connect(lambda: self.start_setup_command())
        self.commandLinkButton_Main_initialise.clicked.connect(lambda: self.initialise_command())
        self.commandLinkButton_Main_set_trim.clicked.connect(lambda: self.set_trim_command())
        self.commandLinkButton_Main_Start.clicked.connect(lambda: self.init_cmd_command())
        
        self.pushButton_Main_phases_scan.clicked.connect(lambda: self.phases_scan_command(0))
        self.commandLinkButton_Main_phases_scan_1.clicked.connect(lambda: self.phases_scan_command(1))
        self.commandLinkButton_Main_phases_scan_2.clicked.connect(lambda: self.phases_scan_command(2))
        self.commandLinkButton_Main_phases_scan_3.clicked.connect(lambda: self.phases_scan_command(3))
        
        self.commandLinkButton_Disable_Channels.clicked.connect(lambda: self.disable_channels())
        #LISTS:
        self.radioButton_all_active.setChecked(True)
        self.radioButton_all_active.setStatusTip("Show only active Physical ULs")
        self.radioButton_all_active.toggled.connect(lambda: self.toggle_all_active())
        # List of ASICs
        self.comboBox_Main_list_ASICs.activated[str].connect(self.chosen_list_ASICs)
        self.pushButton_Main_list_ASICs_OK.clicked.connect(lambda: self.save_list_ASICs())
        self.toolButton_Main_clear_table.clicked.connect(lambda: self.clear_Main_table())
        self.pushButton_Main_Update_Active.clicked.connect(lambda: self.update_active_smxes())
        self.commandLinkButton_Check_all.clicked.connect(lambda: self.Check_all_ASICS(1))
        self.commandLinkButton_unCheck_all.clicked.connect(lambda: self.Check_all_ASICS(0))
        # List of trim calibration
        self.comboBox_Main_trim_calibration.activated[str].connect(self.chosen_list_trim_calibration)
        self.pushButton_Main_trim_calibration_run.clicked.connect(lambda: self.trim_calibration_command(0))
        self.commandLinkButton_Main_trim_calibration_existing.clicked.connect(lambda: self.trim_calibration_command(1))
        self.commandLinkButton_Main_trim_calibration_settings.clicked.connect(lambda: self.trim_calibration_command(2))
        
        #XYTER register config
        self.comboBox_Config_list_settings.activated[str].connect(self.chosen_list_register_config)
        self.pushButton_Config_list_settings.clicked.connect(lambda: self.save_list_register_config())
        self.toolButton_Config_clear_table.clicked.connect(lambda: self.clear_Config_table())
        self.commandLinkButton_Config_info.clicked.connect(lambda: self.Config_info_display())

        
        
        #TOOLS:
        self.pushButton_Main_circuit.clicked.connect(lambda: self.circuit_monitoring_command(0))
        self.commandLinkButton_Main_circuit_1.clicked.connect(lambda: self.circuit_monitoring_command(1))
        self.commandLinkButton_Main_circuit_2.clicked.connect(lambda: self.circuit_monitoring_command(2))
        self.commandLinkButton_Main_circuit_3.clicked.connect(lambda: self.circuit_monitoring_command(3))
        
        self.pushButton_Main_channel_test.clicked.connect(lambda: self.channel_test_command(0))
        self.commandLinkButton_Main_channel_test_1.clicked.connect(lambda: self.channel_test_command(1))
        self.commandLinkButton_Main_channel_test_2.clicked.connect(lambda: self.channel_test_command(2))
        self.commandLinkButton_Main_channel_test_3.clicked.connect(lambda: self.channel_test_command(3))
        
        self.pushButton_Main_check_trim.clicked.connect(lambda: self.check_trim_command(0))
        self.commandLinkButton_Main_check_trim_1.clicked.connect(lambda: self.check_trim_command(1))
        self.commandLinkButton_Main_check_trim_2.clicked.connect(lambda: self.check_trim_command(2))
        self.commandLinkButton_Main_check_trim_3.clicked.connect(lambda: self.check_trim_command(3))
        
        self.pushButton_Main_fast_ENC.clicked.connect(lambda: self.fast_ENC_command(0))
        self.commandLinkButton_Main_fast_ENC_1.clicked.connect(lambda: self.fast_ENC_command(1))
        self.commandLinkButton_Main_fast_ENC_2.clicked.connect(lambda: self.fast_ENC_command(2))
        self.commandLinkButton_Main_fast_ENC_3.clicked.connect(lambda: self.fast_ENC_command(3))
        
        self.pushButton_Main_scurve.clicked.connect(lambda: self.scurve_command(0))
        self.commandLinkButton_Main_scurve_1.clicked.connect(lambda: self.scurve_command(1))
        self.commandLinkButton_Main_scurve_2.clicked.connect(lambda: self.scurve_command(2))
        self.commandLinkButton_Main_scurve_3.clicked.connect(lambda: self.scurve_command(3))
        #self.pushButton_Main_scurve.setEnabled(False)
        
        #DAQ
        self.tableWidget_DAQ.setColumnWidth(3, 300)
        self.commandLinkButton_DAQ_start.clicked.connect(lambda: self.DAQ_start_command())
        self.commandLinkButton_DAQ_stop.clicked.connect(lambda: self.DAQ_stop_command())
        self.commandLinkButton_DAQ_data_files.clicked.connect(lambda: self.DAQ_commands(0))
        self.pushButton_DAQ_update_rate.clicked.connect(lambda: self.DAQ_commands(1))
        self.pushButton_DAQ_mark_rate.clicked.connect(lambda: self.DAQ_commands(2))
       
    def universal_command(self):
        command = self.lineEdit_Main_command.text()
        if (command == "count_test"):
            self.thread = ThreadClass(parent = None, command = "testing")
            self.thread.signal_str.connect(self.Show_Message_box)
            self.thread.signal_str1.connect(self.Cursor_type)
            self.thread.start() 
        else:
            os.system("cd {}run && {} ".format(Env.base_path, command))
        
    def exit_command(self):
        sys.exit()
        
    def save_image_command(self):
        pyautogui.screenshot('test.png')
        
    def vivado_command(self):
        self.thread = ThreadClass(parent = None, command = "vivado")
        self.thread.signal_str.connect(self.Show_Message_box)
        self.thread.signal_str1.connect(self.Cursor_type)
        self.thread.start() 
        
    def cancel_button(self):
        self.thread.stop()   
        self.Cursor_type('default') 
               
    def GUI_Manual_command(self):
        os.system('xdg-open '+os.getcwd()+'/../GUI/PFAD_GUI_v1_Manual.pdf &')
        
    def Manual_command(self):
        os.system('xdg-open '+os.getcwd()+'/../lib/SMX2.2\ manual\ v4.pdf &')
        
    def controlhub_command(self):
        os.system('controlhub_start')
        
    def Environment_command(self):
        os.system('gedit '+os.getcwd()+'/../Environment.py &')
        
    def refresh_command(self):
        self.Show_Message_box('Welcome to PFAD!')
        self.Show_DAQ_Message_box('')
        self.Update_chosen_lists()              
        self.update_list_of_ASICs() 
        self.update_list_of_trim_calibration() 
        self.update_list_register_config() 
        self.Cursor_type('default')

    def Update_chosen_lists(self):
        self.list_ASICs, self.XYTER_registers_config, self.list_trim_calibration, ASIC_ul, ASIC_name, ASIC_position, registers_130, ana_chan_63, ana_chan_65, ana_chan_67 	= PFAD_update_configuration()
        
    def start_setup_command(self):
        self.smxes, self.smx_tester = init_cmd()
        for smx in self.smxes:
            initialise(smx)
            set_trim(smx)
        self.smxes_active = self.smxes
        return self.smxes, self.smx_tester
        
    def init_cmd_command(self):
        self.smxes, self.smx_tester = init_cmd()
        ul_smxes = []    
        for smx in self.smxes:
            ul_smxes.append(smx.uplinks[0])
        print(ul_smxes)
        self.smxes_active = self.smxes
        #print(self.smx_tester)
        return self.smxes, self.smx_tester
                  
    def initialise_command(self):
        for smx in self.smxes_active:
            initialise(smx)
            
    def set_trim_command(self):
        for smx in self.smxes_active:
            set_trim(smx)
   
    def phases_scan_command(self,command):
        if(command == 0): 
            self.thread = ThreadClass(parent = None, command = "phases_scan")
            self.thread.signal_str.connect(self.Show_Message_box)
            self.thread.signal_bool.connect(self.pushButton_Main_phases_scan.setEnabled)
            self.thread.signal_str1.connect(self.Cursor_type)
            self.thread.start() 
        if(command == 1): 
            self.Show_Message_box( "The phases scan needs to be performed before starting.\nIt takes around 10s per chip.\n Must be done once after the configuration is changed,\n then it remains valid each time the same configuration runs.\n")
        if(command == 2): os.system('gedit '+Env.phases_path+'clock_phases.txt &')
        if(command == 3): os.system('gedit '+Env.phases_path+'data_phases.txt &')
        
    def disable_channels(self):
        self.list_ASICs, self.XYTER_registers_config, self.list_trim_calibration, ASIC_ul, ASIC_name, ASIC_position, registers_130, ana_chan_63, ana_chan_65, ana_chan_67 	= PFAD_update_configuration()
        os.system('xdg-open '+ Env.config_and_calibration_path+'register_config/{}/disable_channels &'.format(self.XYTER_registers_config))
    #LISTS:
    
    def trim_calibration_command(self, command):
        name = self.lineEdit_Main_trim_calibration_name.text()
        if (command == 0 and not name == ''): 
            self.Show_Message_box('Trim calibration running...')            
            if(not os.path.exists(Env.config_and_calibration_path+'trim_calibration_files/{}'.format(name))):
                os.mkdir(Env.config_and_calibration_path+'trim_calibration_files/{}'.format(name))
            os.system('sed -i s/list\_trim\_calibration.*/list\_trim\_calibration\ {}/g {}'.format(name, Env.config_and_calibration_path + 'Chosen_lists.txt'))
            for smx in self.smxes_active:
                trim_calibration(smx)
            self.chosen_list_trim_calibration(name)
        if (command == 0 and name == ''): 
            print ("Errors: please give a valid name!")        
        if (command == 1): self.Show_Message_box('Trim calibration needs to be done after "Initialise".\nBe sure to choose the right settings for the trim calibration ("Settings" button)\n')
        if (command == 2): 
            self.Show_Message_box('Trim calibration settings.')
            os.system('gedit '+Env.config_and_calibration_path+'trim_calibration_files/trim_calibration_settings.txt &')
        
      
    def toggle_all_active(self):
        if (self.radioButton_all_active.isChecked()): self.radioButton_all_active.setStatusTip("Show only active Physical ULs")
        else: self.radioButton_all_active.setStatusTip("Show all Physical ULs")
    
    def update_list_of_ASICs(self):
        self.comboBox_Main_list_ASICs.clear()
        dir_list_ASICs = self.dir_scan(Env.config_and_calibration_path + 'list_of_ASICs')
        #print('List of ASICs available: ', dir_list_ASICs)
        for i_list_ASICs in range(len(dir_list_ASICs)):
            self.comboBox_Main_list_ASICs.addItem(dir_list_ASICs[i_list_ASICs])
        self.comboBox_Main_list_ASICs.setCurrentText(self.list_ASICs)
        return dir_list_ASICs
            
    def update_list_of_trim_calibration(self):
        self.comboBox_Main_trim_calibration.clear()
        dir_list_trim = self.dir_scan(Env.config_and_calibration_path + 'trim_calibration_files')
        #print('List of trim calibrations available: ', dir_list_trim)
        for i_list in range(len(dir_list_trim)):
            self.comboBox_Main_trim_calibration.addItem(dir_list_trim[i_list])
        self.comboBox_Main_trim_calibration.setCurrentText(self.list_trim_calibration)
        return dir_list_trim
        
    def update_list_register_config(self):
        self.comboBox_Config_list_settings.clear()
        dir_list_config = self.dir_scan(Env.config_and_calibration_path + 'register_config')
        #print('List of register config available: ', list_config)
        for i_list in range(len(dir_list_config)):
            self.comboBox_Config_list_settings.addItem(dir_list_config[i_list])
        self.comboBox_Config_list_settings.setCurrentText(self.XYTER_registers_config)
        return dir_list_config
        
    def save_list_ASICs(self):
        name = self.lineEdit_Main_list_ASICSs.text()
        if (name == ''): print ("Errors: please give a valid name!")    
        else: 
            if(not os.path.exists(Env.config_and_calibration_path+'list_of_ASICs/{}'.format(name))):
                os.mkdir(Env.config_and_calibration_path+'list_of_ASICs/{}'.format(name))
            file_name = Env.config_and_calibration_path+'list_of_ASICs/{}/list_of_asics.txt'.format(name)
            fileout = open(file_name,"w")
            for row in range(41):
                item = self.tableWidget_Main_list_of_ASICs.item(row,1)
                if (item): 
                    ul = self.tableWidget_Main_list_of_ASICs.item(row,1).text() #Physical UL
                    name = self.tableWidget_Main_list_of_ASICs.item(row,2).text() #Name
                    pos = self.tableWidget_Main_list_of_ASICs.item(row,3).text() #Position
                    fileout.write("{}    {}    {}\n".format(ul,name,pos))
                else: break
            self.chosen_list_ASICs(name)
            
            
    def save_list_register_config(self):
        name = self.lineEdit_Config_list_settings.text()
        if (name == ''): print ("Errors: please give a valid name!")    
        else: 
            if(not os.path.exists(Env.config_and_calibration_path+'register_config/{}'.format(name))):
                os.mkdir(Env.config_and_calibration_path+'register_config/{}'.format(name))
            if(not os.path.exists(Env.config_and_calibration_path+'register_config/{}/disable_channels'.format(name))):
                os.mkdir(Env.config_and_calibration_path+'register_config/{}/disable_channels'.format(name))
            file_name = Env.config_and_calibration_path+'register_config/{}/list_register_config.txt'.format(name)
            fileout = open(file_name,"w")
            for row in range(41):
                item = self.tableWidget_Config_settings.item(row,1)
                if (item): 
                    ul = self.tableWidget_Config_settings.item(row,1).text() #Physical UL
                    fileout.write("{}    ".format(ul))
                    for reg in range(19+3):
                        reg_item = self.tableWidget_Config_settings.item(row,reg+3).text() #registers
                        fileout.write("{}    ".format(reg_item))
                    fileout.write("\n")
                else: break    
            if (row==0): #check 'all' row
                for ul in range(41):
                    fileout.write("{}    ".format(ul))
                    for reg in range(19+3):
                        reg_item = self.tableWidget_Config_settings.item(42,reg+3).text() #registers
                        fileout.write("{}    ".format(reg_item))
                    fileout.write("\n")
            self.chosen_list_register_config(name)
        
    def clear_Main_table(self):
        self.tableWidget_Main_list_of_ASICs.clearContents()
        self.Show_Message_box('Table cleared')
        
    def clear_Config_table(self):
        self.tableWidget_Config_settings.clearContents()
        self.Show_Message_box('Table cleared')
        
    def update_active_smxes(self):
        ul_smxes = []    
        for smx in self.smxes_active:
            ul_smxes.append(smx.uplinks[0])
        print(ul_smxes)
        
        self.smxes_active = []
        column = 4
        i = 0
        for smx in self.smxes:
            item = self.tableWidget_Main_list_of_ASICs.item(i,column)
            if (item.checkState() == QtCore.Qt.Checked):
                self.smxes_active.append(smx)
            i+=1
            
        ul_smxes = []    
        for smx in self.smxes_active:
            ul_smxes.append(smx.uplinks[0])
        print(ul_smxes)
        return self.smxes_active
    
    def Check_all_ASICS(self, command=1):
        column = 4
        i = 0
        for i in range(41):
            item0 = self.tableWidget_Main_list_of_ASICs.item(i,1)
            if (item0): 
                item = self.tableWidget_Main_list_of_ASICs.item(i,column)
                if (command == 1):
                    item.setCheckState(QtCore.Qt.Checked)
                else:
                    item.setCheckState(QtCore.Qt.Unchecked)
                
                
            
         
            
        
    def chosen_list_ASICs(self,text):
        self.Show_Message_box('Chosen list of ASICs: '+text)
        if (text == ''): text = 'default'
        os.system('sed -i s/list\_ASICs.*/list\_ASICs\ {}/g {}'.format(text, Env.config_and_calibration_path + 'Chosen_lists.txt'))
        #self.list_ASICs = text
        self.list_ASICs, self.XYTER_registers_config, self.list_trim_calibration, ASIC_ul, ASIC_name, ASIC_position, registers_130, ana_chan_63, ana_chan_65, ana_chan_67 	= PFAD_update_configuration()
        i = 0 #row
        j = 0 #column
        self.tableWidget_Main_list_of_ASICs.clearContents()
        self.tableWidget_Main_list_of_ASICs.setColumnWidth(4, 20)
        if (self.radioButton_all_active.isChecked()):
            i = 0
            for smx in self.smxes:
                ASIC = ['{}'.format(i), '{}'.format(smx.uplinks[0]), ASIC_name[smx.uplinks[0]], ASIC_position[smx.uplinks[0]] , ' ' ]
                for j in range(len(ASIC)):
                    item = QtWidgets.QTableWidgetItem(ASIC[j])
                    self.tableWidget_Main_list_of_ASICs.setItem(i,j,item)
                #checkbox    
                item = QtWidgets.QTableWidgetItem(' ')
                item.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
                item.setCheckState(QtCore.Qt.Checked)
                self.tableWidget_Main_list_of_ASICs.setItem(i,len(ASIC)-1,item)
                
                i+=1
                
        else:
            i = 0
            for ul in range(max(ASIC_ul)+1):
                ASIC = ['{}'.format(i), '{}'.format(ul), ASIC_name[ul], ASIC_position[ul],'   ' ]
                for j in range(len(ASIC)):
                    item = QtWidgets.QTableWidgetItem(ASIC[j])
                    self.tableWidget_Main_list_of_ASICs.setItem(i,j,item)
                #checkbox    
                item = QtWidgets.QTableWidgetItem(' ')
                item.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
                item.setCheckState(QtCore.Qt.Checked)
                self.tableWidget_Main_list_of_ASICs.setItem(i,len(ASIC)-1,item)
                i+=1
                                 
    def chosen_list_trim_calibration(self,text):
        self.Show_Message_box('Chosen list of trim calibration: '+text)
        if (text == ''): text = 'default'
        os.system('sed -i s/list\_trim\_calibration.*/list\_trim\_calibration\ {}/g {}'.format(text, Env.config_and_calibration_path + 'Chosen_lists.txt'))
        self.list_trim_calibration = text
         
    def chosen_list_register_config(self,text):
        self.Show_Message_box('Chosen list of XYTER register config: '+text)
        if (text == ''): text = 'default'
        os.system('sed -i s/XYTER\_registers\_config.*/XYTER\_registers\_config\ {}/g {}'.format(text, Env.config_and_calibration_path + 'Chosen_lists.txt'))
        
        #self.XYTER_registers_config = text
        #self.Update_chosen_lists()
        self.list_ASICs, self.XYTER_registers_config, self.list_trim_calibration, ASIC_ul, ASIC_name, ASIC_position, registers_130, ana_chan_63, ana_chan_65, ana_chan_67 	= PFAD_update_configuration()
        
        i = 0 #row
        j = 0 #column
        self.tableWidget_Config_settings.clearContents()
        if (self.radioButton_all_active.isChecked()):
            for smx in self.smxes:
                ASIC = ['{}'.format(i), '{}'.format(smx.uplinks[0]), ASIC_name[smx.uplinks[0]]]
                for ival, val in enumerate(registers_130):
                    ASIC.append('{}'.format(val[smx.uplinks[0]]))
                ASIC.append('{}'.format(ana_chan_63[smx.uplinks[0]]))
                ASIC.append('{}'.format(ana_chan_65[smx.uplinks[0]]))
                ASIC.append('{}'.format(ana_chan_67[smx.uplinks[0]]))
                for j in range(len(ASIC)):
                    item = QtWidgets.QTableWidgetItem(ASIC[j])
                    self.tableWidget_Config_settings.setItem(i,j,item)
                i+=1
        else:
            for ul in range(33):
                ASIC = ['{}'.format(i), '{}'.format(ul), ASIC_name[ul]]
                for ival, val in enumerate(registers_130):
                    ASIC.append('{}'.format(val[ul]))
                ASIC.append('{}'.format(ana_chan_63[ul]))
                ASIC.append('{}'.format(ana_chan_65[ul]))
                ASIC.append('{}'.format(ana_chan_67[ul]))
                for j in range(len(ASIC)):
                    item = QtWidgets.QTableWidgetItem(ASIC[j])
                    self.tableWidget_Config_settings.setItem(i,j,item)
                i+=1
                
                
    def Config_info_display(self):
        self.Show_Message_box('Please consult the XYTER Manual\nfor setting the register configuration.\nSettings -> XYTERManual')
                   
    # TOOLS:
    def circuit_monitoring_command(self,command):
        if (command==0): 
            for smx in self.smxes_active:
                circuit_monitoring_logging(smx, Env.log_path+'Circuit_monitoring/logging.txt')
            self.Show_Message_box('Circuit monitoring finished.\nSee the saved files with the "Go to files" button') 
        if (command==1): os.system('gedit '+Env.log_path+'Circuit_monitoring/settings.txt &')
        #if (command==2): os.system('xdg-open '+Env.log_path+'Circuit_monitoring/Files &')
        if (command==2): os.system('gedit '+Env.log_path+'Circuit_monitoring/logging.txt &')
        if (command==3): self.Show_Message_box('Circuit monitoring provides measurements of the VDDM current,\nCSA_bias and chip temperature (the temperature is not calibrated).\n"Go to files" to see the logging of Circuit monitoring.')
    def channel_test_command(self,command):
        if (command==0):
            for smx in self.smxes_active:
                channel_test(smx)   
            self.Show_Message_box('Channel test finished.\nSee the saved files with the "Go to files" button')  
        if (command==1): os.system('gedit '+Env.log_path+'Channel_test/settings.txt &')
        if (command==2): os.system('xdg-open '+Env.log_path+'Channel_test/Files &')
        if (command==3): self.Show_Message_box('The channel test gives a GOOD/BAD list of the channels based on the response of the first 4 distriminators\nwhen pulses are injected with relatively large amplitude (such that they all should be able to fire).\n Plots with values from 0 to 4 vs channel are created and saved. "Go to files".\nPlease make sure you provide the proper settings before running.')
    def check_trim_command(self,command):
        if (command==0):
            self.thread = ThreadClass(parent = None, command = "check_trim", smxes = self.smxes_active)
            self.thread.signal_str.connect(self.Show_Message_box)
            self.thread.signal_str1.connect(self.Cursor_type)
            self.thread.start() 
            #for smx in self.smxes_active:
            #    check_trim(smx)
            self.Show_Message_box('Check trim finished.\nSee the saved files with the "Go to files" button') 
        if (command==1): os.system('gedit '+Env.log_path+'Check_trim/settings.txt &')
        if (command==2): os.system('xdg-open '+Env.log_path+'Check_trim/Files &')
        if (command==3): self.Show_Message_box('Check trim is injecting a number of pulses of gradual amplitude and the distriminators should respond liniarily.\nA matrix will be shown in terminal which should have the shape of  (0 \ 10) - diagonal\nif the trim calibration is good.\nPlease make sure you provide the proper settings before running.')
    def fast_ENC_command(self,command):
        if (command==0):
            self.thread = ThreadClass(parent = None, command = "fast_ENC", smxes = self.smxes_active)
            self.thread.signal_str.connect(self.Show_Message_box)
            self.thread.signal_str1.connect(self.Cursor_type)
            self.thread.start() 
            #self.Cursor_type('wait')
            #for smx in self.smxes_active:
            #    fast_ENC(smx)  
            #self.Cursor_type('default')
            self.Show_Message_box('Fast ENC finished.\nSee the saved files with the "Go to files" button')
        if (command==1): os.system('gedit '+Env.log_path+'Fast_ENC/settings.txt &')
        if (command==2): os.system('xdg-open '+Env.log_path+'Fast_ENC/Files &')
        if (command==3): self.Show_Message_box('The Fast ENC noise analysis is an amplitude scan performed on each channel based on the first 4 distriminators.\n In the terminal a list (x1,y1) (x2,y2) (x3,y3) (x4,y4) enc: value is given.\n x = the discriminator position, y = ENC level of the discriminator, finaly the last value is the ENC value.\nIf the x values do not have a linear increase, it is sign that the trim calibration is wrong/not applied.\nAplot with ENC vs channel is created and saved "Go to files".\nPlease make sure you provide the proper settings before running.')
    def scurve_command(self,command):
        if (command==0):
            for smx in self.smxes_active:
                 ENC_scurves_scan(smx)
        if (command==1): os.system('gedit '+Env.log_path+'S-curve_analysis/settings.txt &')
        if (command==2): os.system('xdg-open '+Env.log_path+'S-curve_analysis/Files &')
        if (command==3): self.Show_Message_box('The S-curve analysis is a finer amplitude scan performed on all distriminators\nand all channels with higher statistics than the Fast ENC.\nIt takes longer time, but provides more reliable values for the ENC for the system characterisation.\nA rootfile is created which should be later analyzed.\n"Go to files" to see the rootfiles.\nPlease make sure you provide the proper settings before running.')
    
    
    #DAQ!!!
    def DAQ_start_command(self):
        info_name = self.lineEdit_DAQ_file_name.text()
        duration = self.spinBox_DAQ_time.value()
        
        if (duration<=0):duration = -1
        print("NAME:   ", info_name)
        print("DURATION:  ",duration)
        DAQ_start(duration,info_name)
        """
        self.DAQthread = DAQThreadClass(parent = None, DAQinfo_name = info_name, DAQduration = duration, DAQsmxes = self.smxes, DAQsmx_tester = self.smx_tester)
        self.DAQthread.signal_str.connect(self.Show_Message_box)
        self.DAQthread.signal_str1.connect(self.Show_DAQ_Message_box)
        self.DAQthread.start() 
        """
    def DAQ_stop_command(self):
        self.Show_Message_box('Please press "CTRL + C" ')
        #self.DAQthread.stop() 
    
    def DAQ_commands(self,command):
        if (command == 0): #go to files
            os.system('xdg-open '+Env.DAQ_path+ ' &')
        if (command == 1): #update rates
            print("UPDATE RATES")
            self.list_ASICs, self.XYTER_registers_config, self.list_trim_calibration, ASIC_ul, ASIC_name, ASIC_position, registers_130, ana_chan_63, ana_chan_65, ana_chan_67 	= PFAD_update_configuration()
            rate=[0 for i in range(50)]
            file_name = Env.log_path+'DAQ/DAQ_rates_tmp.txt'
            i = 0 #row
            j = 0 #column
            self.tableWidget_DAQ.clearContents()
            print(file_name)
            print(os.path.exists(file_name))
            if (os.path.exists(file_name)):
                with open(file_name) as f:
                    for line in f:
                        row = line.split()
                        if (row[0] == "#"): continue
                        else:
                            if (row[0] == "all"): ASIC = [' ',' ',' ', row[1]]
                            else: ASIC = ['{}'.format(int(row[0])), ASIC_name[int(row[0])], ASIC_position[int(row[0])], row[1]]
                            for j in range(len(ASIC)):
                                item = QtWidgets.QTableWidgetItem(ASIC[j])
                                self.tableWidget_DAQ.setItem(i,j,item)
                            i+=1
        
            
        if (command == 2): #mark rates
            threshold = self.spinBox_DAQ_rate.value()
            column = 3
            for i in range (1,42):
                item = self.tableWidget_DAQ.item(i,0)
                if (item):
                    item_rate = self.tableWidget_DAQ.item(i,column)
                    item_rate.setBackground(QtGui.QColor(255,255,255))
                    if (int(item_rate.text()) > threshold):
                        item_rate.setBackground(QtGui.QColor(254, 116, 116))
                    if (int(item_rate.text()) == 0):
                        item_rate.setBackground(QtGui.QColor(200,249,200))
                        
    
    
    def Show_Message_box(self,text):
        self.label_Main_Message_box.setText(text)
        self.label_Main_Message_box.adjustSize()
        self.label_Config_Message_box.setText(text)
        self.label_Config_Message_box.adjustSize()
        self.label_Tools_Message_box.setText(text)
        self.label_Tools_Message_box.adjustSize()
        self.label_DAQ_Message_box.setText(text)
        self.label_DAQ_Message_box.adjustSize()
        
    def Show_DAQ_Message_box(self,text):
        self.label_logging.setText(text)
        self.label_logging.adjustSize()
        
        
    def Cursor_type(self,text):
        if (text == "default"): self.tabWidget_main.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        if (text == "wait"):self.tabWidget_main.setCursor(QtGui.QCursor(QtCore.Qt.WaitCursor))
        
    def dir_scan(self, rootdir):
        dir_list = []
        for it in os.scandir(rootdir):
            if it.is_dir():
                tmp = it.path
                tmp = tmp.replace(rootdir+'/','')
                dir_list.append(tmp)
        return dir_list
        
class ThreadClass(QtCore.QThread):
    signal_str = QtCore.pyqtSignal(str)
    signal_str1 = QtCore.pyqtSignal(str)
    signal_int = QtCore.pyqtSignal(int)  
    signal_bool = QtCore.pyqtSignal(bool)
    
    def __init__(self, parent = None, command="",smxes = []):
        super(ThreadClass, self).__init__(parent)
        self.command = command
        self.smxes = smxes
        self.is_running = True
                        
    def run(self):
        if (self.command == "phases_scan"):
            self.signal_bool.emit(False)
            self.signal_str1.emit("wait")
            self.signal_str.emit('Started phases scan ...')  
            set_clk_phase_data_phase()
            self.signal_str.emit('Finished phases scan ...')  
            self.signal_bool.emit(True)
            self.signal_str1.emit("default")
            
        if (self.command == "vivado"):
            self.signal_str1.emit("wait")
            self.signal_str.emit('Started VIVADO ...')  
            os.system('/home/ststest/xilinx/Vivado/Vivado/2020.2/bin/vivado -mode batch -source ../FW/Vivado_set.tcl')
            self.signal_str.emit('Finished VIVADO ...')  
            self.signal_str1.emit("default")
            
        if (self.command == "fast_ENC"):
            self.signal_str1.emit("wait")
            self.signal_str.emit('Started fast ENC ...')  
            for smx in self.smxes:
                fast_ENC(smx)  
            self.signal_str.emit('Finished fast ENC ...')  
            self.signal_str1.emit("default")
            
        if (self.command == "check_trim"):
            self.signal_str1.emit("wait")
            self.signal_str.emit('Started check trim ...')  
            for smx in self.smxes:
                check_trim(smx)  
            self.signal_str.emit('Finished check trim ...')  
            self.signal_str1.emit("default")
            
        if (self.command == "testing"): 
            self.signal_str1.emit("wait")
            self.signal_str.emit('Started TEST')  
            time.sleep(2)
            for i in range(0,10): 
                self.signal_str.emit('{}'.format(i))   
                time.sleep(2)
            self.signal_str.emit('Finished TEST')  
            self.signal_str1.emit("default")   
    
    def stop(self):
        self.is_running = False
        self.terminate()

class DAQThreadClass(QtCore.QThread):
    signal_str = QtCore.pyqtSignal(str)
    signal_str1 = QtCore.pyqtSignal(str)
    
    def __init__(self, parent = None, DAQinfo_name="", DAQduration = -1, DAQsmxes = [], DAQsmx_tester = []):
        super(DAQThreadClass, self).__init__(parent)
        self.info_name = DAQinfo_name
        self.duration = DAQduration
        self.smxes = DAQsmxes
        self.smx_tester = DAQsmx_tester
        self.is_running = True
        
    def run(self):
        self.signal_str.emit('DAQ started ...')
        self.signal_str1.emit('Data acquisition started.\nName: {}\nDuration: {} seconds\nThe rootfile will be save at:\n("Go to Files" for location)'.format(self.info_name, self.duration))
        time_start = time.time()
        DAQ_start(self.duration,self.info_name)
        time_finish = time.time() - time_start
        self.signal_str.emit('DAQ finished after {} seconds'.format(time_finish))
        self.signal_str1.emit('Data acquisition finished.\nName: {}\nDuration: {} seconds\nThe rootfile will be save at:\n("Go to Files" for location)'.format(self.info_name, time_finish))
        
    def stop(self):
        self.is_running = False
        self.terminate()
    
app = QtWidgets.QApplication(sys.argv)
mainWindow = PFAD_GUI()
mainWindow.show()
sys.exit(app.exec_())
